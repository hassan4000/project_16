package com.wjez.app.api


import com.wjez.app.fragment.project21.settingModel.SettingModel
import com.wjez.app.model.*
import com.wjez.app.model.project16.*
import com.wjez.app.model.project16.AboutUsModel
import com.wjez.app.model.project21.*
import io.reactivex.Observable
import io.reactivex.internal.operators.observable.ObservableAll
import okhttp3.*
import retrofit2.http.*



interface AppApi {

    @Multipart
    @POST("api/customer/service/request")
    fun addSerivces(@Part image:ArrayList<MultipartBody.Part>,
                    @QueryMap map:Map<String,String>,
                    @Part sound:MultipartBody.Part):
            Observable<ResponseBody>




    @POST("api/firebase_uuid")
    fun addUUIDToFirebase(@Body map: Map<String, String>):Observable<ResponseBody>


    @POST("api/customer/change/request/status/approval")
    fun changToApproval(@Body map: Map<String, String>):Observable<ResponseBody>


    @GET("api/provider/dashboard")
    fun getDashBoardProvider():Observable<HomeResponseProvider>

    @POST("api/customer/rate/provider")
    fun addFeedback(@Body map: Map<String, String>):Observable<FeedbackResponse>

    @GET("api/customer/service/requests")
    fun getAllMyRequset():Observable<MyRequsetWjezModel>


    @GET("api/provider/service/requests")
    fun getAllMyRequsetProvider():Observable<MyRequsetWjezModel>


    @POST("api/provider/change/service/status/cancelled")
    fun cancelRequsetProvider(@Body map: Map<String, String>):Observable<ResponseBody>

    @POST("api/customer/block/provider")
    fun blockProvider(@Body map: Map<String, String>):Observable<ResponseBody>
    @POST("api/customer/location/update")
    fun updateLocation(@Body map: Map<String, String>):Observable<ResponseBody>
    @POST("api/provider/location/update")
    fun updateLocationProvider(@Body map: Map<String, String>):Observable<ResponseBody>
    @POST("api/customer/report/provider")
    fun reportProvider(@Body map: Map<String, String>):Observable<ResponseBody>

    @GET("api/customer/get/provider/review/{path}")
    fun getReviews(@Path("path") id: String):Observable<ProviderWithReviewResposne>

    @POST("api/auth/customer/register")
    fun signUpNormal(@Body map:Map<String,String>):Observable<TokenResponse>
    @GET("api/customer/home")
    fun getHomeData():Observable<HomeResponse>



    @GET("api/categories/get")
    fun getCategoryList():Observable<CategeoryListResponse>
    @GET("api/provider/served/categories")
    fun getCategoryListSelected():Observable<CategeoryListResponse>
    @GET("api/categories/get/{number}/sub")
    fun getSubCategory(@Path("number")path: String):Observable<SubCategoryResponse>
    @GET("api/customer/provider/near/{id}")
    fun getPoviderNear(
        @Path("id")id:String,
        @QueryMap map: Map<String, String>
       ):Observable<ProviderNearByResponse>

    @GET("api/customer/search")
    fun searchWjez(@QueryMap map: Map<String, String>):Observable<HomeResponse>

    @GET("api/customer/info")
    fun getUserInfo():Observable<UserInfoResponse>

    @GET("api/provider/info")
    fun getUserProviderInfo():Observable<UserInfoDataResponse>
    @POST("api/check/phone")
    fun checkPhoneWjez(@Body map: Map<String, String>):Observable<ResponseBody>

    @POST("api/check/email")
    fun checkEmailWjez(@Body map: Map<String, String>):Observable<ResponseBody>

    @Multipart
    @POST("api/customer/setting/edit/information")
    fun updateProfileWithImage(@Part image:MultipartBody.Part,@QueryMap map:Map<String,String>):Observable<ResponseBody>



    @Multipart
    @POST("api/auth/upload/files")
    fun uploadDocument(@Part image:MultipartBody.Part):Observable<DocumnetIdResponse>

    @Multipart
    @POST("api/auth/upload/image")
    fun uploadProviderPic(@Part image:MultipartBody.Part):Observable<DocumnetIdResponse>

  //  @JvmSuppressWildcards
    @POST("api/auth/provider/register/new")
    fun signupProvider(@Body paramsMap: SignupBody):Observable<TokenResponse>




    @GET("api/cities")
    fun getCountryWjez():Observable<CountryResponseModel>

    @POST("api/customer/setting/edit/information")
    fun updateProfile(@Body map:Map<String,String>):Observable<ResponseBody>

    @POST("api/auth/customer/login")
    fun loginNormal(@Body map:Map<String,String>):Observable<TokenResponse>

    @POST("api/auth/provider/login")
    fun loginProvider(@Body map:Map<String,String>):Observable<TokenResponse>

    @POST("api/customer/setting/change/password")
    fun editPassword(@Body map:Map<String,String>):Observable<MessageModel>
    @POST("api/provider/setting/change/password")
    fun editPasswordProvider(@Body map:Map<String,String>):Observable<MessageModel>


    @POST("api/token/set")
    fun saveDevicesToken(@Body map: Map<String, String>):Observable<ResponseBody>
//////////////////////////////



    @GET("titles")
    fun getJobTitle():Observable<JobTitleModel>

    @POST("favorites")
    fun addFav(@Body map: Map<String, String>):Observable<MessageModel>

    @GET("featured")
    fun getFeatured():Observable<TrendingModel>



    @GET("account/{accountId}")
    fun getAccountById(@Path("accountId")id:String):Observable<LoginResposeModel>
    @GET("favorites")
    fun getFavorites():Observable<TrendingModel>
    @POST("orders")
    fun addOrders(@Body map: Map<String, String>):Observable<MessageModel>

    @POST("mark-read")
    fun postMarkAsRead(@Body map: Map<String, String>):Observable<ResponseBody>
    @POST("reply")
    fun postReply(@Body map: Map<String, String>):Observable<ResponseBody>
    @GET("trend")
    fun getTrend():Observable<TrendingModel>


    @GET("api/customer/all/offers/{id}")
    fun getOffersByID(@Path("id")path:String):Observable<OffersNumberResponse>


    @GET("api/customer/all/offers/{id}")
    fun getOffersByIDAndFilter(@Path("id")path:String,@QueryMap map: Map<String, String>)
            :Observable<OffersNumberResponse>

    @GET("api/customer/all/offers")
    fun getOffersByFilter(@QueryMap map: Map<String, String>)
            :Observable<OffersListResponse>


    @GET("messages")
    fun getAllMessages():Observable<AllMessagesModel>

    @POST("messages/{messageId}")
    fun updateMessage(@Path("messageId")path:String,@Body map: Map<String, String>):Observable<MessageModel>

    @POST("messages/update/change-status")
    fun updateStatus(@Body map: Map<String, String>):Observable<ResponseBody>

    @GET("titles")
    fun getAllCategories():Observable<AllCategoryModel>

    @DELETE("messages/{messageId}")
    fun deleteMessages(@Path ("messageId") map:String):Observable<ResponseBody>



    @GET("titles/{id}")
    fun getSpecial(@Path("id")path:String):Observable<JobTitleModel>




    @POST("api/customer/change/service/status/cancelled")
    fun cancelSerices(@Body map: Map<String, String>):Observable<ResponseBody>

    @POST("api/provider/send/offer")
    fun sendOffer(@Body map: Map<String, String>):Observable<ResponseBody>

    @POST("api/provider/change/service/status/finished")
    fun finishedProvider(@Body map: Map<String, String>):Observable<ResponseBody>




    @POST("social-login")
    fun loginSocial(@Body map:Map<String,String>):Observable<LoginResposeModel>



    @POST("messages")
    fun addNewMessage(@Body map: Map<String, String>):Observable<MessageModel>

    @POST("profile/edit/name")
    fun editName(@Body map:Map<String,String>):Observable<MessageModel>

    @POST("profile/edit/email")
    fun editEmail(@Body map:Map<String,String>):Observable<MessageModel>
    @POST("check-phone")
    fun checkPhone(@Body map:Map<String,String>):Observable<MessageModel>

    @POST("check/email")
    fun checkEmail(@Body map:Map<String,String>):Observable<MessageModel>

    @POST("profile/edit/bio")
    fun editBio(@Body map:Map<String,String>):Observable<MessageModel>

    @POST("profile/edit/title")
    fun editJobTitle(@Body map:Map<String,String>):Observable<MessageModel>

    @POST("profile/edit/phone")
    fun editPhone(@Body map:Map<String,String>):Observable<MessageModel>





    @POST("forget-password")
    fun fogotPassword(@Body map: Map<String, String>):Observable<MessageModel>
    @POST("profile/edit/date")
    fun editDateOfBith(@Body map:Map<String,String>):Observable<MessageModel>

    @GET("settings")
    fun getSetting():Observable<SettingModel>



    @GET("aboutApp")
    fun getAboutUs():Observable<AboutUsModel>

    @GET("privacyPolicy")
    fun getprivacyPolicy():Observable<PrivacyPolicyModel>

    @GET("inbox")
    fun getInbox():Observable<MessageInboxSendModel>


    @GET("sent")
    fun getSent():Observable<MessageInboxSendModel>


    @Multipart
    @POST("register")
    fun signUp(@Part image:MultipartBody.Part, @QueryMap map:Map<String,String>):
            Observable<MessageModel>


    @Multipart
    @POST("profile/edit/image")
    fun editPhoto(@Part image:MultipartBody.Part):
            Observable<ImageModel>

    @GET("json_requests.aspx")
    fun getProductByCategory(@QueryMap params: Map<String, String> ):Observable<SubCategroyModel>

    @GET("json_requests.aspx")
    fun getLogin(@QueryMap params: Map<String, String> ):Observable<LoginResponse>


    @GET("json_requests.aspx")
    fun getAboutUs1(@QueryMap params: Map<String, String> ):Observable<AboutUsModel>

    @GET("json_requests.aspx")
    fun getAboutUs3(@QueryMap params: Map<String, String> ):Observable<AboutUSModel3>


    @GET("json_requests.aspx")
    fun getNewArrival(@QueryMap params: Map<String, String> ):Observable<NewArrivalsModel>


    @GET("json_requests.aspx")
    fun getPhotos(@QueryMap params: Map<String, String> ):Observable<PhotoModel>
    @GET("json_requests.aspx")
    fun getFeature(@QueryMap params: Map<String, String> ):Observable<FeatureProductModel>

    @GET("json_requests.aspx")
    fun getClasifire(@QueryMap params: Map<String, String> ):Observable<ClasifierModel>

    @GET("json_requests.aspx")
    fun getAllCompany(@QueryMap params: Map<String, String> ):Observable<AllCompanyModel>

    @GET("json_requests.aspx")
    fun getProductBySub(@QueryMap params: Map<String, String> ):Observable<ProductBySubModel>

    @GET("json")
    fun getLocation(@QueryMap params: Map<String, String>):Observable<MapResponserModel>

    @GET("json_requests.aspx")
    fun getNationality(@QueryMap params: Map<String, String>):Observable<NationalityModel>

    @GET("json_requests.aspx")
    fun getRegister(@QueryMap params: Map<String, String>):Observable<ResponseBody>

}
