package com.wjez.app.tools






import android.content.Intent

import android.graphics.PorterDuff
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.transition.Fade
import android.transition.TransitionInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.firebase.auth.PhoneAuthProvider

import com.wjez.app.R
import com.wjez.app.fragment.homeProvider.HomeProviderWjiz
import com.wjez.app.model.User
import com.wjez.app.model.project16.HomeResponse
import com.wjez.app.model.project16.HomeResponseProvider
import com.wjez.app.model.project16.MyRequsetWjezModel
import com.wjez.app.model.project16.UserInfoResponse
import com.wjez.app.model.project21.*


abstract class TemplateActivity : AppCompatActivity() {

    var current_fragment: TemplateFragment? = null





        //var db:AppDatabase?=null
        protected set
    protected var fragment_place: ViewGroup? = null
    var user: User?=null
    // private FirebaseAnalytics mFirebaseAnalytics;

    companion object {



        var storedVerificationId=""
        var tokenResendOtp: PhoneAuthProvider.ForceResendingToken?=null

        var signupCustomer:HashMap<String,String>?=null

        var loginType=""
        var wjezUser:UserInfoResponse?=null
        var phoneNumber=""
        var countryNumber=""
        var loginResponse=LoginResposeModel()

        var selectedCategoryName=""
        var selectedCategory:HomeResponse.Data.Category?=null
        var selectedSubCategory:HomeResponse.Data.Category?=null
        var selectedProvider:HomeResponse.Data.Provider?=null
        var selectedRequset:MyRequsetWjezModel.Upcoming?=null
        var selectedRequsetProivder:MyRequsetWjezModel.Upcoming?=null

        var subCategoryList:ArrayList<HomeResponse.Data.Category>?=null
        var providerSubCategoryList:ArrayList<HomeResponse.Data.Provider>?=null


        var myRequsetPrevList:ArrayList<MyRequsetWjezModel.Upcoming>?=null
        var myRequsetUpcomingList:ArrayList<MyRequsetWjezModel.Upcoming>?=null

        var homeResponse:HomeResponse.Data?=null
        var searchResponse:HomeResponse.Data?=null



        var itemToShowOffersID=-1



        var selectedOffers:HomeResponse.Data.Offer?=null






        var mapToRQ=HashMap<String,String>()
        var categoryID="0"
        private var back_pressed: Long = 0

        var photosUri=ArrayList<Uri>()
        var SignUpModel=SignUpModel()






        var phoneForgotPasswrod=""
        var countryCodeForgotPasswrod=""
        var emailForgotPasswrod=""


        var socialImage=""



        //add services
        var addServicesaddress=""
        var addServicesLat:Double?=null
        var addServiceslon:Double?=null
        var addServicesPhone:String?=null
        var addServicesName:String?=null
        var addServicesCitiesID:String?=null
        var addServicesDesc:String?=null



        //proider Signup

        var nameProvider=""
        var phoneNumberProvider=""
        var birthdayProvider=""
        var idNumberProvider=""
        var docIDProvider=""
        var pictureIDProvider=""
        var passwordProvider=""
        var repasswordProvider=""
        var cityIdProvider=""
        var langProvider=""
        var bioProvider=""


        //prvider save data

        var homeProviderData:HomeResponseProvider.Data?=null

        var fromDashboradProvider=false


        //home customer save seleved category && subcategory

        var selectedCategoryHomeCustomer=-1
        var selectedSubCategoryHomeCustomer=-1


    }

    fun getMapForProduct(subid:String,start_row:String,limit_to:String):HashMap<String,String>{
        categoryID=subid
        var map=HashMap<String,String>()
        map.put("token",BasicTools.getToken(this))
        map.put("fun","getproductsbysubcategory")
        map.put("subid",""+subid)
        map.put("start_row",""+start_row)
        map.put("limit_to",""+limit_to)
        TemplateActivity.mapToRQ=map
        return map
    }

    fun resetRequest(){
         addServicesaddress=""
         addServicesLat=null
         addServiceslon=null
         addServicesPhone=null
         addServicesName=null
        addServicesDesc=null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //db= AppDatabase(this)
        setStatusBarColor()
        set_layout()
        init_views()
        set_fragment_place()
        init_activity(savedInstanceState)
        init_events()
        init_analytics()
    }




    abstract fun set_layout()

    abstract fun init_activity(savedInstanceState: Bundle?)

    abstract fun init_views()

    abstract fun init_events()

    //abstract fun load_background()

    abstract fun set_fragment_place()

    private fun init_analytics() {
        try {
            //mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun clearStack(main: Boolean) {
        while (supportFragmentManager.backStackEntryCount > if (main) 1 else 2) {
            supportFragmentManager.popBackStackImmediate()
        }
    }

    @JvmOverloads
    fun show_fragment2(fragment: TemplateFragment, clearStack: Boolean, main: Boolean = false) {
        if (current_fragment !== fragment) {
            if (fragment_place != null) {
                val runnable = Runnable {
                    if (clearStack) {
                        while (supportFragmentManager.backStackEntryCount > 1) {
                            supportFragmentManager.popBackStackImmediate()
                        }
                        if (main) {
                            while (supportFragmentManager.backStackEntryCount > 0) {
                                supportFragmentManager.popBackStackImmediate()
                            }
                        }
                    }
                    replaceFragment(fragment)
                    current_fragment = fragment
                }
                runOnUiThread(runnable)
            }
        }
    }

    @JvmOverloads
    fun show_fragment(fragment: TemplateFragment, forward: Boolean = true) {
        if (current_fragment != fragment) {
            if (fragment_place != null) {
                val runnable = Runnable {
                    if (current_fragment != null)
                        current_fragment!!.removeSnackbar()
                    //fragment.setForward(forward);
                    replaceFragment(fragment)
                    current_fragment = fragment
                }
                runOnUiThread(runnable)
            }
        }
    }


    @JvmOverloads
    fun show_fragment(fragment: TemplateFragment, originView: View, sharedView: String, forward: Boolean = true) {
        if (current_fragment !== fragment) {
            if (fragment_place != null) {
                val runnable = Runnable {
                    if (current_fragment != null)
                        current_fragment!!.removeSnackbar()
                    //fragment.setForward(forward);
                    replaceFragment(fragment, originView, sharedView)
                    current_fragment = fragment
                }
                runOnUiThread(runnable)
            }
        }
    }

    private fun replaceFragment(fragment: TemplateFragment) {
        val backStateName = fragment.javaClass.name+System.currentTimeMillis();
        val manager = supportFragmentManager
        val fragmentPopped = manager.popBackStackImmediate(backStateName, 0)
        if (!fragmentPopped) {
            val transaction = manager.beginTransaction()
            transaction.setCustomAnimations(R.anim.fade_out_short,R.anim.fade_in)
            transaction.replace(fragment_place!!.id, fragment)
            transaction.addToBackStack(backStateName)
            transaction.commit()
            manager.executePendingTransactions()
        }
    }

    private fun replaceFragmentWithAnim(fragment: TemplateFragment, anim1:Int, anim2:Int) {

        val animRes1= anim1
        val animRes2= anim2

        val backStateName = fragment.javaClass.name+System.currentTimeMillis();
        val manager = supportFragmentManager
        val fragmentPopped = manager.popBackStackImmediate(backStateName, 0)
        if (!fragmentPopped) {
            val transaction = manager.beginTransaction()
            transaction.setCustomAnimations(anim1,anim2)
            transaction.replace(fragment_place!!.id, fragment)
            transaction.addToBackStack(backStateName)
            transaction.commit()
        }
    }

    private fun replaceFragmentWithAnim(fragment: TemplateFragment, enterDirection:Int) {

        val animRes1:Int
        var animRes2: Int
        if (enterDirection == Constants.FRAGMENT_TRANSITION_ENTER_DIRECTION) {
            //Entry
            animRes1 = R.anim.enter_from_right
            animRes2 = R.anim.exit_to_left
        } else {
            animRes1 = R.anim.enter_from_left
            animRes2 = R.anim.exit_to_right
        }

        val backStateName = fragment.javaClass.name+System.currentTimeMillis();
        val manager = supportFragmentManager
        val fragmentPopped = manager.popBackStackImmediate(backStateName, 0)
        if (!fragmentPopped) {
            val transaction = manager.beginTransaction()
            transaction.setCustomAnimations(animRes1,animRes2)
            transaction.replace(fragment_place!!.id, fragment)
            transaction.addToBackStack(backStateName)
            transaction.commit()
        }
    }

    private fun replaceFragment(fragment: TemplateFragment, originView: View, sharedView: String) {
        val backStateName = fragment.javaClass.name+System.currentTimeMillis();
        val manager = supportFragmentManager
        val fragmentPopped = manager.popBackStackImmediate(backStateName, 0)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragment.sharedElementEnterTransition = TransitionInflater.from(this).inflateTransition(android.R.transition.move)
            fragment.enterTransition = Fade()
            current_fragment!!.exitTransition = Fade()
            fragment.sharedElementReturnTransition = TransitionInflater.from(this).inflateTransition(android.R.transition.move)
        }
        if (!fragmentPopped) {
            val transaction = manager.beginTransaction()
            transaction.replace(fragment_place!!.id, fragment)
            transaction.addSharedElement(originView, sharedView)
            transaction.addToBackStack(backStateName)
            transaction.commit()
        }
    }



    @JvmOverloads
    fun show_fragment2WithAnimation(fragment: TemplateFragment, clearStack: Boolean, main: Boolean = false
                                    , enterDirection:Int) {
        if (current_fragment !== fragment) {
            if (fragment_place != null) {
                val runnable = Runnable {
                    if (clearStack) {
                        while (supportFragmentManager.backStackEntryCount > 1) {
                            supportFragmentManager.popBackStackImmediate()
                        }
                        if (main) {
                            while (supportFragmentManager.backStackEntryCount > 0) {
                                supportFragmentManager.popBackStackImmediate()
                            }
                        }
                    }
                    replaceFragmentWithAnim(fragment,enterDirection)
                    current_fragment = fragment
                }
                runOnUiThread(runnable)
            }
        }
    }



    fun send_data(message: DataMessage) {
        message._receiver.onReceive(message._extra)
    }

    fun send_data(message: ObjectMessage) {
        message._receiver.onReceive(message._object)
    }


    override fun onBackPressed() {
        if (current_fragment != null && current_fragment!!.on_back_pressed()) {
            current_fragment!!.cancel()
            if (supportFragmentManager.backStackEntryCount == 1) {

                doubleClickToExit()
               // finish()
                /* if (this.getClass().getName().equals("MainActivity"))
                     finish();
                else
                    doubleClickToExit();*/
            } else {
                super.onBackPressed()
                // MainActivityNew.setSelectedByFragmentManager(getSupportFragmentManager());
                current_fragment = supportFragmentManager.fragments[supportFragmentManager.fragments.size - 1] as TemplateFragment
            }
        } else {
            super.onBackPressed()

        }
    }

    private fun doubleClickToExit() {
        if (back_pressed + 2000 > System.currentTimeMillis())
            finish()
        else
            showToastMessageShort(R.string.double_click)
        back_pressed = System.currentTimeMillis()
    }

    override fun onStart() {
        super.onStart()
        //BasicTools.logScreen(getClass().getName(), mFirebaseAnalytics);
    }




    fun showToastMessage(msg: String?) {
        //Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
        val toast = Toast.makeText(applicationContext, msg, Toast.LENGTH_LONG)
/*        val view = toast.view

        //Gets the actual oval background of the Toast then sets the colour filter
        view!!.background.setColorFilter(fetchColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN)

        //Gets the TextView from the Toast so it can be editted
        val text = view.findViewById<TextView>(android.R.id.message)
        text.setTextColor(fetchColor(R.color.tabs_white))
        //BasicTools.setDroidFont(this,text);*/

        toast.show()
    }


    fun showToastMessage(msg: Int) {
        //Toast.makeText(getApplicationContext(), getResources().getString(msg), Toast.LENGTH_LONG).show();
        val toast = Toast.makeText(applicationContext, resources.getString(msg), Toast.LENGTH_LONG)
/*        val view = toast.view

        //Gets the actual oval background of the Toast then sets the colour filter
        view?.background?.setColorFilter(fetchColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN)

        //Gets the TextView from the Toast so it can be editted
        val text = view.findViewById<TextView>(android.R.id.message)
        text.setTextColor(fetchColor(R.color.tabs_white))*/
        //BasicTools.setDroidFont(this,text);

        toast.show()
    }

    fun showToastMessageShort(msg: String?) {
        //Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
        val toast = Toast.makeText(applicationContext, msg, Toast.LENGTH_SHORT)
    /*    val view = toast.view

        //Gets the actual oval background of the Toast then sets the colour filter
        view!!.background.setColorFilter(fetchColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN)

        //Gets the TextView from the Toast so it can be editted
        val text = view.findViewById<TextView>(android.R.id.message)
        text.setTextColor(fetchColor(R.color.tabs_white))
        //BasicTools.setDroidFont(this,text);*/


        toast.show()
    }


    fun showToastMessageShort(msg: Int) {
        //Toast.makeText(getApplicationContext(), getResources().getString(msg), Toast.LENGTH_SHORT).show();
        val toast = Toast.makeText(applicationContext, resources.getString(msg), Toast.LENGTH_SHORT)
        val view = toast.view
        //Gets the actual oval background of the Toast then sets the colour filter
        view!!.background.setColorFilter(fetchColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN)

        //Gets the TextView from the Toast so it can be editted
        val text = view!!.findViewById<TextView>(android.R.id.message)
        text.setTextColor(fetchColor(R.color.tabs_white))
        //BasicTools.setDroidFont(this,text);

        toast.show()
    }

    fun fetchColor(@ColorRes color: Int): Int {
        return ContextCompat.getColor(this, color)
    }

    fun setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= 21) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            // window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
            window.statusBarColor = resources.getColor(R.color.colorPrimary)
            window.navigationBarColor = resources.getColor(R.color.colorPrimaryDark)
            //actionBar!!.elevation = 0F;

        }

        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 22) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            // window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
            window.statusBarColor = resources.getColor(R.color.colorPrimary)
            window.navigationBarColor = resources.getColor(R.color.colorPrimaryDark)

        }


        //        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        //
        //            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        //            getWindow().setStatusBarColor(Color.WHITE);
        //
        //        }
    }

    public fun showActivity(toActivity: Class<*>) {
        val intent = Intent(this, toActivity)
        startActivity(intent)
    }


}

