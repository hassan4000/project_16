package com.wjez.app.tools
/**
 *
 */
interface ICancelable {
    fun cancel()
}
