package com.wjez.app.tools

/**
 * Created by Shaban on 8/20/2017.
 */

interface OnBottomReached {

    fun onReachBottom()
    fun onScrolledUp()

}
