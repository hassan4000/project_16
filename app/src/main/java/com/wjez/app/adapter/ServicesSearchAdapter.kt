package com.wjez.app.adapter




import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView

import com.wjez.app.R

import com.wjez.app.fragment.searchFragment.ISearchFragment
import com.wjez.app.model.project16.HomeResponse


import com.wjez.app.model.project21.TrendingModel

import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.DownloadListener
import kotlinx.android.synthetic.main.card_category_search_customer.view.*


import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ServicesSearchAdapter(): RecyclerView.Adapter<ServicesSearchAdapter.AdapterViewHolder>(){

    private var data:ArrayList<HomeResponse.Data.Category> ?= null
    private var context:Context ?=null

    private var iview:ISearchFragment ?=null

    private var anim: Animation? = null



    constructor(context:Context,item:ArrayList<HomeResponse.Data.Category>,iview:ISearchFragment ) : this() {

        this.context=context
        this.data=item
        this.iview=iview


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {
        return AdapterViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.card_category_search_customer, parent, false)
        )
    }



    fun change_data(new_data:ArrayList<HomeResponse.Data.Category>){
        CoroutineScope(Dispatchers.Main).launch {
            data?.clear()
            for(item in new_data)
                data?.add(item)
            notifyDataSetChanged()

        }


    }


    fun addItem(item:HomeResponse.Data.Category,pos:Int){
        CoroutineScope(Dispatchers.Main).launch {

            if (pos < data!!.size) run {
                data!!.add(pos, item)
                this@ServicesSearchAdapter.notifyItemInserted(pos)
            }else run {
                data!!.add(item)
                this@ServicesSearchAdapter.notifyItemInserted(data!!.size-1)
            }

        }
    }
    fun addRefersh(new_data:ArrayList<HomeResponse.Data.Category>){
        CoroutineScope(Dispatchers.Main).launch {

            for(item in new_data)
                data?.add(item)
            notifyItemInserted(data!!.size-1)
        }

    }

    fun getUserDataAt(position: Int): HomeResponse.Data.Category? {
        return if (position >= 0 && position < data!!.size) data!!.get(position)
        else null
    }

    fun getList(): ArrayList<HomeResponse.Data.Category> {
        return data!!
    }

    override fun getItemCount() = data!!.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: AdapterViewHolder, position: Int) {

        val item = data?.get(position)


        var image = holder.view.iv_icon as ImageView
        var text = holder.view.tv_name as TextView
        var root = holder.view.root_card as LinearLayout

        if (BasicTools.isDeviceLanEn()) {
            text.setText(item?.name)
        } else {
            text.setText(item?.nameAr)
        }


        Log.i("TEST_TEST", BasicTools.getUrlImg(context!!, item?.image!!.toString()))


        BasicTools.loadImage(
            BasicTools.getUrlImg(context!!, item?.image!!.toString()),
            image,
            object : DownloadListener {
                override fun completed(status: Boolean, bitmap: Bitmap) {
                    anim = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
                    anim!!.setDuration(1000)
                    image.animation = anim
                }
            })



        root.setOnClickListener {

        }


    }




    class AdapterViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
