package com.wjez.app.adapter



import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

import com.wjez.app.R

import kotlinx.android.synthetic.main.card_image_add_product.view.*



import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ImageAddProductAdapter(): RecyclerView.Adapter<ImageAddProductAdapter.AdapterViewHolder>(){

    private var data:ArrayList<Uri> ?= null
    private var context:Context ?=null


    private var anim: Animation? = null



    constructor(context:Context,item:ArrayList<Uri>) : this() {

        this.context=context
        this.data=item


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {
        return AdapterViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.card_image_add_product, parent, false)
        )
    }



    fun change_data(new_data:ArrayList<Uri>){
        CoroutineScope(Dispatchers.Main).launch {
            data?.clear()
            for(item in new_data)
                data?.add(item)
            notifyDataSetChanged()

        }


    }


    fun addItem(item:Uri,pos:Int){
        CoroutineScope(Dispatchers.Main).launch {

            if (pos < data!!.size) run {
                data!!.add(pos, item)
                this@ImageAddProductAdapter.notifyItemInserted(pos)
            }else run {
                data!!.add(item)
                this@ImageAddProductAdapter.notifyItemInserted(data!!.size-1)
            }

        }
    }
    fun addRefersh(new_data:ArrayList<Uri>){
        CoroutineScope(Dispatchers.Main).launch {

            for(item in new_data)
                data?.add(item)
            notifyItemInserted(data!!.size-1)
        }

    }

    fun getUserDataAt(position: Int): Uri? {
        return if (position >= 0 && position < data!!.size) data!!.get(position)
        else null
    }

    fun getList(): ArrayList<Uri> {
        return data!!
    }

    override fun getItemCount() = data!!.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: AdapterViewHolder, position: Int) {

        val item=data?.get(position)

        Log.i("ImageAdapter","IMAGE_URI=$item")


        val Image=holder.view.iv_main_image
        val deleteImage=holder.view.iv_cancel
       // val tvIndex=holder.view.tv_index

       // tvIndex.setText("${position+1}")
        Glide.with(context!!).load(item).placeholder(
                R.drawable.wjez_logo_v2
        ).error(R.drawable.wjez_logo_v2).into(Image)


        deleteImage.setOnClickListener{
         CoroutineScope(Dispatchers.Main).launch {
             data!!.removeAt(position)
            notifyDataSetChanged()
         }
        }


        //  holder.view.text_view_note.text = notes[position].note


/*
        holder.view.card_product.setOnClickListener{

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
               holder.view.iv_card_product.setTransitionName(Constants.TRANSITION+position)
            }
            iview!!.openProductInfo(holder.view.iv_card_product,item!!,Constants.TRANSITION+position)
        }*/



    }



    class AdapterViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
