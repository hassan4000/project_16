package com.wjez.app.adapter




import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView

import com.wjez.app.R
import com.wjez.app.fragment.homeFragment.IHomeFragment

import com.wjez.app.fragment.providerSubCategory.IProviderSubCategoryFragment

import com.wjez.app.model.project16.HomeResponse




import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.DownloadListener
import kotlinx.android.synthetic.main.card_provider_near_you_v2.view.*
import kotlinx.android.synthetic.main.card_provider_near_you_v2.view.iv_profile
import kotlinx.android.synthetic.main.card_provider_near_you_v2.view.ratingBar
import kotlinx.android.synthetic.main.card_provider_near_you_v2.view.tv_name
import kotlinx.android.synthetic.main.card_provider_near_you_v2.view.tv_rating


import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProviderNearbyStoresCustomerAdapter(): RecyclerView.Adapter<ProviderNearbyStoresCustomerAdapter.AdapterViewHolder>() {

    private var data: ArrayList<HomeResponse.Data.Provider>? = null
    private var context: Context? = null

    private var iview: IHomeFragment? = null


    private var anim: Animation? = null


    constructor(
        context: Context,
        item: ArrayList<HomeResponse.Data.Provider>,
        iview: IHomeFragment
    ) : this() {

        this.context = context
        this.data = item
        this.iview = iview


    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {
        return AdapterViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.card_provider_near_you_v2, parent, false)
        )
    }


    fun change_data(new_data: ArrayList<HomeResponse.Data.Provider>) {
        CoroutineScope(Dispatchers.Main).launch {
            data?.clear()
            for (item in new_data)
                data?.add(item)
            notifyDataSetChanged()

        }


    }


    fun addItem(item: HomeResponse.Data.Provider, pos: Int) {
        CoroutineScope(Dispatchers.Main).launch {

            if (pos < data!!.size) run {
                data!!.add(pos, item)
                this@ProviderNearbyStoresCustomerAdapter.notifyItemInserted(pos)
            } else run {
                data!!.add(item)
                this@ProviderNearbyStoresCustomerAdapter.notifyItemInserted(data!!.size - 1)
            }

        }
    }

    fun addRefersh(new_data: ArrayList<HomeResponse.Data.Provider>) {
        CoroutineScope(Dispatchers.Main).launch {

            for (item in new_data)
                data?.add(item)
            notifyItemInserted(data!!.size - 1)
        }

    }

    fun getUserDataAt(position: Int): HomeResponse.Data.Provider? {
        return if (position >= 0 && position < data!!.size) data!!.get(position)
        else null
    }

    fun getList(): ArrayList<HomeResponse.Data.Provider> {
        return data!!
    }

    override fun getItemCount() = data!!.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: AdapterViewHolder, position: Int) {

        val item = data?.get(position)


        var name = holder.view.tv_name as TextView
        var category = holder.view.tv_category as TextView
        var tvRating = holder.view.tv_rating as TextView
        var root = holder.view.root_card as CardView
        var ratingBar = holder.view.ratingBar as RatingBar
        var image = holder.view.iv_profile as ImageView
        name.setText(item?.name)
        category.setText(item?.userType)
        tvRating.setText(item?.rate)
/*        if (item?.rate != null)
            ratingBar.rating = item.rate?.toFloat()!!*/


        //Log.i("TEST_TEST", BasicTools.getUrlImg(context!!, item?.image!!.toString()))


        if (item?.image != null){
    BasicTools.loadImage(
    BasicTools.getUrlImg(context!!, item?.image!!.toString()),
    image,
    object : DownloadListener {
        override fun completed(status: Boolean, bitmap: Bitmap) {
            anim = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
            anim!!.setDuration(1000)
            image.animation = anim
        }
    })

}


        root.setOnClickListener {

            if (item != null) {
                iview?.openProfileFile(item)
            }
        }


    }




    class AdapterViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
