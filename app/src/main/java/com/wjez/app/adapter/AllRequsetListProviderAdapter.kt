package com.wjez.app.adapter




import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView

import com.wjez.app.R
import com.wjez.app.fragment.allRequsetProviderFragment.IAllRequsetProviderFragment
import com.wjez.app.fragment.allServicesFragment.IAllServicesFragment
import com.wjez.app.fragment.homeFragment.IHomeFragment
import com.wjez.app.fragment.homeProvider.IHomeProvider
import com.wjez.app.model.project16.HomeResponse
import com.wjez.app.model.project16.HomeResponseProvider
import com.wjez.app.model.project16.MyRequsetWjezModel


import com.wjez.app.model.project21.TrendingModel

import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.DownloadListener
import kotlinx.android.synthetic.main.card_provider_home.view.*


import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AllRequsetListProviderAdapter(): RecyclerView.Adapter<AllRequsetListProviderAdapter.AdapterViewHolder>(){

    private var data:ArrayList<MyRequsetWjezModel.Upcoming> ?= null
    private var context:Context ?=null

    private var iview:IAllRequsetProviderFragment ?=null


    private var anim: Animation? = null






    constructor(context:Context, item:ArrayList<MyRequsetWjezModel.Upcoming>, iview:IAllRequsetProviderFragment) : this() {

        this.context=context
        this.data=item
        this.iview=iview


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {
        return AdapterViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.card_provider_home, parent, false)
        )
    }



    fun change_data(new_data:ArrayList<MyRequsetWjezModel.Upcoming>){
        CoroutineScope(Dispatchers.Main).launch {
            data?.clear()
            for(item in new_data)
                data?.add(item)
            notifyDataSetChanged()

        }


    }


    fun addItem(item:MyRequsetWjezModel.Upcoming,pos:Int){
        CoroutineScope(Dispatchers.Main).launch {

            if (pos < data!!.size) run {
                data!!.add(pos, item)
                this@AllRequsetListProviderAdapter.notifyItemInserted(pos)
            }else run {
                data!!.add(item)
                this@AllRequsetListProviderAdapter.notifyItemInserted(data!!.size-1)
            }

        }
    }
    fun addRefersh(new_data:ArrayList<MyRequsetWjezModel.Upcoming>){
        CoroutineScope(Dispatchers.Main).launch {

            for(item in new_data)
                data?.add(item)
            notifyItemInserted(data!!.size-1)
        }

    }

    fun getUserDataAt(position: Int): MyRequsetWjezModel.Upcoming? {
        return if (position >= 0 && position < data!!.size) data!!.get(position)
        else null
    }

    fun getList(): ArrayList<MyRequsetWjezModel.Upcoming> {
        return data!!
    }

    override fun getItemCount() = data!!.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: AdapterViewHolder, position: Int) {

        val item = data?.get(position)


      //  var image = holder.view.iv_icon as ImageView
        var text = holder.view.tv_name as TextView
        var rootBtnRequset = holder.view.card_details as CardView
        var date = holder.view.iv_date as TextView
        var root = holder.view.root_card as CardView
        var image = holder.view.iv_profile as ImageView
        var cardBtn = holder.view.card_details as CardView
        var tvDistence = holder.view.tv_distence as TextView

        tvDistence.setText("${item?.distance?.toString()} km")
        date.setText(item?.createdAt!!.substring(0,18).replace(Regex("T")," "))
        if (BasicTools.isDeviceLanEn()) {
            text.setText(item?.user?.name)
        } else {
            text.setText(item?.user?.name)
        }


       // Log.i("TEST_TEST", BasicTools.getUrlImg(context!!, item?.image!!.toString()))

        rootBtnRequset.visibility=View.VISIBLE

        if(item!!.user!=null&& item.user?.image!=null)
        BasicTools.loadImage(
            BasicTools.getUrlImg(context!!, item?.user!!.image.toString()),
            image,
            object : DownloadListener {
                override fun completed(status: Boolean, bitmap: Bitmap) {
                    anim = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
                    anim!!.setDuration(1000)
                    image.animation = anim
                }
            })



        root.setOnClickListener {

            iview?.openDetails(item)
           /* if(iviewAllServices!=null)
                iviewAllServices!!.openSubCategory(item!!)*/
        }

        cardBtn.setOnClickListener {


        }

     /*   cardBtn.setOnClickListener {


        }*/


    }




    class AdapterViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
