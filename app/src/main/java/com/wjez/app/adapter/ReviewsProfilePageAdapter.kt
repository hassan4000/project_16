package com.wjez.app.adapter




import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.media.Rating

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView

import com.wjez.app.R
import com.wjez.app.fragment.homeFragment.IHomeFragment
import com.wjez.app.model.project16.HomeResponse
import com.wjez.app.model.project16.ProviderWithReviewResposne


import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.BasicTools.getUrlImg
import com.wjez.app.tools.DownloadListener
import kotlinx.android.synthetic.main.card_review.view.*


import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ReviewsProfilePageAdapter(): RecyclerView.Adapter<ReviewsProfilePageAdapter.AdapterViewHolder>(){

    private var data:ArrayList<ProviderWithReviewResposne.Review> ?= null
    private var context:Context ?=null



    private var anim: Animation? = null



    constructor(context:Context,item:ArrayList<ProviderWithReviewResposne.Review> ) : this() {

        this.context=context
        this.data=item



    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {
        return AdapterViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.card_review, parent, false)
        )
    }



    fun change_data(new_data:ArrayList<ProviderWithReviewResposne.Review>){
        CoroutineScope(Dispatchers.Main).launch {
            data?.clear()
            for(item in new_data)
                data?.add(item)
            notifyDataSetChanged()

        }


    }


    fun addItem(item:ProviderWithReviewResposne.Review,pos:Int){
        CoroutineScope(Dispatchers.Main).launch {

            if (pos < data!!.size) run {
                data!!.add(pos, item)
                this@ReviewsProfilePageAdapter.notifyItemInserted(pos)
            }else run {
                data!!.add(item)
                this@ReviewsProfilePageAdapter.notifyItemInserted(data!!.size-1)
            }

        }
    }
    fun addRefersh(new_data:ArrayList<ProviderWithReviewResposne.Review>){
        CoroutineScope(Dispatchers.Main).launch {

            for(item in new_data)
                data?.add(item)
            notifyItemInserted(data!!.size-1)
        }

    }

    fun getUserDataAt(position: Int): ProviderWithReviewResposne.Review? {
        return if (position >= 0 && position < data!!.size) data!!.get(position)
        else null
    }

    fun getList(): ArrayList<ProviderWithReviewResposne.Review> {
        return data!!
    }

    override fun getItemCount() = data!!.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: AdapterViewHolder, position: Int) {

        val item=data?.get(position)








        var name=  holder.view.tv_name as TextView


        var desc=  holder.view.tv_desc as TextView
        var ratingBar=  holder.view.ratingBar as RatingBar
        var image=  holder.view.iv_profile as ImageView




       name.setText(item?.user?.name)

        desc.setText(item?.feedback)

        if(!item!!.rate.isNullOrEmpty())
        ratingBar.rating= item?.rate!!.toFloat()
    /*    if(BasicTools.isDeviceLanEn()){
            text.setText(item?.provider?.name)
        }else{
            text.setText(item?.nameAr)
        }
*/



        BasicTools.loadImage(getUrlImg(context!!,item?.user?.image!!),image,object : DownloadListener {
            override fun completed(status: Boolean, bitmap: Bitmap) {
                anim = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
                anim!!.setDuration(1000)
                image.animation= anim
            }
        })






    }



    class AdapterViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
