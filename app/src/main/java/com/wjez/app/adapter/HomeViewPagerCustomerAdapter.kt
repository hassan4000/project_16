package com.wjez.app.adapter




import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.media.Rating

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView

import com.wjez.app.R
import com.wjez.app.fragment.homeFragment.IHomeFragment
import com.wjez.app.model.project16.HomeResponse




import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.BasicTools.getUrlImg
import com.wjez.app.tools.DownloadListener
import kotlinx.android.synthetic.main.card_viewpager_home_customer.view.*


import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeViewPagerCustomerAdapter(): RecyclerView.Adapter<HomeViewPagerCustomerAdapter.AdapterViewHolder>(){

    private var data:ArrayList<String> ?= null
    private var context:Context ?=null

    private var iview:IHomeFragment ?=null

    private var anim: Animation? = null



    constructor(context:Context,item:ArrayList<String>,iview:IHomeFragment ) : this() {

        this.context=context
        this.data=item
        this.iview=iview


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {
        return AdapterViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.card_viewpager_home_customer, parent, false)
        )
    }



    fun change_data(new_data:ArrayList<String>){
        CoroutineScope(Dispatchers.Main).launch {
            data?.clear()
            for(item in new_data)
                data?.add(item)
            notifyDataSetChanged()

        }


    }


    fun addItem(item:String,pos:Int){
        CoroutineScope(Dispatchers.Main).launch {

            if (pos < data!!.size) run {
                data!!.add(pos, item)
                this@HomeViewPagerCustomerAdapter.notifyItemInserted(pos)
            }else run {
                data!!.add(item)
                this@HomeViewPagerCustomerAdapter.notifyItemInserted(data!!.size-1)
            }

        }
    }
    fun addRefersh(new_data:ArrayList<String>){
        CoroutineScope(Dispatchers.Main).launch {

            for(item in new_data)
                data?.add(item)
            notifyItemInserted(data!!.size-1)
        }

    }

    fun getUserDataAt(position: Int): String? {
        return if (position >= 0 && position < data!!.size) data!!.get(position)
        else null
    }

    fun getList(): ArrayList<String> {
        return data!!
    }

    override fun getItemCount() = data!!.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: AdapterViewHolder, position: Int) {

        val item=data?.get(position)








        var name=  holder.view.tv_heading as TextView
        name.setText(item)




    }



    class AdapterViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
