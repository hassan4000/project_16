package com.wjez.app.adapter

import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.wjez.app.R

import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.DownloadListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.wjez.app.model.project16.ChatFirebaseModel
import kotlinx.android.synthetic.main.card_receiver.view.*

class ChatsAdapter(
    mContext: Context,
    mChatList:List<ChatFirebaseModel>,
    userID:String,
    imageUrl:String):RecyclerView.Adapter<ChatsAdapter.ViewHolder?>(){

    private  val mContext:Context
    private val mChatList:List<ChatFirebaseModel>
    private val imageUrl:String
    private val userID:String
   // var firebaseUser:FirebaseUser=FirebaseAuth.getInstance().currentUser!!
    private var anim: Animation? = null

    init {

        this.mContext=mContext
        this.imageUrl=imageUrl
        this.userID=userID
        this.mChatList=mChatList
    }




    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {

        return  if(p1==1){
            val view:View=LayoutInflater.from(mContext).inflate(R.layout.card_sender,p0,false)
            ViewHolder(view)
        }

        else {
            val view:View=LayoutInflater.from(mContext).inflate(R.layout.card_receiver,p0,false)
            ViewHolder(view)
        }

    }

    override fun getItemCount(): Int {

        return  mChatList.size

    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        var item=mChatList.get(p1)

        /*        SENDER                  */
        if(item.sender.equals(userID)){

            if(item.url!!.isEmpty()){

                p0.textSender?.visibility=View.VISIBLE
                p0.senderImageData?.visibility=View.GONE
                p0.textSender?.setText(item.message)

            }else{
                p0.textSender?.visibility=View.GONE
                p0.senderImageData?.visibility=View.VISIBLE
                BasicTools.loadImage(item.url!!,p0.senderImageData!!,object : DownloadListener {
                    override fun completed(status: Boolean, bitmap: Bitmap) {
                        anim = AnimationUtils.loadAnimation(mContext, android.R.anim.fade_in)
                        anim!!.setDuration(1000)
                        p0.senderImageData!!.animation= anim
                    }
                })
            }

            if(p1==mChatList.size-1){
                if(item.isSeen!!){
                    p0.textSeenSender?.visibility=View.VISIBLE
                    p0.textSeenSender?.text="seen"
                }
                else{
                    p0.textSeenSender?.visibility=View.VISIBLE
                    p0.textSeenSender?.text="sent"
                }
            }else{
                p0.textSeenSender?.visibility=View.GONE
            }
        }


        /*  ----------------------- RECEIVER  --------------------------------------------------    */
        else{

            if(item.url!!.isEmpty()){

                p0.textReceiver?.visibility=View.VISIBLE
                p0.receiverImageData?.visibility=View.GONE
                p0.textReceiver?.setText(item.message)

            }else{
                p0.textReceiver?.visibility=View.GONE
                p0.receiverImageData?.visibility=View.VISIBLE
                BasicTools.loadImage(item.url!!,p0.receiverImageData!!,object : DownloadListener {
                    override fun completed(status: Boolean, bitmap: Bitmap) {
                        anim = AnimationUtils.loadAnimation(mContext, android.R.anim.fade_in)
                        anim!!.setDuration(1000)
                        p0.receiverImageData!!.animation= anim
                    }
                })
            }

            BasicTools.loadImage(imageUrl,p0.receiverImage!!,object : DownloadListener {
                override fun completed(status: Boolean, bitmap: Bitmap) {
                    anim = AnimationUtils.loadAnimation(mContext, android.R.anim.fade_in)
                    anim!!.setDuration(1000)
                    p0.receiverImage!!.animation= anim
                }
            })





        }


    }



    inner  class  ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){


        var receiverImage:ImageView?=null
        var receiverImageData:ImageView?=null
        var senderImageData:ImageView?=null
        var textSender:TextView?=null
        var textReceiver:TextView?=null
        var textSeenSender:TextView?=null

        init {
            receiverImage=itemView.findViewById(R.id.iv_image_receiver)
            receiverImageData=itemView.findViewById(R.id.iv_image_receiver_data)
            senderImageData=itemView.findViewById(R.id.iv_image_sender_data)
            textSender=itemView.findViewById(R.id.tv_text_sender)
            textReceiver=itemView.findViewById(R.id.tv_text_receiver)
            textSeenSender=itemView.findViewById(R.id.tv_seen_sender)
        }


    }

    override fun getItemViewType(position: Int): Int {
     //   return super.getItemViewType(position)


        return  if(mChatList.get(position).sender.equals(userID)){
               1
        }
        else {
              2
        }
    }




}