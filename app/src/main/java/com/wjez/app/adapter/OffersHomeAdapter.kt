package com.wjez.app.adapter




import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.media.Rating

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView

import com.wjez.app.R
import com.wjez.app.fragment.homeFragment.IHomeFragment
import com.wjez.app.model.project16.HomeResponse




import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.BasicTools.getUrlImg
import com.wjez.app.tools.DownloadListener
import kotlinx.android.synthetic.main.activity_requset_details_provider.view.*
import kotlinx.android.synthetic.main.card_offers_customer.view.*
import kotlinx.android.synthetic.main.card_offers_customer.view.iv_profile
import kotlinx.android.synthetic.main.card_offers_customer.view.tv_name
import kotlinx.android.synthetic.main.card_offers_customer.view.tv_price


import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class OffersHomeAdapter(): RecyclerView.Adapter<OffersHomeAdapter.AdapterViewHolder>(){

    private var data:ArrayList<HomeResponse.Data.Offer> ?= null
    private var context:Context ?=null

    private var iview:IHomeFragment ?=null

    private var anim: Animation? = null



    constructor(context:Context,item:ArrayList<HomeResponse.Data.Offer>,iview:IHomeFragment ) : this() {

        this.context=context
        this.data=item
        this.iview=iview


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {
        return AdapterViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.card_offers_customer, parent, false)
        )
    }



    fun change_data(new_data:ArrayList<HomeResponse.Data.Offer>){
        CoroutineScope(Dispatchers.Main).launch {
            data?.clear()
            for(item in new_data)
                data?.add(item)
            notifyDataSetChanged()

        }


    }


    fun addItem(item:HomeResponse.Data.Offer,pos:Int){
        CoroutineScope(Dispatchers.Main).launch {

            if (pos < data!!.size) run {
                data!!.add(pos, item)
                this@OffersHomeAdapter.notifyItemInserted(pos)
            }else run {
                data!!.add(item)
                this@OffersHomeAdapter.notifyItemInserted(data!!.size-1)
            }

        }
    }
    fun addRefersh(new_data:ArrayList<HomeResponse.Data.Offer>){
        CoroutineScope(Dispatchers.Main).launch {

            for(item in new_data)
                data?.add(item)
            notifyItemInserted(data!!.size-1)
        }

    }

    fun getUserDataAt(position: Int): HomeResponse.Data.Offer? {
        return if (position >= 0 && position < data!!.size) data!!.get(position)
        else null
    }

    fun getList(): ArrayList<HomeResponse.Data.Offer> {
        return data!!
    }

    override fun getItemCount() = data!!.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: AdapterViewHolder, position: Int) {

        val item=data?.get(position)








        var name=  holder.view.tv_name as TextView
        var cost=  holder.view.tv_price as TextView
        var rate=  holder.view.tv_rating as TextView
        var desc=  holder.view.tv_desc as TextView
        var ratingBar=  holder.view.ratingBar as RatingBar

        ratingBar.setIsIndicator(false)
        var image=  holder.view.iv_profile as ImageView


        var rootCard=holder.view.card_root_offers

        var accept=holder.view.card_accept as CardView
        var refuse=holder.view.card_refuse as CardView

        name.setText(item?.provider?.name)
        cost.setText(item?.cost)
        rate.setText(item?.provider?.rate.toString())
        desc.setText(item?.details)

        ratingBar.rating= item?.provider?.rate!!.toString().toFloat()
    /*    if(BasicTools.isDeviceLanEn()){
            text.setText(item?.provider?.name)
        }else{
            text.setText(item?.nameAr)
        }
*/


        if(item?.provider?.image!=null)
        BasicTools.loadImage(getUrlImg(context!!,item?.provider?.image!!),image,object : DownloadListener {
            override fun completed(status: Boolean, bitmap: Bitmap) {
                anim = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
                anim!!.setDuration(1000)
                image.animation= anim
            }
        })



        rootCard.setOnClickListener {
            iview?.openPage(item)
        }

        accept.setOnClickListener {
            iview?.openPage(item)
        }

        refuse.setOnClickListener {
            iview?.openPage(item)
        }




    }



    class AdapterViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
