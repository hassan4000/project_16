package com.wjez.app.adapter




import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView

import com.wjez.app.R


import com.wjez.app.model.project21.TrendingModel

import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.DownloadListener



import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FavorityListFragmentAdapter(): RecyclerView.Adapter<FavorityListFragmentAdapter.AdapterViewHolder>(){

    private var data:ArrayList<TrendingModel.Data> ?= null
    private var context:Context ?=null

//    private var iview:IFavorityFragment ?=null

    private var anim: Animation? = null



    constructor(context:Context,item:ArrayList<TrendingModel.Data> ) : this() {

        this.context=context
        this.data=item


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {
        return AdapterViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.activity_about_us, parent, false)
        )
    }



    fun change_data(new_data:ArrayList<TrendingModel.Data>){
        CoroutineScope(Dispatchers.Main).launch {
            data?.clear()
            for(item in new_data)
                data?.add(item)
            notifyDataSetChanged()

        }


    }


    fun addItem(item:TrendingModel.Data,pos:Int){
        CoroutineScope(Dispatchers.Main).launch {

            if (pos < data!!.size) run {
                data!!.add(pos, item)
                this@FavorityListFragmentAdapter.notifyItemInserted(pos)
            }else run {
                data!!.add(item)
                this@FavorityListFragmentAdapter.notifyItemInserted(data!!.size-1)
            }

        }
    }
    fun addRefersh(new_data:ArrayList<TrendingModel.Data>){
        CoroutineScope(Dispatchers.Main).launch {

            for(item in new_data)
                data?.add(item)
            notifyItemInserted(data!!.size-1)
        }

    }

    fun getUserDataAt(position: Int): TrendingModel.Data? {
        return if (position >= 0 && position < data!!.size) data!!.get(position)
        else null
    }

    fun getList(): ArrayList<TrendingModel.Data> {
        return data!!
    }

    override fun getItemCount() = data!!.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: AdapterViewHolder, position: Int) {

        val item=data?.get(position)

        Log.i("ImageAdapter","IMAGE_URI=$item")






       // var image=  holder.view.iv_profile as ImageView




    /*
        BasicTools.loadImage(item?.image!!.toString(),image,object : DownloadListener {
            override fun completed(status: Boolean, bitmap: Bitmap) {
                anim = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
                anim!!.setDuration(1000)
                image.animation= anim
            }
        })
*/





    }



    class AdapterViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
