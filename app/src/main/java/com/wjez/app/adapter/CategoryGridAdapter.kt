package com.wjez.app.adapter




import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView

import com.wjez.app.R
import com.wjez.app.activity.provider.signup_provider.ISignUpProvider2Activity
import com.wjez.app.activity.updateProfileProvider.IUpdateProfileProviderActivity
import com.wjez.app.fragment.allServicesFragment.IAllServicesFragment
import com.wjez.app.fragment.homeFragment.IHomeFragment
import com.wjez.app.model.project16.HomeResponse


import com.wjez.app.model.project21.TrendingModel

import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.DownloadListener
import kotlinx.android.synthetic.main.card_category_grid.view.*


import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CategoryGridAdapter(): RecyclerView.Adapter<CategoryGridAdapter.AdapterViewHolder>(){

    private var data:ArrayList<HomeResponse.Data.Category> ?= null
    private var context:Context ?=null


    private var iviewAllServices: ISignUpProvider2Activity?=null
    private var iviewUpdateProvider: IUpdateProfileProviderActivity?=null

    private var anim: Animation? = null






    constructor(context:Context,item:ArrayList<HomeResponse.Data.Category>,iview:ISignUpProvider2Activity) : this() {
        this.context=context
        this.data=item
        this.iviewAllServices=iview

    }


    constructor(context:Context,item:ArrayList<HomeResponse.Data.Category>,iview:IUpdateProfileProviderActivity) : this() {
        this.context=context
        this.data=item
        this.iviewUpdateProvider=iview

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {
        return AdapterViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.card_category_grid, parent, false)
        )
    }



    fun change_data(new_data:ArrayList<HomeResponse.Data.Category>){
        CoroutineScope(Dispatchers.Main).launch {
            data?.clear()
            for(item in new_data)
                data?.add(item)
            notifyDataSetChanged()

        }


    }


    fun addItem(item:HomeResponse.Data.Category,pos:Int){
        CoroutineScope(Dispatchers.Main).launch {

            if (pos < data!!.size) run {
                data!!.add(pos, item)
                this@CategoryGridAdapter.notifyItemInserted(pos)
            }else run {
                data!!.add(item)
                this@CategoryGridAdapter.notifyItemInserted(data!!.size-1)
            }

        }
    }
    fun addRefersh(new_data:ArrayList<HomeResponse.Data.Category>){
        CoroutineScope(Dispatchers.Main).launch {

            for(item in new_data)
                data?.add(item)
            notifyItemInserted(data!!.size-1)
        }

    }

    fun getUserDataAt(position: Int): HomeResponse.Data.Category? {
        return if (position >= 0 && position < data!!.size) data!!.get(position)
        else null
    }

    fun getList(): ArrayList<HomeResponse.Data.Category> {
        return data!!
    }

    override fun getItemCount() = data!!.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: AdapterViewHolder, position: Int) {

        val item = data?.get(position)


      //  var image = holder.view.iv_icon as ImageView
        var text = holder.view.tv_name as TextView
        var root = holder.view.root_card as CardView
        var image = holder.view.iv_icon as ImageView

        if (BasicTools.isDeviceLanEn()) {
            text.setText(item?.name)
        } else {
            text.setText(item?.nameAr)
        }


        if(item!!.selected!!){
            root.setCardBackgroundColor(ContextCompat.getColor(context!!,R.color.login_btn))
            text.setTextColor(ContextCompat.getColor(context!!,R.color.white))
            root.radius=30F
        }
        else{
            root.setCardBackgroundColor(ContextCompat.getColor(context!!,R.color.bg_category_color))
            text.setTextColor(ContextCompat.getColor(context!!,R.color.text_color_wjez))
            root.radius=30F
        }

       // Log.i("TEST_TEST", BasicTools.getUrlImg(context!!, item?.image!!.toString()))


        if(item.image!=null){


        BasicTools.loadImage(
            BasicTools.getUrlImg(context!!, item?.image!!.toString()),
            image,
            object : DownloadListener {
                override fun completed(status: Boolean, bitmap: Bitmap) {
                    anim = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
                    anim!!.setDuration(1000)
                    image.animation = anim


                }
            })}



        root.setOnClickListener {

            if(iviewAllServices!=null)
            iviewAllServices?.changeIndex(position)

            else if(iviewUpdateProvider!=null)
                iviewUpdateProvider?.changeIndex(position)
        }


    }




    class AdapterViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
