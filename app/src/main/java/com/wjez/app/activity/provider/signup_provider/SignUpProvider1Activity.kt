package com.wjez.app.activity.provider.signup_provider


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.OpenableColumns
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.widget.DatePicker
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import com.mobsandgeeks.saripaar.annotation.Password
import com.wjez.app.R
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.model.project16.DocumnetIdResponse
import com.wjez.app.tools.*
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_signup_provider.*
import kotlinx.android.synthetic.main.activity_signup_provider.edit_full_name
import kotlinx.android.synthetic.main.activity_signup_provider.edit_password
import kotlinx.android.synthetic.main.activity_signup_provider.edit_phone_number
import kotlinx.android.synthetic.main.activity_signup_provider.edit_re_password
import kotlinx.android.synthetic.main.activity_signup_provider.eye_password
import kotlinx.android.synthetic.main.activity_signup_provider.eye_re_password

import kotlinx.android.synthetic.main.toolbar_signup_provider.*
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class SignUpProvider1Activity : TemplateActivity(), Validator.ValidationListener, DatePickerDialog.OnDateSetListener {


    var disposable= CompositeDisposable()
    private var mAddImages =  ArrayList<Uri>()
    lateinit var ivProfile: ImageView
    var items=ArrayList<Uri>()
    var validator: Validator? = null

    var selectDocumentPic=false

    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    @Password
    lateinit var editPasswrod: EditText

    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    @ConfirmPassword(messageResId = R.string.password_donot_match)
    lateinit var editRePasswrod: EditText

    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    lateinit var editfullName: EditText

    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    lateinit var editphone: EditText


    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    lateinit var editIDNumber: EditText


    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    lateinit var editBirthday: EditText


    override fun set_layout() {
        setContentView(R.layout.activity_signup_provider)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun init_views() {

        editBirthday=edit_birthday
        editPasswrod=edit_password
        editRePasswrod=edit_re_password
        editfullName=edit_full_name
        editphone=edit_phone_number
        editIDNumber=edit_id_number

    }

    override fun init_events() {

        validator = Validator(this)
        validator!!.setValidationListener(this)


        card_next.setOnClickListener {
            validator!!.validate()
        }

        iv_document_image.setOnClickListener {
            pickGalleryImages()
        }

        eye_password.setOnClickListener {
            if(edit_password.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){


                eye_password.setImageResource(R.drawable.visibility_3x)
                edit_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                eye_password.setImageResource(R.drawable.visibility_off_3x)
                edit_password.setTransformationMethod(PasswordTransformationMethod.getInstance());

            }
        }



        eye_re_password.setOnClickListener {
            if(editRePasswrod.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){


                eye_re_password.setImageResource(R.drawable.visibility_3x)
                editRePasswrod.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                eye_re_password.setImageResource(R.drawable.visibility_off_3x)
                editRePasswrod.setTransformationMethod(PasswordTransformationMethod.getInstance());

            }
        }




        iv_back.setOnClickListener {
            BasicTools.exitActivity(this@SignUpProvider1Activity)
        }

        editBirthday.setOnClickListener {
            val datePickerFragment = DatePickerFragment()
            datePickerFragment.show(supportFragmentManager, "Date Picker")
        }

    }

    override fun set_fragment_place() {

    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        // Called when you request permission to read and write to external storage
        when (requestCode) {
            Constants.REQUEST_READ_WRITE_STORAGE_PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    launchGallery()
                } else {
                    Toast.makeText(this, "permission_denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun launchGallery() {
        mAddImages = java.util.ArrayList()
        FilePickerBuilder.instance.setMaxCount(1)
            .setSelectedFiles(mAddImages)
            .setActivityTheme(R.style.filePickerActivityTheme)
            .pickPhoto(this@SignUpProvider1Activity)
    }

    private fun pickGalleryImages() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                Constants.REQUEST_READ_WRITE_STORAGE_PERMISSION_CODE
            )
        } else
            launchGallery()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // If the image capture activity was called and was successful
        if (requestCode == FilePickerConst.REQUEST_CODE_PHOTO) {
            if (resultCode == Activity.RESULT_OK && data != null) {


                //  mAddImages!!.clear()
                mAddImages.addAll(data.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_MEDIA) as ArrayList<Uri>)

                items = data.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_MEDIA)!!
                if(items==null){
                    Log.i("signup","IMAGES=NULL")
                }else{


                    if (items.isNotEmpty()){
             /*           Glide.with(this@SignUpProvider1Activity).load(items.get(0)).placeholder(
                            R.drawable.wjez_logo_v1
                        ).error(R.drawable.wjez_logo_v2).into(ivProfile)*/

                        edit_document_image_personal.setText(resources.getText(R.string.done))
                        selectDocumentPic=true
                        TemplateActivity.photosUri.clear()
                        TemplateActivity.photosUri.add(items.get(0))
                    }

//                    for (item in items)
//                        imageAdapter.addItem(item, 0)

                }

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)

        }
    }

    override fun onValidationFailed(errors: MutableList<ValidationError>?) {
        BasicTools.showValidationErrorMessagesForFields(errors!!, this@SignUpProvider1Activity)

    }

    override fun onValidationSucceeded() {

        if(!selectDocumentPic){
            showToastMessage(R.string.select_document_image_personal)
            return
        }

        checkPhoneRQ(editphone.text.toString().trim())

    }

    fun checkPhoneRQ(txt:String){
        if (BasicTools.isConnected(this@SignUpProvider1Activity)) {



            BasicTools.showShimmer(card_next,shimmer_next,true)

            var map = HashMap<String, String>()
            map.put("phone",txt)
            val shopApi = ApiClient.getClient(BasicTools.getProtocol(this@SignUpProvider1Activity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.checkPhoneWjez(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ResponseBody>(this@SignUpProvider1Activity) {
                        override fun onSuccess(result: ResponseBody) {


                            BasicTools.showShimmer(card_next,shimmer_next,false)
                            updateDoc()



                        }
                        override fun onFailed(status: Int) {

                            BasicTools.showShimmer(card_next,shimmer_next,false)
                            if(status==400){
                                showToastMessage(R.string.check_phone_hint)
                            }else
                                showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            BasicTools.showShimmer(card_next,shimmer_next,false)
            showToastMessage(R.string.no_connection)}
    }

    @SuppressLint("SetTextI18n")
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val monthNum:Int=month+1
        editBirthday!!.setText(BasicTools.normalNumber(year.toString())+"-"
                +BasicTools.normalNumber(monthNum.toString())+"-"
                +BasicTools.normalNumber(dayOfMonth.toString()))


        /*        */



    }


    private fun getFile(uri: Uri): File?{
        val parcelFileDescriptor = contentResolver.openFileDescriptor(uri, "r", null)

        parcelFileDescriptor?.let {
            val inputStream = FileInputStream(parcelFileDescriptor.fileDescriptor)
            val file = File(cacheDir, getFileName(uri))
            val outputStream = FileOutputStream(file)

            inputStream.copyTo(outputStream)
            inputStream.close()
            outputStream.close()
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                FileUtils.copy(inputStream,outputStream)
            }*/
            return file
        }
        return null
    }

    fun getFileName(fileUri: Uri): String {

        var name = ""
        val returnCursor = contentResolver.query(fileUri, null, null, null, null)
        if (returnCursor != null) {
            val nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
            returnCursor.moveToFirst()
            name = returnCursor.getString(nameIndex)
            returnCursor.close()
        }

        return name
    }

    fun updateDoc(){
        if (BasicTools.isConnected(this@SignUpProvider1Activity)) {


            val file = getFile(TemplateActivity.photosUri.get(0))!!
            val requestFile = RequestBody.Companion.create("multipart/form-data".toMediaTypeOrNull(), file)
            val bodyImage = MultipartBody.Part.createFormData("document", file.getName(), requestFile)






            BasicTools.showShimmer(card_next,shimmer_next,true)
            val shopApi = ApiClient.getClient(
                BasicTools.getProtocol(this@SignUpProvider1Activity).toString())
                ?.create(AppApi::class.java)

            val observable= shopApi!!.uploadDocument(bodyImage)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<DocumnetIdResponse>(this@SignUpProvider1Activity) {
                        override fun onSuccess(result: DocumnetIdResponse) {





                            nameProvider=editfullName.text.toString().trim()
                            birthdayProvider=editBirthday.text.toString().trim()
                            phoneNumberProvider=editphone.text.toString().trim()
                            idNumberProvider=editIDNumber.text.toString().trim()
                            passwordProvider=editPasswrod.text.toString().trim()
                            repasswordProvider=editRePasswrod.text.toString().trim()

                            docIDProvider=result.data?.id.toString()
                            BasicTools.showShimmer(card_next,shimmer_next,false)

                            BasicTools.openActivity(this@SignUpProvider1Activity,SignUpProvider2Activity::class.java,false)








                        }
                        override fun onFailed(status: Int) {

                            BasicTools.showShimmer(card_next,shimmer_next,false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            BasicTools.showShimmer(card_next,shimmer_next,false)
            showToastMessage(R.string.no_connection)}
    }


}
