package com.wjez.app.activity.home_provider

import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem
import com.wjez.app.R
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.fragment.accountFragment.AccountFragment


import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.TemplateActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.appcheck.FirebaseAppCheck
import com.google.firebase.appcheck.safetynet.SafetyNetAppCheckProviderFactory

import com.wjez.app.fragment.accountFragmentProvider.AccountProviderFragment
import com.wjez.app.fragment.homeFragment.HomeFragment
import com.wjez.app.fragment.homeProvider.HomeProviderWjiz
import com.wjez.app.fragment.tabMyRequset.TabMyRequsetFragment
import com.wjez.app.fragment.tabMyRequsetProvider.TabMyRequsetProviderFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_home.*
import okhttp3.ResponseBody

class HomeProvider : TemplateActivity() {

    var disposable = CompositeDisposable()
    var bottomNavigation: AHBottomNavigation? = null
    override fun set_layout() {
        setContentView(R.layout.activity_home)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun init_views() {

    }

    override fun init_events() {

        initBottomNavigation()
        bottomNavigation!!.setOnTabSelectedListener { position, wasSelected ->
            // Do something cool here...
            when (position.toInt()) {
                0 -> if (!wasSelected) {
                    var f = HomeProviderWjiz()
                    show_fragment2(f, true, true)

                }
                1 -> if (!wasSelected) {

                    var fragment = TabMyRequsetProviderFragment()
                    show_fragment2(fragment, true, false)
                }
                2 -> if (!wasSelected) {

                    /*         var fragment= AccountFragment()
                             show_fragment2(fragment,true,false)*/


                }
                3 -> if (!wasSelected) {
                    var f = AccountProviderFragment()
                    show_fragment2(f, true, true)
                }


            }

            true
        }


        var f = HomeProviderWjiz()
        show_fragment2(f, true, true)


        /*    tv_view_all_trending.setOnClickListener {
    
                BasicTools.openActivity(this@HomeProvider,AllTrendings::class.java,false);
            }
            tv_categories.setOnClickListener {
                BasicTools.openActivity(this@HomeProvider,AllCategories::class.java,false);
            }*/



        FirebaseApp.initializeApp(/*context=*/ this)
        val firebaseAppCheck = FirebaseAppCheck.getInstance()
        firebaseAppCheck.installAppCheckProviderFactory(
            SafetyNetAppCheckProviderFactory.getInstance())


/*
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->


                if (!task.isSuccessful) {
                    Log.i("TEST_TEST", "" + task.exception?.message)
                    return@OnCompleteListener
                } else {

                    if (BasicTools.getToken(this@HomeProvider).isNotEmpty()) {
                        val token = task.result?.token
                        Log.i("TEST_TEST", "$token")
                        saveTokenRQ(token)
                    }
                }
            })*/


    }

    override fun set_fragment_place() {

        this.fragment_place = root_fragment_home

    }

    fun initBottomNavigation() {
        Log.e("initBottomNavigation", "success")

        bottomNavigation = findViewById(R.id.bottom_navigation) as AHBottomNavigation
        val pixels = 5 * resources.displayMetrics.density
        //collapsing_toolbar.getLayoutParams().height = (int) pixels;
        if (Build.VERSION.SDK_INT >= 21)
            bottomNavigation!!.setElevation(pixels)


        val item1 =
            AHBottomNavigationItem("", R.drawable.provider_icon1)
        val item2 =
            AHBottomNavigationItem("", R.drawable.provider_icon2)
        val item3 = AHBottomNavigationItem(
            resources.getString(R.string.account),
            R.drawable.message_wjez_icon
        )
        val item4 =
            AHBottomNavigationItem("", R.drawable.provider_icon4)


        bottomNavigation!!.addItem(item1)
        bottomNavigation!!.addItem(item2)
        bottomNavigation!!.addItem(item3)
        bottomNavigation!!.addItem(item4)



        if (BasicTools.getToken(this@HomeProvider).isEmpty())
            bottomNavigation!!.disableItemAtPosition(1)
        else bottomNavigation!!.enableItemAtPosition(1)

        //bottomNavigation.disableItemAtPosition(4);

        bottomNavigation!!.setCurrentItem(0);
        bottomNavigation!!.setBehaviorTranslationEnabled(false)

        // Manage titles
        // bottomNavigation!!.setTitleState(AHBottomNavigation.TitleState.SHOW_WHEN_ACTIVE)
        // bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        bottomNavigation!!.setTitleState(AHBottomNavigation.TitleState.ALWAYS_HIDE);


        bottomNavigation!!.setDefaultBackgroundColor(fetchColor(R.color.bg_naigation))
        bottomNavigation!!.setAccentColor(fetchColor(R.color.white))
        bottomNavigation!!.setInactiveColor(fetchColor(R.color.login_btn))


        bottomNavigation!!.currentItem = 0


        //bottomNavigation!!.isColored = true


        /* notification = AHNotification.Builder()
             .setText("")
             .setBackgroundColor(ContextCompat.getColor(this@MainActivity, R.color.main_orange))
             .setTextColor(ContextCompat.getColor(this@MainActivity, R.color.tabs_white))
             .build()
         bottomNavigation.setNotification(notification, 3)*/

    }


    fun saveTokenRQ(token: String?) {
        if (BasicTools.isConnected(this@HomeProvider)) {

            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@HomeProvider),
                BasicTools.getProtocol(this@HomeProvider).toString()
            )
                ?.create(AppApi::class.java)

           // val id: String = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)


            var map = HashMap<String, String>()
            map.put("token", "$token")

            val observable = shopApi!!.saveDevicesToken(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ResponseBody>(this@HomeProvider) {
                        override fun onSuccess(result: ResponseBody) {

                        }

                        override fun onFailed(status: Int) {
                            //BasicTools.logOut(parent!!)
                        }
                    })
            )

        } else {


        }
    }

}



