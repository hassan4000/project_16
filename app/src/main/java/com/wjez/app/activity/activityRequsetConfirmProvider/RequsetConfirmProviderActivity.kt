package com.wjez.app.activity.activityRequsetConfirmProvider


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.wjez.app.R
import com.wjez.app.activity.home_provider.HomeProvider
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.TemplateActivity
import kotlinx.android.synthetic.main.activity_request_confirmation_v2.*

class RequsetConfirmProviderActivity : TemplateActivity() {
    override fun set_layout() {
        setContentView(R.layout.activity_request_confirmation_v2)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun init_views() {

    }

    override fun init_events() {

        card_next.setOnClickListener {
            BasicTools.clearAllActivity(this,HomeProvider::class.java)
        }



    }

    override fun set_fragment_place() {

    }


}
