package com.wjez.app.activity.addSerivcesCustomerAcitivty


import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.widget.EditText
import android.widget.Spinner
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import com.wjez.app.R
import com.wjez.app.activity.homeActivity.HomeActivity
import com.wjez.app.activity.selcectLocationAddRequset.SelcectLocationAddRequsetActivity
import com.wjez.app.activity.updateLocationCustomerActivity.UpdateLocationCustomerActivity
import com.wjez.app.adapter.CountrySpinnerAdapter
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.model.project16.CountryModel
import com.wjez.app.model.project16.CountryResponseModel
import com.wjez.app.model.project16.UserInfoResponse
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.Constants
import com.wjez.app.tools.TemplateActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_add_requset.*
import kotlinx.android.synthetic.main.activity_add_requset.tv_address
import kotlinx.android.synthetic.main.toolbar_add_requset.*



class AddServicesActivity : TemplateActivity() , Validator.ValidationListener{

    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mLocationPermissionGranted = false

    var disposable= CompositeDisposable()
    var validator: Validator? = null

    lateinit var spinnerAdapter: CountrySpinnerAdapter

    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    lateinit var editfullName: EditText

    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    lateinit var editphone: EditText


    override fun set_layout() {
        setContentView(R.layout.activity_add_requset)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun init_views() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this@AddServicesActivity)

        editfullName=edit_full_name
        editphone=edit_phone_number

        spinnerAdapter= CountrySpinnerAdapter(this@AddServicesActivity, ArrayList())
        spinner_cities.adapter=spinnerAdapter

    }

    @SuppressLint("SetTextI18n")
    override fun init_events() {

        validator = Validator(this)
        validator!!.setValidationListener(this)

        root_location.setOnClickListener {
            if (checkMapServices()) {
                if (!mLocationPermissionGranted) {
                    getLocationPermission() // get Permission
                } else {
                    //open map



                    BasicTools.openActivity(this,SelcectLocationAddRequsetActivity::class.java,false)

                }
            }
        }

        card_next.setOnClickListener {

            validator!!.validate()

        }


        iv_back.setOnClickListener {
            BasicTools.exitActivity(this)
        }


       if(BasicTools.isDeviceLanEn()){
        tv_title_toolbar.setText("${selectedCategory?.name}  ${resources.getString(R.string.requset)}")
    }    else{
       tv_title_toolbar.setText("${resources.getString(R.string.requset)} ${selectedCategory?.nameAr}")
    }

        getCities()

    }

    override fun set_fragment_place() {

    }

    fun buildAlertMessageNoGps() {
        val builder = AlertDialog.Builder(this@AddServicesActivity) //alert dialog object (yes,no)
        builder.setMessage(resources.getString(R.string.gps_hint))
            .setCancelable(true)
            .setPositiveButton(resources.getString(R.string.yes), DialogInterface.OnClickListener { dialog, id ->
                // K Button
                val enableGpsIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivityForResult(enableGpsIntent, Constants.PERMISSIONS_REQUEST_ENABLE_GPS)
            }).setNegativeButton(resources.getString(R.string.no), DialogInterface.OnClickListener{ dialog, id ->
                dialog.dismiss()
            })
        val alert = builder.create()
        alert.show()

    }

    fun isServicesOK(): Boolean {
        Log.d("TEST_TEST", "isServicesOK: checking google services version")

        val available =
            GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this@AddServicesActivity)

        if (available == ConnectionResult.SUCCESS) {
            //everything is fine and the user can make map requests
            Log.d("TEST_TEST", "isServicesOK: Google Play Services is working")
            return true
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            //an error occured but we can resolve it
            Log.d("TEST_TEST", "isServicesOK: an error occured but we can fix it")
            val dialog = GoogleApiAvailability.getInstance()
                .getErrorDialog(this@AddServicesActivity, available, Constants.ERROR_DIALOG_REQUEST)
            dialog!!.show()
        } else {
            this@AddServicesActivity.showToastMessage("You can't make map requests")
        }
        return false
    }

    fun isMapsEnabled(): Boolean {

        //get LOCATION_SERVICE object : service state
        val manager =this@AddServicesActivity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        //if the GPS service not enable show the alert Dialog
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps()
            return false
        }
        return true
    }




    fun checkMapServices(): Boolean {
        if (isServicesOK()) {  //
            if (isMapsEnabled()) {
                return true
            }
        }
        return false

    }

    fun getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         */
        if (ContextCompat.checkSelfPermission(
                this@AddServicesActivity,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) { //PERMISSION_GRANTED : the device accept it the fist run at  lolipop and above and accept in installation below lolipop
            mLocationPermissionGranted = true //GPS Permission is ok

            getLastKnownLocation()



            BasicTools.openActivity(this,SelcectLocationAddRequsetActivity::class.java,false)

        } else { //runtime Permission
            ActivityCompat.requestPermissions(
                this@AddServicesActivity,
                arrayOf<String>(android.Manifest.permission.ACCESS_FINE_LOCATION),
                Constants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }

    }

    fun getLastKnownLocation() {
        Log.d("TEST_TEST", "getLastKnownLocation: called.")
        if (ActivityCompat.checkSelfPermission(
                this@AddServicesActivity,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this@AddServicesActivity,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            return
        }
        mFusedLocationClient!!.getLastLocation().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val location = task.result
                if (location != null) {
                    // myloc = location
                }

                if (location != null) {
                    Log.d("TEST_TEST", "onComplete: latitude: " + location.latitude)
                    Log.d("TEST_TEST", "onComplete: longitude: " + location.longitude)
                }
            }
        }


    }


    override fun onResume() {
        Log.i("TEST_TEST","onResume")
        tv_address.setText(addServicesaddress)
        super.onResume()
    }


    override fun onStart() {
        Log.i("TEST_TEST","onStart")
        super.onStart()
    }

    override fun onPause() {
        Log.i("TEST_TEST","onPause")
        super.onPause()
    }

    override fun onValidationFailed(errors:List<ValidationError>?) {
        BasicTools.showValidationErrorMessagesForFields(errors!!, this@AddServicesActivity)

    }

    override fun onValidationSucceeded() {

        if(addServicesLat==null) {

            showToastMessage(R.string.plz_select_location)
            return
        }


        if(spinner_cities.selectedItem==null) {

            showToastMessage(R.string.plz_select_location)
            return
        }


        addServicesPhone=editphone.text.toString().trim()
        addServicesName=editfullName.text.toString().trim()
        addServicesCitiesID=((spinner_cities.selectedItem) as CountryModel).countryId
        BasicTools.openActivity(this@AddServicesActivity,AddServicesActivityV2::class.java,false)



    }

    fun getCities(){
        if (BasicTools.isConnected(this@AddServicesActivity)) {
            BasicTools.showShimmer(spinner_cities,shimmer_wait_spinner,true)


            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@AddServicesActivity),
                BasicTools.getProtocol(this@AddServicesActivity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getCountryWjez()
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<CountryResponseModel>(this@AddServicesActivity) {
                        override fun onSuccess(result: CountryResponseModel) {
                            BasicTools.showShimmer(spinner_cities,shimmer_wait_spinner,false)

                            spinnerAdapter= CountrySpinnerAdapter(this@AddServicesActivity,result.data!!)
                            spinner_cities.adapter=spinnerAdapter

                        }
                        override fun onFailed(status: Int) {

                            BasicTools.showShimmer(spinner_cities,shimmer_wait_spinner,false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            BasicTools.showShimmer(spinner_cities,shimmer_wait_spinner,false)
            showToastMessage(R.string.no_connection)}
    }





}
