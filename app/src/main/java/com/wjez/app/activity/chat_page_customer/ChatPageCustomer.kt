package com.wjez.app.activity.chat_page_customer


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.wjez.app.R
import com.wjez.app.tools.TemplateActivity

class ChatPageCustomer : TemplateActivity() {
    override fun set_layout() {
        setContentView(R.layout.activity_chat_page)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun init_views() {

    }

    override fun init_events() {

    }

    override fun set_fragment_place() {

    }


}
