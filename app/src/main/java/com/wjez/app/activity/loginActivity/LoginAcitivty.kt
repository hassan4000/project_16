package com.wjez.app.activity.loginActivity


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.widget.EditText
import com.wjez.app.R

import com.wjez.app.activity.forgotPassword.SelectWayActivity
import com.wjez.app.activity.homeActivity.HomeActivity
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.model.project21.LoginResposeModel
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.BasicTools.openActivity
import com.wjez.app.tools.BasicTools.showValidationErrorMessagesForFields
import com.wjez.app.tools.Constants
import com.wjez.app.tools.TemplateActivity
import com.wjez.shop.tools.hide
import com.wjez.shop.tools.visible




import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.hbb20.CountryCodePicker
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import com.wjez.app.activity.home_provider.HomeProvider
import com.wjez.app.activity.provider.signup_provider.SignUpProvider1Activity
import com.wjez.app.activity.signUp.SignUpAcitivty
import com.wjez.app.fragment.homeProvider.HomeProviderWjiz
import com.wjez.app.model.project16.TokenResponse
import com.wjez.app.model.project16.UserInfoDataResponse
import com.wjez.app.model.project16.UserInfoResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*


class LoginAcitivty : TemplateActivity(), Validator.ValidationListener {
    var validator: Validator? = null
    lateinit var countryCode: CountryCodePicker
    lateinit var firebaseAuth: FirebaseAuth
    lateinit var refernce: DatabaseReference


    lateinit var mGoogleSignClient: GoogleSignInClient

  /*  @Email(sequence = 2, messageResId = R.string.enter_a_valid_email_address)*/
   @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    lateinit var edituserName: EditText

    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    lateinit var editPassword: EditText

    var disposable:CompositeDisposable=CompositeDisposable()
    var disposableProivder:CompositeDisposable=CompositeDisposable()
    override fun set_layout() {
        setContentView(R.layout.activity_login)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }


    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account: GoogleSignInAccount = completedTask.getResult(ApiException::class.java)!!


            Log.i("TEST_TEST",account.email!!)
            Log.i("TEST_TEST",account.id!!)
            Log.i("TEST_TEST",account.displayName!!)
            Log.i("TEST_TESTPhoto",account.photoUrl.toString())

            TemplateActivity.socialImage=account.photoUrl.toString()


           // loginRQ(Constants.LOGIN_TYPE_GMAIL,account.email!!,account.id.toString())
            // Signed in successfully, show authenticated UI.

        } catch (e: ApiException) {
            Log.i("TEST_TEST_error",e.printStackTrace().toString())

            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.


        }
    }
    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {




      //  callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)




        if (requestCode == 7000) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.


            val task =
                GoogleSignIn.getSignedInAccountFromIntent(data)

            handleSignInResult(task)

        }
    }

    fun showShimmerLoginBtn(state: Boolean){

        if(state){
            card_login.visibility= View.GONE
            shimmer_login.visible()
        }else{
            card_login.visibility= View.VISIBLE
            shimmer_login.hide()
        }
    }

    override fun init_views() {

        countryCode=country_code

        iv_back_login.setOnClickListener {
            BasicTools.exitActivity(this)
        }


        edituserName=edit_email
        editPassword=edit_password




    }







    override fun init_events() {

        firebaseAuth=FirebaseAuth.getInstance()

        val gso= GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        mGoogleSignClient = GoogleSignIn.getClient(this, gso)


        iv_sign_google.setOnClickListener {
            mGoogleSignClient.signOut().addOnCompleteListener {

                val signInIntent: Intent = mGoogleSignClient.getSignInIntent()
                startActivityForResult(signInIntent, 7000)
            }






        }



   //     BasicTools.setProtocol(this@LoginAcitivty,"http://")
        validator = Validator(this)
        validator!!.setValidationListener(this)



        tv_sign_up.setOnClickListener {

            TemplateActivity.SignUpModel.smsCode=""
            if(TemplateActivity.loginType.equals(Constants.LOGIN_TYPE_CONSTOMER))
            openActivity(this@LoginAcitivty,SignUpAcitivty::class.java,false)

            else  if(TemplateActivity.loginType.equals(Constants.LOGIN_TYPE_PROVIDER)) {
                openActivity(this@LoginAcitivty,SignUpProvider1Activity::class.java,false)
            }
        }
        card_login.setOnClickListener {

            validator!!.validate()
      //      openActivity(this@LoginAcitivty,EnterPhoneNumberAcivity::class.java,false)
        }


        eye_password.setOnClickListener {
            if(edit_password.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){


                eye_password.setImageResource(R.drawable.visibility_3x)
                edit_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                eye_password.setImageResource(R.drawable.visibility_off_3x)
                edit_password.setTransformationMethod(PasswordTransformationMethod.getInstance());

            }
        }




        iv_facebook.setOnClickListener {
            showShimmerLoginBtn(true)





        }


        skip.setOnClickListener {

            BasicTools.clearAllActivity(this@LoginAcitivty,HomeActivity::class.java)
        }




        tv_forgot_password.setOnClickListener {
            BasicTools.openActivity(this@LoginAcitivty,SelectWayActivity::class.java,false)
        }


 /*       editEmail=edit_email_login
        editPassword=edit_password_login*/




     /*   btn_login.setOnClickListener{
            validator!!.validate()
        }*/

    }





    override fun set_fragment_place() {

    }





    override fun onValidationFailed(errors: MutableList<ValidationError>?) {
        showValidationErrorMessagesForFields(errors!!, this@LoginAcitivty)
    }

    override fun onValidationSucceeded() {
/*
         var auth: FirebaseAuth
        auth = FirebaseAuth.getInstance()
        val firebaseAuthSettings = auth.firebaseAuthSettings
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            "+963993461719", // Phone number to verify
            60, // Timeout duration
            TimeUnit.SECONDS, // Unit of timeout
            this, // Activity (for callback binding)
            object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                    // This callback will be invoked in two situations:
                    // 1 - Instant verification. In some cases the phone number can be instantly
                    //     verified without needing to send or enter a verification code.
                    // 2 - Auto-retrieval. On some devices Google Play services can automatically
                    //     detect the incoming verification SMS and perform verification without
                    //     user action.
                    Log.d("TAG", "onVerificationCompleted:$credential")

                    auth.signInWithCredential(credential).addOnCompleteListener(this@LoginAcitivty) { task ->
                            if (task.isSuccessful) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d("TAG", "signInWithCredential:success")

                                loginRQ()

                                val user = task.result?.user
                                // ...
                            } else {
                                // Sign in failed, display a message and update the UI
                                Log.w("TAG", "signInWithCredential:failure", task.exception)
                                if (task.exception is FirebaseAuthInvalidCredentialsException) {
                                    // The verification code entered was invalid
                                }
                            }
                        }



                    //  signInWithPhoneAuthCredential(credential)
                }




                override fun onVerificationFailed(e: FirebaseException) {
                    // This callback is invoked in an invalid request for verification is made,
                    // for instance if the the phone number format is not valid.
                    Log.w("TAG", "onVerificationFailed", e)

                    if (e is FirebaseAuthInvalidCredentialsException) {
                        Log.i("TEST_TEST",e.message.toString())
                        // Invalid request
                        // ...
                    } else if (e is FirebaseTooManyRequestsException) {

                        // The SMS quota for the project has been exceeded
                        // ...
                    }

                    // Show a message and update the UI
                    // ...
                }

                override fun onCodeSent(
                    verificationId: String,
                    token: PhoneAuthProvider.ForceResendingToken
                ) {
                    // The SMS verification code has been sent to the provided phone number, we
                    // now need to ask the user to enter the code and then construct a credential
                    // by combining the code with a verification ID.
                    Log.d("TAG", "onCodeSent:$verificationId")

                    // Save verification ID and resending token so we can use them later
                    // storedVerificationId = verificationId
                    //  resendToken = token

                    Log.i("TEST_TEST","token"+token)
                    Log.i("TEST_TEST","verificationId"+verificationId)




                    // ...
                }
            })*/




        if(TemplateActivity.loginType.equals(Constants.LOGIN_TYPE_CONSTOMER))
        loginRQ()
        else if(TemplateActivity.loginType.equals(Constants.LOGIN_TYPE_PROVIDER))
            loginRQProvider()
    }




    fun getUserInfo(){
        if (BasicTools.isConnected(this@LoginAcitivty)) {



            showShimmerLoginBtn(true)


            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@LoginAcitivty),  BasicTools.getProtocol(this@LoginAcitivty).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getUserInfo()
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<UserInfoResponse>(this@LoginAcitivty) {
                        override fun onSuccess(result: UserInfoResponse) {


                            showShimmerLoginBtn(false)

                            TemplateActivity.wjezUser=result

                            BasicTools.setUserID(this@LoginAcitivty,result.id.toString())
                            BasicTools.setLoginProviderCustomer(this@LoginAcitivty,result.userType.toString())

                            if(result.userType.equals(Constants.LOGIN_TYPE_CONSTOMER))
                                BasicTools.openActivity(this@LoginAcitivty,HomeActivity::class.java,true)
                            else if(result.userType.equals(Constants.LOGIN_TYPE_PROVIDER))
                                BasicTools.openActivity(this@LoginAcitivty,HomeProvider::class.java,true)


                        /*    firebaseAuth.signInWithEmailAndPassword(edituserName.text.toString(),editPassword.text.toString()).
                            addOnCompleteListener {

                                if(it.isSuccessful){

                                    if(result.userType.equals(Constants.LOGIN_TYPE_CONSTOMER))
                                        BasicTools.openActivity(this@LoginAcitivty,HomeActivity::class.java,true)
                                    else if(result.userType.equals(Constants.LOGIN_TYPE_PROVIDER))
                                        BasicTools.openActivity(this@LoginAcitivty,HomeProvider::class.java,true)

                                }
                                else{
                                    showToastMessage(it.exception.toString())
                                }
                            }
*/


                        }
                        override fun onFailed(status: Int) {

                            showShimmerLoginBtn(false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            showShimmerLoginBtn(false)
            showToastMessage(R.string.no_connection)}
    }



    fun getUserInfoProvider(){
        if (BasicTools.isConnected(this@LoginAcitivty)) {



            showShimmerLoginBtn(true)


            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@LoginAcitivty),  BasicTools.getProtocol(this@LoginAcitivty).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getUserProviderInfo()
            disposableProivder.clear()
            disposableProivder.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<UserInfoDataResponse>(this@LoginAcitivty) {
                        override fun onSuccess(result: UserInfoDataResponse) {


                            showShimmerLoginBtn(false)

                            TemplateActivity.wjezUser=result.data
                            BasicTools.setUserID(this@LoginAcitivty,result.data?.id.toString())
                            BasicTools.setLoginProviderCustomer(this@LoginAcitivty,result.data!!.userType.toString())


                            if(result.data!!.userType.toString().equals(Constants.LOGIN_TYPE_CONSTOMER))
                                BasicTools.openActivity(this@LoginAcitivty,HomeActivity::class.java,true)
                            else if(result.data!!.userType.toString().equals(Constants.LOGIN_TYPE_PROVIDER))
                                BasicTools.openActivity(this@LoginAcitivty,HomeProvider::class.java,true)





                /*            firebaseAuth.signInWithEmailAndPassword(edituserName.text.toString(),editPassword.text.toString()).
                            addOnCompleteListener {

                                if(it.isSuccessful){

*//*

                                    if(result.data!!.userType.toString().equals(Constants.LOGIN_TYPE_CONSTOMER))
                                        BasicTools.openActivity(this@LoginAcitivty,HomeActivity::class.java,true)
                                    else if(result.data!!.userType.toString().equals(Constants.LOGIN_TYPE_PROVIDER))
                                        BasicTools.openActivity(this@LoginAcitivty,HomeProvider::class.java,true)
*//*


                                }
                                else{
                                    showToastMessage(it.exception.toString())
                                }
                            }*/


                        }
                        override fun onFailed(status: Int) {

                            showShimmerLoginBtn(false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            showShimmerLoginBtn(false)
            showToastMessage(R.string.no_connection)}
    }


    fun loginRQ(){
        if (BasicTools.isConnected(this@LoginAcitivty)) {





            var map = HashMap<String, String>()
            map.put("password",editPassword.text.trim().toString())
            map.put("phone",edituserName.text.trim().toString())

            showShimmerLoginBtn(true)
            val shopApi = ApiClient.getClient(BasicTools.getProtocol(this@LoginAcitivty).toString())
                ?.create(AppApi::class.java)

            val observable= shopApi!!.loginNormal(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<TokenResponse>(this@LoginAcitivty) {
                        override fun onSuccess(result: TokenResponse) {


                            showShimmerLoginBtn(false)
                        if(result.status!!){

                          //  TemplateActivity.loginResponse=result
                           // BasicTools.setIsSocial(this@LoginAcitivty,false)
                          //  BasicTools.setLoginType(this@LoginAcitivty,Constants.LOGIN_TYPE_NORMAL)
                            BasicTools.setToken(this@LoginAcitivty,result.token!!.toString())
                         //   BasicTools.setUserName(this@LoginAcitivty,result.data?.email!!.toString())
                            BasicTools.setPhoneUser(this@LoginAcitivty,edituserName.text.toString().trim())
                            BasicTools.setPassword(this@LoginAcitivty,editPassword.text.trim().toString())

                            if(TemplateActivity.loginType.equals(Constants.LOGIN_TYPE_CONSTOMER))
                                getUserInfo()
                            else if(TemplateActivity.loginType.equals(Constants.LOGIN_TYPE_PROVIDER))
                                getUserInfoProvider()


                        }
                            else{
                            showShimmerLoginBtn(false)
                            BasicTools.logOut(this@LoginAcitivty)
                            showToastMessage(R.string.faild_to_login)
                        }


                        }
                        override fun onFailed(status: Int) {

                            showShimmerLoginBtn(false)
                            BasicTools.logOut(this@LoginAcitivty)
                            showToastMessage(R.string.faild_to_login)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            showShimmerLoginBtn(false)
            showToastMessage(R.string.no_connection)}
    }


    fun loginRQProvider(){
        if (BasicTools.isConnected(this@LoginAcitivty)) {





            var map = HashMap<String, String>()
            map.put("password",editPassword.text.trim().toString())
            map.put("phone",edituserName.text.trim().toString())

            showShimmerLoginBtn(true)
            val shopApi = ApiClient.getClient(BasicTools.getProtocol(this@LoginAcitivty).toString())
                ?.create(AppApi::class.java)

            val observable= shopApi!!.loginProvider(map)
            disposableProivder.clear()
            disposableProivder.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<TokenResponse>(this@LoginAcitivty) {
                        override fun onSuccess(result: TokenResponse) {


                            showShimmerLoginBtn(false)
                            if(result.status!!){

                                //  TemplateActivity.loginResponse=result
                                // BasicTools.setIsSocial(this@LoginAcitivty,false)
                                //  BasicTools.setLoginType(this@LoginAcitivty,Constants.LOGIN_TYPE_NORMAL)
                                BasicTools.setToken(this@LoginAcitivty,result.token!!.toString())
                                //   BasicTools.setUserName(this@LoginAcitivty,result.data?.email!!.toString())
                                BasicTools.setPhoneUser(this@LoginAcitivty,edituserName.text.toString().trim())
                                BasicTools.setPassword(this@LoginAcitivty,editPassword.text.trim().toString())
                                if(TemplateActivity.loginType.equals(Constants.LOGIN_TYPE_CONSTOMER))
                                getUserInfo()
                                else if(TemplateActivity.loginType.equals(Constants.LOGIN_TYPE_PROVIDER))
                                    getUserInfoProvider()

                            }
                            else{
                                showShimmerLoginBtn(false)
                                BasicTools.logOut(this@LoginAcitivty)
                                showToastMessage(R.string.faild_to_login)
                            }


                        }
                        override fun onFailed(status: Int) {

                            showShimmerLoginBtn(false)
                            BasicTools.logOut(this@LoginAcitivty)
                            showToastMessage(R.string.faild_to_login)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            showShimmerLoginBtn(false)
            showToastMessage(R.string.no_connection)}
    }






/*
    fun loginRQ(loginType:String,email:String,providerID:String){
        if (BasicTools.isConnected(this@LoginAcitivty)) {





            var map = HashMap<String, String>()
            map.put("provider_id",providerID)
            map.put("provider_name",loginType)
            map.put("email",email)

            showShimmerLoginBtn(true)
            val shopApi = ApiClient.getClient(BasicTools.getProtocol(this@LoginAcitivty).toString())
                ?.create(AppApi::class.java)

            val observable= shopApi!!.loginSocial(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<LoginResposeModel>(this@LoginAcitivty) {
                        override fun onSuccess(result: LoginResposeModel) {



                            if(result.status!!){
                                showShimmerLoginBtn(false)
                                TemplateActivity.loginResponse=result
                                BasicTools.setSocialProviderID(this@LoginAcitivty,providerID)
                                BasicTools.setIsSocial(this@LoginAcitivty,true)
                                BasicTools.setLoginType(this@LoginAcitivty,loginType)
                                BasicTools.setToken(this@LoginAcitivty,result.data?.token!!.toString())
                                BasicTools.setUserName(this@LoginAcitivty,result.data?.email!!.toString())
                                BasicTools.setPhoneUser(this@LoginAcitivty,result.data?.phone!!.toString())
                                BasicTools.setPassword(this@LoginAcitivty,editPassword.text.trim().toString())
                                BasicTools.openActivity(this@LoginAcitivty,HomeActivity::class.java,true)
                            }
                            else{
                                showShimmerLoginBtn(false)
                                BasicTools.logOut(this@LoginAcitivty)
                                showToastMessage(R.string.faild_to_login)
                            }


                        }
                        override fun onFailed(status: Int) {

                            showShimmerLoginBtn(false)
                            BasicTools.logOut(this@LoginAcitivty)
                            showToastMessage(R.string.faild_to_login)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            showShimmerLoginBtn(false)
            showToastMessage(R.string.no_connection)}
    }*/


}
