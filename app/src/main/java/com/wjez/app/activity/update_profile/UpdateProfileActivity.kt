package com.wjez.app.activity.update_profile


import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.OpenableColumns
import android.util.Log
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.annotation.Email
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import com.wjez.app.R
import com.wjez.app.activity.homeActivity.HomeActivity
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.model.project16.UserInfoResponse
import com.wjez.app.tools.*
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_signup_wjez.*
import kotlinx.android.synthetic.main.activity_update_profile.*
import kotlinx.android.synthetic.main.activity_update_profile.edit_full_name
import kotlinx.android.synthetic.main.activity_update_profile.edit_phone_number

import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class UpdateProfileActivity : TemplateActivity(),Validator.ValidationListener {
    var selectImage=false
    var disposable= CompositeDisposable()
    private var mAddImages =  ArrayList<Uri>()
    var items=ArrayList<Uri>()

    private var anim: Animation? = null
    var validator: Validator? = null

    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    lateinit var editfullName: EditText

    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    lateinit var editphone: EditText

    lateinit var tvEditImage:TextView
    @Email(sequence = 1, messageResId = R.string.enter_a_valid_email_address)
    lateinit var editEmail:EditText
    override fun set_layout() {
        setContentView(R.layout.activity_update_profile)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun init_views() {

        tvEditImage=tv_edit_image
        validator = Validator(this)
        validator!!.setValidationListener(this)

        editfullName=edit_full_name
        editphone=edit_phone_number
        editEmail=edit_email

        editfullName.setText(TemplateActivity.wjezUser?.name)
        editphone.setText(TemplateActivity.wjezUser?.phone)
        editEmail.setText(TemplateActivity.wjezUser?.email )

        iv_back.setOnClickListener {
            BasicTools.exitActivity(this)
        }
        tv_title_toolbar.setText(resources.getText(R.string.edit_info))

    }

    override fun init_events() {

        tvEditImage.setOnClickListener {

            pickGalleryImages()
        }

        card_save.setOnClickListener {

            validator!!.validate()
        }

        if (TemplateActivity.wjezUser!!.image != null){
            BasicTools.loadImage(
                BasicTools.getUrlImg(this@UpdateProfileActivity, TemplateActivity.wjezUser!!.image!!.toString()),
                iv_profile_user,
                object : DownloadListener {
                    override fun completed(status: Boolean, bitmap: Bitmap) {
                        anim = AnimationUtils.loadAnimation(this@UpdateProfileActivity, android.R.anim.fade_in)
                        anim!!.setDuration(1000)
                        iv_profile_user.animation = anim
                    }
                })

        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        // Called when you request permission to read and write to external storage
        when (requestCode) {
            Constants.REQUEST_READ_WRITE_STORAGE_PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    launchGallery()
                } else {
                    Toast.makeText(this, "permission_denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun pickGalleryImages() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                Constants.REQUEST_READ_WRITE_STORAGE_PERMISSION_CODE
            )
        } else
            launchGallery()
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // If the image capture activity was called and was successful
        if (requestCode == FilePickerConst.REQUEST_CODE_PHOTO) {
            if (resultCode == Activity.RESULT_OK && data != null) {


                //  mAddImages!!.clear()
                mAddImages.addAll(data.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_MEDIA) as ArrayList<Uri>)

                items = data.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_MEDIA)!!
                if(items==null){
                    Log.i("signup","IMAGES=NULL")
                }else{


                    if (items.isNotEmpty()){
                        Glide.with(this@UpdateProfileActivity).load(items.get(0)).placeholder(
                            R.drawable.wjez_logo_v2
                        ).error(R.drawable.wjez_logo_v2).into(iv_profile_user)
                        TemplateActivity.photosUri.clear()
                        TemplateActivity.photosUri.add(items.get(0))
                        selectImage=true
                    }

//                    for (item in items)
//                        imageAdapter.addItem(item, 0)

                }

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)

        }
    }

    private fun launchGallery() {
        mAddImages = java.util.ArrayList()
        FilePickerBuilder.instance.setMaxCount(1)
            .setSelectedFiles(mAddImages)
            .setActivityTheme(R.style.filePickerActivityTheme)
            .pickPhoto(this@UpdateProfileActivity)
    }

    override fun set_fragment_place() {

    }

    override fun onValidationFailed(errors: MutableList<ValidationError>?) {
        BasicTools.showValidationErrorMessagesForFields(errors!!, this@UpdateProfileActivity)

    }

    override fun onValidationSucceeded() {

        if(TemplateActivity.wjezUser!!.phone.equals(editphone.text.toString().trim()))

            if(!editEmail.text.toString().trim().equals(TemplateActivity.wjezUser!!.email.toString().trim()))
            checkEmailRQ(editEmail.text.toString().trim())
        else checkIfSelected()
        else{
            checkPhoneRQ(editphone.text.toString().trim())
        }
    }


    fun checkIfSelected(){
        if(selectImage)
            updateInfoWithPhoto()
        else updateInfo()
    }


    fun getUserInfo(){
        if (BasicTools.isConnected(this@UpdateProfileActivity)) {



            BasicTools.showShimmer(card_save,shimmer_wait,true)



            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@UpdateProfileActivity),  BasicTools.getProtocol(this@UpdateProfileActivity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getUserInfo()
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<UserInfoResponse>(this@UpdateProfileActivity) {
                        override fun onSuccess(result: UserInfoResponse) {

                            BasicTools.showShimmer(card_save,shimmer_wait,false)


                            TemplateActivity.wjezUser=result

                            BasicTools.clearAllActivity(this@UpdateProfileActivity,
                                HomeActivity::class.java)


                        }
                        override fun onFailed(status: Int) {

                            BasicTools.showShimmer(card_save,shimmer_wait,false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            BasicTools.showShimmer(card_save,shimmer_wait,false)
            showToastMessage(R.string.no_connection)}
    }

    fun checkPhoneRQ(txt:String){
        if (BasicTools.isConnected(this@UpdateProfileActivity)) {



            BasicTools.showShimmer(card_save,shimmer_wait,true)

            var map = HashMap<String, String>()
            map.put("phone",txt)
            val shopApi = ApiClient.getClient(BasicTools.getProtocol(this@UpdateProfileActivity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.checkPhoneWjez(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ResponseBody>(this@UpdateProfileActivity) {
                        override fun onSuccess(result: ResponseBody) {


                            BasicTools.showShimmer(card_save,shimmer_wait,false)


                            if(!editEmail.text.isNullOrEmpty()){

                                Log.i("TEST_TEST","${editEmail.text.toString().trim()}")
                                Log.i("TEST_TEST","${TemplateActivity.wjezUser!!.email.toString().trim()}")
                                if(!editEmail.text.toString().trim().equals(TemplateActivity.wjezUser!!.email.toString().trim()))
                                    checkEmailRQ(editEmail.text.toString().trim())
                                else checkIfSelected()

                            }else{

                                checkIfSelected()
                            }


                        }
                        override fun onFailed(status: Int) {

                            BasicTools.showShimmer(card_save,shimmer_wait,false)
                            if(status==400){
                                showToastMessage(R.string.check_phone_hint)
                            }else
                                showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            BasicTools.showShimmer(card_save,shimmer_wait,false)
            showToastMessage(R.string.no_connection)}
    }


    fun checkEmailRQ(txt:String){
        if (BasicTools.isConnected(this@UpdateProfileActivity)) {



            BasicTools.showShimmer(card_save,shimmer_wait,true)

            var map = HashMap<String, String>()
            map.put("email",txt)
            val shopApi = ApiClient.getClient(BasicTools.getProtocol(this@UpdateProfileActivity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.checkEmailWjez(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ResponseBody>(this@UpdateProfileActivity) {
                        override fun onSuccess(result: ResponseBody) {


                            BasicTools.showShimmer(card_save,shimmer_wait,false)
                            checkIfSelected()



                        }
                        override fun onFailed(status: Int) {

                            BasicTools.showShimmer(card_save,shimmer_wait,false)
                            if(status==400){
                                showToastMessage(R.string.check_email_hint)
                            }else
                                showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            BasicTools.showShimmer(card_save,shimmer_wait,false)
            showToastMessage(R.string.no_connection)}
    }




    fun updateInfoWithPhoto(){
        if (BasicTools.isConnected(this@UpdateProfileActivity)) {


            val file = getFile(TemplateActivity.photosUri.get(0))!!
            val requestFile = RequestBody.Companion.create("multipart/form-data".toMediaTypeOrNull(), file)
            val bodyImage = MultipartBody.Part.createFormData("image", file.getName(), requestFile)


            var map = HashMap<String, String>()
            //  map.put("password",editPasswrod.text.toString().trim())
            if(!editEmail.text.isNullOrEmpty())
            map.put("email",editEmail.text.toString().trim())
            map.put("name",editfullName.text.toString().trim())
            map.put("phone",editphone.text.toString().trim())
            //    map.put("password_confirmation",editRePassword.text.toString().trim())


            BasicTools.showShimmer(card_save,shimmer_wait,true)
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@UpdateProfileActivity),
                BasicTools.getProtocol(this@UpdateProfileActivity).toString())
                ?.create(AppApi::class.java)

            val observable= shopApi!!.updateProfileWithImage(bodyImage,map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ResponseBody>(this@UpdateProfileActivity) {
                        override fun onSuccess(result: ResponseBody) {



                            BasicTools.showShimmer(card_save,shimmer_wait,false)

                            TemplateActivity.wjezUser?.name=editfullName.text.toString().trim()
                            TemplateActivity.wjezUser?.phone=editphone.text.toString().trim()
                            TemplateActivity.wjezUser?.email=editEmail.text.toString().trim()


                            getUserInfo()






                        }
                        override fun onFailed(status: Int) {

                            BasicTools.showShimmer(card_save,shimmer_wait,false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            BasicTools.showShimmer(card_save,shimmer_wait,false)
            showToastMessage(R.string.no_connection)}
    }

    fun updateInfo(){
        if (BasicTools.isConnected(this@UpdateProfileActivity)) {


            var map = HashMap<String, String>()
            //  map.put("password",editPasswrod.text.toString().trim())
            if(!editEmail.text.isNullOrEmpty())
                map.put("email",editEmail.text.toString().trim())
            map.put("name",editfullName.text.toString().trim())
            map.put("phone",editphone.text.toString().trim())
            //    map.put("password_confirmation",editRePassword.text.toString().trim())


            BasicTools.showShimmer(card_save,shimmer_wait,true)
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@UpdateProfileActivity),
                BasicTools.getProtocol(this@UpdateProfileActivity).toString())
                ?.create(AppApi::class.java)

            val observable= shopApi!!.updateProfile(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ResponseBody>(this@UpdateProfileActivity) {
                        override fun onSuccess(result: ResponseBody) {



                            BasicTools.showShimmer(card_save,shimmer_wait,false)

                            TemplateActivity.wjezUser?.name=editfullName.text.toString().trim()
                            TemplateActivity.wjezUser?.phone=editphone.text.toString().trim()
                            TemplateActivity.wjezUser?.email=editEmail.text.toString().trim()



                            getUserInfo()





                        }
                        override fun onFailed(status: Int) {

                            BasicTools.showShimmer(card_save,shimmer_wait,false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            BasicTools.showShimmer(card_save,shimmer_wait,false)
            showToastMessage(R.string.no_connection)}
    }

    private fun getFile(uri: Uri): File?{
        val parcelFileDescriptor = contentResolver.openFileDescriptor(uri, "r", null)

        parcelFileDescriptor?.let {
            val inputStream = FileInputStream(parcelFileDescriptor.fileDescriptor)
            val file = File(cacheDir, getFileName(uri))
            val outputStream = FileOutputStream(file)

            inputStream.copyTo(outputStream)
            inputStream.close()
            outputStream.close()
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                FileUtils.copy(inputStream,outputStream)
            }*/
            return file
        }
        return null
    }

    fun getFileName(fileUri: Uri): String {

        var name = ""
        val returnCursor = contentResolver.query(fileUri, null, null, null, null)
        if (returnCursor != null) {
            val nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
            returnCursor.moveToFirst()
            name = returnCursor.getString(nameIndex)
            returnCursor.close()
        }

        return name
    }


}
