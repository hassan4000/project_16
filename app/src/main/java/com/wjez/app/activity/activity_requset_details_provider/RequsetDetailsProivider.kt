package com.wjez.app.activity.activity_requset_details_provider


import android.annotation.SuppressLint
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.CalendarContract
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.wjez.app.R
import com.wjez.app.activity.activityRequsetConfirmProvider.RequsetConfirmProviderActivity
import com.wjez.app.activity.homeActivity.HomeActivity
import com.wjez.app.activity.home_provider.HomeProvider
import com.wjez.app.adapter.ImageRequsetDetailsProviderAdapter
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.model.project16.HomeResponseProvider
import com.wjez.app.tools.*

import com.wjez.shop.tools.gone
import com.wjez.shop.tools.visible
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


import kotlinx.android.synthetic.main.activity_requset_details_provider.*

import kotlinx.android.synthetic.main.activity_requset_details_provider.root_cancel_button
import kotlinx.android.synthetic.main.activity_requset_details_provider.root_price
import kotlinx.android.synthetic.main.activity_requset_details_provider.rv_photos
import kotlinx.android.synthetic.main.activity_requset_details_provider.shimmer_cancel
import kotlinx.android.synthetic.main.activity_requset_details_provider.tv_details
import kotlinx.android.synthetic.main.activity_requset_details_provider.tv_id
import kotlinx.android.synthetic.main.activity_requset_details_provider.tv_price
import kotlinx.android.synthetic.main.toolbar_requset_details.*


import okhttp3.ResponseBody

class RequsetDetailsProivider : TemplateActivity(),IRequsetDetailsProivider {
    lateinit var layoutManager: LinearLayoutManager
    lateinit var imageAdapter: ImageRequsetDetailsProviderAdapter
    var disposable= CompositeDisposable()
    var disposableCancel= CompositeDisposable()
    var isLoading=false
    private var anim: Animation? = null
    override fun set_layout() {
        setContentView(R.layout.activity_requset_details_provider)
    }

    override fun init_activity(savedInstanceState: Bundle?) {


    }

    override fun init_views() {

        tv_details.setText(TemplateActivity.selectedRequsetProivder?.details)
        tv_distence.setText("${TemplateActivity.selectedRequsetProivder?.distance.toString()} km")

        iv_date.setText(TemplateActivity.selectedRequsetProivder?.createdAt?.substring(0,10))


        if (TemplateActivity.selectedRequsetProivder!!.user!!.image != null){
            BasicTools.loadImage(
                BasicTools.getUrlImg(this@RequsetDetailsProivider, TemplateActivity.selectedRequsetProivder!!.user!!.image!!.toString()),
                iv_profile,
                object : DownloadListener {
                    override fun completed(status: Boolean, bitmap: Bitmap) {
                        var anim: Animation? = null
                        anim = AnimationUtils.loadAnimation(parent!!, android.R.anim.fade_in)
                        anim!!.setDuration(1000)
                        iv_profile.animation = anim
                    }
                })

        }


        if(BasicTools.isDeviceLanEn()){
            tv_title_toolbar.setText(TemplateActivity.selectedRequsetProivder?.name)
            tv_name.setText(TemplateActivity.selectedRequsetProivder?.name)

        }
        else {
            tv_title_toolbar.setText(TemplateActivity.selectedRequsetProivder?.name)
            tv_name.setText(TemplateActivity.selectedRequsetProivder?.name)

        }


        checkImages()
        checkCost()

        checkShowPrice()

        checkShowCancel()

        checkDone()
        checkStatus()

    }



    @SuppressLint("SetTextI18n")
    override fun init_events() {

        iv_back.setOnClickListener {
            BasicTools.exitActivity(this@RequsetDetailsProivider)
        }

        tv_id.setText("ID = ${TemplateActivity.selectedRequsetProivder!!.id.toString()}")


        root_cancel_button.setOnClickListener {
            cancelRQ()
        }

        iv_send.setOnClickListener {
            if(!edit_price.text.isNullOrEmpty())
                sendOfferRQ()
        }

        card_done.setOnClickListener {
            finishedRQ()
        }
    }

    override fun set_fragment_place() {

    }

    private fun checkImages() {
        layoutManager= LinearLayoutManager(this@RequsetDetailsProivider,LinearLayoutManager.HORIZONTAL,false)
        imageAdapter= ImageRequsetDetailsProviderAdapter(
            this@RequsetDetailsProivider,
            selectedRequsetProivder?.serviceMedia!!,
            this)

        rv_photos.layoutManager=layoutManager
        rv_photos.adapter=imageAdapter
    }

    @SuppressLint("SetTextI18n")
    fun checkCost(){
        if(TemplateActivity.selectedRequsetProivder?.cost!=null){
            card_totla_price.visibility=View.VISIBLE
            tv_price.setText("${TemplateActivity.selectedRequsetProivder?.cost} SAR")
        }
        else{
            card_totla_price.visibility=View.GONE
        }
    }



    private fun checkShowPrice() {

        if(TemplateActivity.fromDashboradProvider)
            root_card_price.visibility=View.VISIBLE
        else          root_card_price.visibility=View.GONE



    }




    fun cancelRQ(){

        if (BasicTools.isConnected(this@RequsetDetailsProivider)) {

            BasicTools.showShimmer(card_cancel,shimmer_cancel,true)
            isLoading=true

            var map=HashMap<String,String>()
            map.put("service_request_id",TemplateActivity.selectedRequsetProivder!!.id.toString())
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@RequsetDetailsProivider),
                BasicTools.getProtocol(this@RequsetDetailsProivider).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.
            cancelRequsetProvider(map)
            disposableCancel.clear()
            disposableCancel.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ResponseBody>(this@RequsetDetailsProivider) {
                        @SuppressLint("SetTextI18n")
                        override fun onSuccess(result: ResponseBody) {

                            BasicTools.showShimmer(card_cancel,shimmer_cancel,false)
                            isLoading=false


                            TemplateActivity.homeProviderData!!.requests=null
                            TemplateActivity.myRequsetUpcomingList=null
                            TemplateActivity.myRequsetPrevList=null
                            BasicTools.clearAllActivity(this@RequsetDetailsProivider, HomeProvider::class.java)





                        }
                        override fun onFailed(status: Int) {

                            isLoading=false
                            BasicTools.showShimmer(card_cancel,shimmer_cancel,false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            isLoading=false
            BasicTools.showShimmer(card_cancel,shimmer_cancel,false)
            showToastMessage(R.string.no_connection)}
    }





    fun sendOfferRQ(){

        if (BasicTools.isConnected(this@RequsetDetailsProivider)) {

            BasicTools.showShimmer(card_offer,shimmer_wait_price,true)
            isLoading=true

            var map=HashMap<String,String>()
            map.put("service_request_id",TemplateActivity.selectedRequsetProivder!!.id.toString())
            map.put("cost",edit_price.text.toString())
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@RequsetDetailsProivider),
                BasicTools.getProtocol(this@RequsetDetailsProivider).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.
            sendOffer(map)
            disposableCancel.clear()
            disposableCancel.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ResponseBody>(this@RequsetDetailsProivider) {
                        @SuppressLint("SetTextI18n")
                        override fun onSuccess(result: ResponseBody) {

                            BasicTools.showShimmer(card_offer,shimmer_wait_price,false)
                            isLoading=false


                            TemplateActivity.homeProviderData!!.requests=null
                            TemplateActivity.myRequsetUpcomingList=null
                            TemplateActivity.myRequsetPrevList=null
                            BasicTools.openActivity(this@RequsetDetailsProivider, RequsetConfirmProviderActivity::class.java,true)





                        }
                        override fun onFailed(status: Int) {

                            isLoading=false
                            BasicTools.showShimmer(card_offer,shimmer_wait_price,false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            isLoading=false
            BasicTools.showShimmer(card_offer,shimmer_wait_price,false)
            showToastMessage(R.string.no_connection)}
    }



    fun finishedRQ(){

        if (BasicTools.isConnected(this@RequsetDetailsProivider)) {

            BasicTools.showShimmer(card_done,shimmer_done,true)
            isLoading=true

            var map=HashMap<String,String>()
            map.put("service_request_id",TemplateActivity.selectedRequsetProivder!!.id.toString())
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@RequsetDetailsProivider),
                BasicTools.getProtocol(this@RequsetDetailsProivider).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.
            finishedProvider(map)
            disposableCancel.clear()
            disposableCancel.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ResponseBody>(this@RequsetDetailsProivider) {
                        @SuppressLint("SetTextI18n")
                        override fun onSuccess(result: ResponseBody) {

                            BasicTools.showShimmer(card_done,shimmer_done,false)
                            isLoading=false
                            TemplateActivity.homeProviderData!!.requests=null
                            TemplateActivity.myRequsetUpcomingList=null
                            TemplateActivity.myRequsetPrevList=null
                            BasicTools.clearAllActivity(this@RequsetDetailsProivider, HomeProvider::class.java)





                        }
                        override fun onFailed(status: Int) {

                            isLoading=false
                            BasicTools.showShimmer(card_done,shimmer_done,false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            isLoading=false
            BasicTools.showShimmer(card_done,shimmer_done,false)
            showToastMessage(R.string.no_connection)}
    }




    private fun checkShowCancel() {



        if (TemplateActivity.fromDashboradProvider ||
            (TemplateActivity.selectedRequsetProivder!!.providerId == null
                    && TemplateActivity.selectedRequsetProivder!!.serviceStatus.equals(Constants.PENDING))
        )

            root_cancel_button.visibility = View.VISIBLE
        else root_cancel_button.visibility = View.GONE

    }

    private fun checkStatus() {

        var status=TemplateActivity.selectedRequsetProivder!!.serviceStatus
        var providerID=TemplateActivity.selectedRequsetProivder!!.providerId


        if(!TemplateActivity.fromDashboradProvider){
            if(status.equals(Constants.PENDING) && providerID==null){
                tv_status.setText(resources.getString(R.string.wait_approval))
                tv_status.setTextColor(ContextCompat.getColor(this@RequsetDetailsProivider,R.color.bg_camera))
            }


            else if(status.equals(Constants.PENDING)){
                tv_status.setText(resources.getString(R.string.in_progress))
                tv_status.setTextColor(ContextCompat.getColor(this@RequsetDetailsProivider,R.color.login_btn))
            }


            else if(status.equals(Constants.CANCELLED)){
                tv_status.setText(resources.getString(R.string.cancel_by_coustomer))
                tv_status.setTextColor(ContextCompat.getColor(this@RequsetDetailsProivider,R.color.google_red))
            }

            else if(status.equals(Constants.FINISHED)){
                tv_status.setText(resources.getString(R.string.finished))
                tv_status.setTextColor(ContextCompat.getColor(this@RequsetDetailsProivider,R.color.green))
            }

        }
        else {
            tv_status.gone()
        }


    }

    private fun checkDone() {

        if(!TemplateActivity.fromDashboradProvider){
            var status=TemplateActivity.selectedRequsetProivder!!.serviceStatus
            var providerID=TemplateActivity.selectedRequsetProivder!!.providerId



            if(status.equals(Constants.PENDING) && providerID!=null){
                root_done.visibility=View.VISIBLE
            }

            else
            root_done.visibility=View.GONE
        }
        else   root_done.visibility=View.GONE

    }





}
