package com.wjez.app.activity.enterCodeActivity


import android.annotation.SuppressLint
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import androidx.annotation.NonNull
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import com.wjez.app.R
import com.wjez.app.activity.signUp.SignUpAcitivty
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.TemplateActivity
import kotlinx.android.synthetic.main.activity_enter_code.*
import kotlinx.android.synthetic.main.toolbar_backpress1.*
import java.util.concurrent.TimeUnit


class EnterCodeActivity : TemplateActivity() {
    var auth: FirebaseAuth?=null
    private val KEY_VERIFICATION_ID = "key_verification_id"
    var counterDown: CountDownTimer?=null
    private var mVerificationId: String? = null
    private var tokenResend: PhoneAuthProvider.ForceResendingToken? = null
    private var mAuth: FirebaseAuth? = null
    private var sms=""
    private var secondTime=false
    override fun set_layout() {
        setContentView(R.layout.activity_enter_code)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

        if (mVerificationId == null && savedInstanceState != null&&secondTime) {
            onRestoreInstanceState(savedInstanceState);
        }

    }
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(KEY_VERIFICATION_ID, mVerificationId)
    }
    override fun onRestoreInstanceState(@NonNull savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        mVerificationId = savedInstanceState.getString(KEY_VERIFICATION_ID)
    }




    fun firebasePhoneV2(){
        btn_next.visibility=View.GONE
        card_resend.visibility=View.GONE
        counterDown!!.cancel()
        counterDown!!.start()
        Log.i("TEST_TEST","${TemplateActivity.countryNumber}${TemplateActivity.phoneNumber}")



    }
    override fun init_views() {
        mAuth = FirebaseAuth.getInstance()

        val options = PhoneAuthOptions.newBuilder(mAuth!!)
            .setPhoneNumber("${TemplateActivity.countryNumber}${TemplateActivity.phoneNumber}") // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(this) // Activity (for callback binding)
            .setCallbacks(callbacks) // OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)







    }

    private val callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks =
        object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
                 sms = phoneAuthCredential.smsCode!!
                Log.i("TEST_TEST","onVerificationCompleted")
                if (sms != null) {
                    edit_1.setText(sms.substring(0,1))
                    edit_2.setText(sms.substring(1,2))
                    edit_3.setText(sms.substring(2,3))
                    edit_4.setText(sms.substring(3,4))
                    edit_5.setText(sms.substring(4,5))
                    edit_6.setText(sms.substring(5,6))
                    verifyVerificationCode(sms)
                }
            }

            override fun onVerificationFailed(p0: FirebaseException) {
                btn_next.visibility=View.VISIBLE
                card_resend.visibility=View.VISIBLE
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                if (p0 is FirebaseAuthInvalidCredentialsException) {
                  showToastMessage(p0.message)
                } else if (p0 is FirebaseTooManyRequestsException) {
                    showToastMessage(p0.message)
                }
            }

            override fun onCodeSent(
                s: String,
                forceResendingToken: PhoneAuthProvider.ForceResendingToken
            ) {

                Log.i("TEST_TEST","onCodeSent")
                btn_next.visibility=View.VISIBLE
                card_resend.visibility=View.GONE
                secondTime=true
                mVerificationId = s
                tokenResend=forceResendingToken
                Log.i("TEST_TEST","mVerificationId $mVerificationId")
                super.onCodeSent(s, forceResendingToken)
                //mResendToken = forceResendingToken
            }

            }


    private fun verifyVerificationCode(code: String) {
        //creating the credential
        val credential = PhoneAuthProvider.getCredential(mVerificationId!!, code)

        //signing the user
        signInWithPhoneAuthCredential(credential)
    }


    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        mAuth!!.signInWithCredential(credential)
            .addOnCompleteListener(
                this,
                OnCompleteListener<AuthResult?> { task ->

                    Log.i("TTTTTTT","TTTTTTTTTTTTTTTTTTTT")
                    if (task.isSuccessful) {
                        //verification successful we will start the profile activity
                        showToastMessage("yesssssssssssssssssssssssssssssssssssssss")

                    } else {
                       showToastMessage(task.exception!!.message)
                    }
                })
    }



    @SuppressLint("SetTextI18n")
    override fun init_events() {

        tv_number.setText("${TemplateActivity.countryNumber}${TemplateActivity.phoneNumber}")

        card_resend.setOnClickListener {


          counterDown!!.cancel()
          counterDown!!.start()


            PhoneAuthProvider.verifyPhoneNumber(
                PhoneAuthOptions
                    .newBuilder(FirebaseAuth.getInstance())
                    .setActivity(this)
                    .setPhoneNumber("${TemplateActivity.countryNumber}${TemplateActivity.phoneNumber}")
                    .setTimeout(60L, TimeUnit.SECONDS)
                    .setCallbacks(callbacks)
                    .setForceResendingToken(tokenResend!!)
                    .build())
        //    firebasePhone()
        }

        counterDown=object : CountDownTimer(60000,1000){
            override fun onFinish() {
                tv_resend.setText(" 00:00 ")
                btn_next.visibility= View.VISIBLE
                card_resend.visibility= View.VISIBLE

                /*     if(BasicTools.isConnected(parent!!))
                     getverifyCodeRQ()
                     else
                         parent!!.showToastMessage(R.string.no_connection)*/

            }

            @SuppressLint("SetTextI18n")
            override fun onTick(p0: Long) {

                val seconds :Int= (p0 / 1000).toInt()
                tv_resend.setText("00:"+  BasicTools.normalNumber(seconds.toString())+" ")
            }

        }.start()


        iv_back.setOnClickListener {
            TemplateActivity.SignUpModel.smsCode=""
            BasicTools.exitActivity(this@EnterCodeActivity)
        }
        btn_next.setOnClickListener {


          // showToastMessage(TemplateActivity.SignUpModel.smsCode.length.toString())
            var code=edit_1.text.toString().trim()+edit_2.text.toString().trim()+edit_3.text.toString().trim()+edit_4.text.toString().trim()+edit_5.text.toString().trim()+edit_6.text.toString().trim()
            verifyVerificationCode(sms)
            /*
                       if(TemplateActivity.SignUpModel.smsCode.isNotEmpty()) {

                           counterDown!!.cancel()
                             BasicTools.openActivity(this@EnterCodeActivity,SignUpAcitivty::class.java,true)
                       }

                       else if(TemplateActivity.SignUpModel.smsCode.equals(code)&&code.isNotEmpty()){
                           counterDown!!.cancel()

                           BasicTools.openActivity(this@EnterCodeActivity,SignUpAcitivty::class.java,true)
                       }
                       else if(!TemplateActivity.SignUpModel.smsCode.equals(code)){

                           showToastMessage(R.string.enter_code_wrong_code)
                       }

                       else {

                       }*/


            BasicTools.openActivity(this@EnterCodeActivity,SignUpAcitivty::class.java,true)
        }

        firebasePhoneV2()
     //   firebasePhone()

    }

    override fun set_fragment_place() {

    }


}
