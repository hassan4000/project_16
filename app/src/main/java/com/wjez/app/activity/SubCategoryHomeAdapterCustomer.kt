package com.wjez.app.activity




import android.annotation.SuppressLint
import android.content.Context

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation

import android.widget.LinearLayout
import android.widget.TextView

import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView

import com.wjez.app.R

import com.wjez.app.fragment.homeFragment.IHomeFragment

import com.wjez.app.model.project16.HomeResponse




import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.TemplateActivity
import kotlinx.android.synthetic.main.card_sub_category_home_customer.view.*


import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SubCategoryHomeAdapterCustomer(): RecyclerView.Adapter<SubCategoryHomeAdapterCustomer.AdapterViewHolder>(){

    private var data:ArrayList<HomeResponse.Data.Category> ?= null
    private var context:Context ?=null

    private var iview:IHomeFragment ?=null


    private var anim: Animation? = null



    constructor(context:Context,item:ArrayList<HomeResponse.Data.Category>,iview: IHomeFragment) : this() {

        this.context=context
        this.data=item
        this.iview=iview
    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {
        return AdapterViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.card_sub_category_home_customer, parent, false)
        )
    }



    fun change_data(new_data:ArrayList<HomeResponse.Data.Category>){
        CoroutineScope(Dispatchers.Main).launch {
            data?.clear()
            for(item in new_data)
                data?.add(item)
            notifyDataSetChanged()

        }


    }


    fun addItem(item:HomeResponse.Data.Category,pos:Int){
        CoroutineScope(Dispatchers.Main).launch {

            if (pos < data!!.size) run {
                data!!.add(pos, item)
                this@SubCategoryHomeAdapterCustomer.notifyItemInserted(pos)
            }else run {
                data!!.add(item)
                this@SubCategoryHomeAdapterCustomer.notifyItemInserted(data!!.size-1)
            }

        }
    }
    fun addRefersh(new_data:ArrayList<HomeResponse.Data.Category>){
        CoroutineScope(Dispatchers.Main).launch {

            for(item in new_data)
                data?.add(item)
            notifyItemInserted(data!!.size-1)
        }

    }

    fun getUserDataAt(position: Int): HomeResponse.Data.Category? {
        return if (position >= 0 && position < data!!.size) data!!.get(position)
        else null
    }

    fun getList(): ArrayList<HomeResponse.Data.Category> {
        return data!!
    }

    override fun getItemCount() = data!!.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: AdapterViewHolder, position: Int) {

        val item = data?.get(position)


        var text = holder.view.tv_name as TextView
        var root = holder.view.root_card as LinearLayout
        var rootGradiant = holder.view.root_gradiant as LinearLayout

        if (BasicTools.isDeviceLanEn()) {
            text.setText(item?.name)
        } else {
            text.setText(item?.nameAr)
        }




        if(TemplateActivity.selectedSubCategoryHomeCustomer==item!!.id!!){
            rootGradiant.background=ContextCompat.getDrawable(context!!,R.drawable.button_bg)
            text.setTextColor(ContextCompat.getColor(context!!,R.color.color_sub_category_txt))
        }
        else    {
            rootGradiant.background=ContextCompat.getDrawable(context!!,R.drawable.button_bg_white)
            text.setTextColor(ContextCompat.getColor(context!!,R.color.text_color_wjez5))

        }

        //Log.i("TEST_TEST", BasicTools.getUrlImg(context!!, item?.image!!.toString()))


      /*  BasicTools.loadImage(
            BasicTools.getUrlImg(context!!, item?.image!!.toString()),
            image,
            object : DownloadListener {
                override fun completed(status: Boolean, bitmap: Bitmap) {
                    anim = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
                    anim!!.setDuration(1000)
                    image.animation = anim
                }
            })
*/


        root.setOnClickListener {



            iview?.funPressOnSubCategory(item!!)


        }




    }




    class AdapterViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
