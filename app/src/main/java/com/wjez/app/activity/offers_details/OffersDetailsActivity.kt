package com.wjez.app.activity.offers_details


import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.wjez.app.R
import com.wjez.app.activity.homeActivity.HomeActivity
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.model.project16.PrivacyPolicyModel
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.DownloadListener
import com.wjez.app.tools.TemplateActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_offers_details.*

import kotlinx.android.synthetic.main.toolbar_general.*
import okhttp3.ResponseBody

class OffersDetailsActivity : TemplateActivity() {
    var disposable= CompositeDisposable()
    private var anim: Animation? = null
    override fun set_layout() {
        setContentView(R.layout.activity_offers_details)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    @SuppressLint("SetTextI18n")
    override fun init_views() {

        var item=TemplateActivity.selectedOffers
        tv_name_provider.setText(item?.provider?.name)
        tv_price.setText(item?.cost)
        tv_rating_provider.setText(item?.provider?.rate.toString())
        tv_details.setText(item?.details)

        ratingBar_provider.rating= item?.provider?.rate!!.toString().toFloat()

        if(item?.provider?.image!=null)
            BasicTools.loadImage(BasicTools.getUrlImg(this@OffersDetailsActivity, item?.provider?.image!!),iv_profile_provider,object :
                DownloadListener {
                override fun completed(status: Boolean, bitmap: Bitmap) {
                    anim = AnimationUtils.loadAnimation(this@OffersDetailsActivity, android.R.anim.fade_in)
                    anim!!.setDuration(1000)
                    iv_profile_provider.animation= anim
                }
            })


        iv_back.setOnClickListener {
            BasicTools.exitActivity(this)
        }

        if(!BasicTools.isDeviceLanEn())

        tv_title_toolbar.setText("${resources.getString(R.string.offer)} ${item?.provider?.name}")

        else{
            tv_title_toolbar.setText("${item?.provider?.name} ${resources.getString(R.string.offers)}")
        }


    }

    override fun init_events() {

        card_refuse.setOnClickListener {

        }

        card_accept.setOnClickListener {

            acceptRQ()
        }

    }

    override fun set_fragment_place() {

    }


    fun acceptRQ(){
        if (BasicTools.isConnected(this@OffersDetailsActivity)) {


            var item=TemplateActivity.selectedOffers

            var map=HashMap<String,String>()
            map.put("service_offer_id",item!!.id.toString())

            BasicTools.showShimmer(root_buttons,shimmer_wait,true)


            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@OffersDetailsActivity),  BasicTools.getProtocol(this@OffersDetailsActivity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.changToApproval(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ResponseBody>(this@OffersDetailsActivity) {
                        override fun onSuccess(result: ResponseBody) {



                            BasicTools.showShimmer(root_buttons,shimmer_wait,false)
                            showToastMessage(R.string.success)
                            TemplateActivity.homeResponse=null
                            BasicTools.clearAllActivity(this@OffersDetailsActivity,HomeActivity::class.java)






                        }
                        override fun onFailed(status: Int) {


                            BasicTools.showShimmer(root_buttons,shimmer_wait,false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {

            BasicTools.showShimmer(root_buttons,shimmer_wait,false)
            showToastMessage(R.string.no_connection)}
    }


}
