package com.wjez.app.activity.offers_list_activity


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.wjez.app.R
import com.wjez.app.activity.offers_details.OffersDetailsActivity
import com.wjez.app.adapter.OffersListAdapter
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.model.project16.FeedbackResponse
import com.wjez.app.model.project16.HomeResponse
import com.wjez.app.model.project16.OffersListResponse
import com.wjez.app.model.project16.OffersNumberResponse
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.TemplateActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_all_offers_list.*
import kotlinx.android.synthetic.main.navigation_sort_by.*
import kotlinx.android.synthetic.main.toolbar_offers_list.*

class OffersListAcitivty : TemplateActivity(),IOffersListAcitivty{

    lateinit var adapter:OffersListAdapter
    lateinit var layoutManager:LinearLayoutManager
    lateinit var rv:RecyclerView
    lateinit var tvNoData:TextView
    var priceFilter="asc"
    var ratingFilter="asc"
    var disposable= CompositeDisposable()

    lateinit var rootFilter1:LinearLayout
    lateinit var rootFilter2:LinearLayout
    lateinit var rootFilter3:LinearLayout
    lateinit var rootFilter4:LinearLayout

    private lateinit  var mSheet: BottomSheetBehavior<LinearLayout>
    override fun set_layout() {
        setContentView(R.layout.activity_all_offers_list)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun init_views() {
        mSheet = BottomSheetBehavior.from<LinearLayout>(root_sort_by)
        mSheet.setState(BottomSheetBehavior.STATE_HIDDEN)
        tvNoData=tv_no_data
        rv=rv_offres


        rootFilter1=root_filter1
        rootFilter2=root_filter2
        rootFilter3=root_filter3
        rootFilter4=root_filter4
    }

    override fun init_events() {

        swip_offers_list.setOnRefreshListener {
            if(TemplateActivity.itemToShowOffersID==-1){

                filterWithoutId(ratingFilter,priceFilter)

            }else{
                filterWithId(ratingFilter,priceFilter,TemplateActivity.itemToShowOffersID.toString())
            }
        }

        adapter= OffersListAdapter(this,ArrayList(),this)
        layoutManager= LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
        rv.adapter=adapter
        rv.layoutManager=layoutManager
        if(TemplateActivity.itemToShowOffersID==-1){

            filterWithoutId(ratingFilter,priceFilter)

        }else{
            filterWithId(ratingFilter,priceFilter,TemplateActivity.itemToShowOffersID.toString())
        }


        iv_back.setOnClickListener {
            BasicTools.exitActivity(this@OffersListAcitivty)
        }

        iv_filter.setOnClickListener {

            mSheet.setState(BottomSheetBehavior.STATE_EXPANDED)

        }

        rootFilter1.setOnClickListener {

            iv_check_filter1.visibility=View.VISIBLE
            iv_notcheck_filter2.visibility=View.VISIBLE
            iv_notcheck_filter1.visibility=View.GONE
            iv_check_filter2.visibility=View.GONE

            priceFilter="asc"
        }

        rootFilter2.setOnClickListener {

            iv_check_filter1.visibility=View.GONE
            iv_notcheck_filter1.visibility=View.VISIBLE


            iv_notcheck_filter2.visibility=View.GONE
            iv_check_filter2.visibility=View.VISIBLE
            priceFilter="desc"
        }

        rootFilter3.setOnClickListener {

            iv_check_filter3.visibility=View.VISIBLE
            iv_notcheck_filter3.visibility=View.GONE

            iv_check_filter4.visibility=View.GONE
            iv_notcheck_filter4.visibility=View.VISIBLE

            ratingFilter="asc"

        }
        rootFilter4.setOnClickListener {
            iv_check_filter3.visibility=View.GONE
            iv_notcheck_filter3.visibility=View.VISIBLE

            iv_check_filter4.visibility=View.VISIBLE
            iv_notcheck_filter4.visibility=View.GONE

            ratingFilter="desc"
        }


        card_apply_feedback.setOnClickListener {


            if(TemplateActivity.itemToShowOffersID==-1){
                filterWithoutId(ratingFilter,priceFilter)
            }else{
                filterWithId(ratingFilter,priceFilter,TemplateActivity.itemToShowOffersID.toString())
            }
            mSheet.setState(BottomSheetBehavior.STATE_HIDDEN)

        }

    }

    override fun set_fragment_place() {

    }


    fun filterWithoutId(rating:String,price:String){

        if (BasicTools.isConnected(this@OffersListAcitivty)) {
            var map = HashMap<String, String>()
            map.put("rating",rating)
            map.put("price",price)

            swip_offers_list.isRefreshing=true
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@OffersListAcitivty),
                BasicTools.getProtocol(this@OffersListAcitivty).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getOffersByFilter(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<OffersListResponse>(this@OffersListAcitivty) {
                        override fun onSuccess(result: OffersListResponse) {



                            swip_offers_list.isRefreshing=false

                            rv.adapter=adapter



                            if(result?.data!!.isNullOrEmpty())
                                tvNoData.visibility= View.VISIBLE

                            else{
                                tvNoData.visibility= View.GONE
                                adapter.change_data(result?.data!!)
                                rv.adapter=adapter
                            }



                        }
                        override fun onFailed(status: Int) {

                            swip_offers_list.isRefreshing=false

                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {


            showToastMessage(R.string.no_connection)}
    }


    fun filterWithId(rating:String,price:String,id:String){

        if (BasicTools.isConnected(this@OffersListAcitivty)) {
            var map = HashMap<String, String>()
            map.put("rating",rating)
            map.put("price",price)
            swip_offers_list.isRefreshing=true
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@OffersListAcitivty),
                BasicTools.getProtocol(this@OffersListAcitivty).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getOffersByIDAndFilter(id,map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<OffersNumberResponse>(this@OffersListAcitivty) {
                        override fun onSuccess(result: OffersNumberResponse) {


                            swip_offers_list.isRefreshing=false





                            if(result?.data?.offers!!.isNullOrEmpty())
                                tvNoData.visibility= View.VISIBLE

                            else{
                                tvNoData.visibility= View.GONE
                                adapter.change_data(result?.data?.offers!!)
                                rv.adapter=adapter
                            }



                        }
                        override fun onFailed(status: Int) {

                            swip_offers_list.isRefreshing=false

                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {


            showToastMessage(R.string.no_connection)}
    }

    override fun openPage(item: HomeResponse.Data.Offer) {
        TemplateActivity.selectedOffers=item
        BasicTools.openActivity(this@OffersListAcitivty,OffersDetailsActivity::class.java,false)
    }


}
