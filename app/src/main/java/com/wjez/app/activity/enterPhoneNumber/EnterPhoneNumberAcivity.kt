package com.wjez.app.activity.enterPhoneNumber


import android.os.Bundle
import android.view.View
import android.widget.EditText
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.annotation.NotEmpty

import com.wjez.app.R
import com.wjez.app.activity.enterCodeActivity.EnterCodeActivity
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.model.project21.MessageModel
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.TemplateActivity
import com.wjez.shop.tools.hide
import com.wjez.shop.tools.visible
import com.hbb20.CountryCodePicker
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_enter_phone_number.*
import kotlinx.android.synthetic.main.toolbar_backpress1.*
import okhttp3.ResponseBody

class EnterPhoneNumberAcivity : TemplateActivity(), Validator.ValidationListener {
    var disposable= CompositeDisposable()
    lateinit var countryCode: CountryCodePicker
    var validator: Validator? = null


    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    lateinit var editphone: EditText
    override fun set_layout() {
        setContentView(R.layout.activity_enter_phone_number)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun init_views() {

        countryCode=country_code
        editphone=edit_phone
    }



    override fun init_events() {


        iv_back.setOnClickListener {
            BasicTools.exitActivity(this@EnterPhoneNumberAcivity)
        }
        validator = Validator(this)
        validator!!.setValidationListener(this)


        card_contintue.setOnClickListener{
            validator!!.validate()

        }



    }

    override fun set_fragment_place() {

    }

    override fun onValidationFailed(errors:List<ValidationError>?) {
        BasicTools.showValidationErrorMessagesForFields(errors!!, this@EnterPhoneNumberAcivity)
    }

    override fun onValidationSucceeded() {
        checkPhoneRQ(editphone.text.trim().toString())

    }


    fun showShimmerBtn(state: Boolean){

        if(state){
            card_contintue.visibility= View.GONE
            shimmer_wait.visible()
        }else{
            card_contintue.visibility= View.VISIBLE
            shimmer_wait.hide()
        }
    }

    fun checkPhoneRQ(txt:String){
        if (BasicTools.isConnected(this@EnterPhoneNumberAcivity)) {



            showShimmerBtn(true)

            var map = HashMap<String, String>()
            map.put("phone",txt)
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@EnterPhoneNumberAcivity),  BasicTools.getProtocol(this@EnterPhoneNumberAcivity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.checkPhoneWjez(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ResponseBody>(this@EnterPhoneNumberAcivity) {
                        override fun onSuccess(result: ResponseBody) {


                            showShimmerBtn(false)



                                TemplateActivity.phoneNumber=editphone.text.trim().toString()
                                TemplateActivity.countryNumber=countryCode.selectedCountryCodeWithPlus

                            BasicTools.openActivity(this@EnterPhoneNumberAcivity,EnterCodeActivity::class.java,false)



                        }
                        override fun onFailed(status: Int) {


                            TemplateActivity.phoneNumber=editphone.text.trim().toString()
                            TemplateActivity.countryNumber=countryCode.selectedCountryCodeWithPlus

                            showShimmerBtn(false)
                            if(status==400){
                            showToastMessage(R.string.check_phone_hint)
                            //    BasicTools.openActivity(this@EnterPhoneNumberAcivity,EnterCodeActivity::class.java,false)
                            }
                            else
                            showToastMessage(R.string.faild)

                        }
                    }))

        }
        else {
            showShimmerBtn(false)
            showToastMessage(R.string.no_connection)}
    }


}
