package com.wjez.app.activity.contactUsActivity


import android.os.Bundle
import com.wjez.app.R
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.fragment.project21.settingModel.SettingModel
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.TemplateActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_contact_us.*
import kotlinx.android.synthetic.main.toolbar_contact_us.*

class ContactUsActivity : TemplateActivity() {
    var disposable= CompositeDisposable()

    var settingModel:SettingModel?=null
    override fun set_layout() {
        setContentView(R.layout.activity_contact_us)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun init_views() {

    }

    override fun init_events() {

        iv_back.setOnClickListener {
            BasicTools.exitActivity(this@ContactUsActivity)
        }

        RQ()
        swip_contact_us.setOnRefreshListener {
            RQ()
        }


        iv_facebook.setOnClickListener {

            if(settingModel!=null)
                BasicTools.open_website(settingModel?.data?.facebookLink!!.toString(),this@ContactUsActivity)
        }

        iv_twitter.setOnClickListener {

            if(settingModel!=null)
                BasicTools.open_website(settingModel?.data?.twitterLink!!.toString(),this@ContactUsActivity)
        }

        iv_insta.setOnClickListener {

            if(settingModel!=null)
                BasicTools.open_website(settingModel?.data?.instagramLink!!.toString(),this@ContactUsActivity)
        }

    }

    override fun set_fragment_place() {

    }


    fun RQ(){
        if (BasicTools.isConnected(this@ContactUsActivity)) {



          swip_contact_us.isRefreshing=true

            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@ContactUsActivity),  BasicTools.getProtocol(this@ContactUsActivity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getSetting()
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<SettingModel>(this@ContactUsActivity) {
                        override fun onSuccess(result: SettingModel) {


                            swip_contact_us.isRefreshing=false

                            tv_phone.setText(result.data?.phone.toString())
                            tv_email.setText(result.data?.email.toString())
                            settingModel=result




                        }
                        override fun onFailed(status: Int) {

                            swip_contact_us.isRefreshing=false

                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            swip_contact_us.isRefreshing=false

            showToastMessage(R.string.no_connection)}
    }


}
