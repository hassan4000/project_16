package com.wjez.app.activity.activity_requset_details


import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.wjez.app.R
import com.wjez.app.activity.homeActivity.HomeActivity
import com.wjez.app.activity.offers_list_activity.OffersListAcitivty
import com.wjez.app.adapter.ImageRequsetDetailsAdapter
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.fragment.project21.settingModel.SettingModel
import com.wjez.app.model.project16.FeedbackResponse
import com.wjez.app.model.project16.OffersNumberResponse
import com.wjez.app.tools.*
import com.wjez.shop.tools.gone
import com.wjez.shop.tools.visible
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_about_us.*
import kotlinx.android.synthetic.main.activity_requset_details.*
import kotlinx.android.synthetic.main.navigation_feedback.*

import okhttp3.ResponseBody
import kotlin.math.log

class RequsetDetailsActivity : TemplateActivity(),IRequsetDetailsActivity {
    lateinit var layoutManager:LinearLayoutManager
    lateinit var imageAdapter:ImageRequsetDetailsAdapter
    private lateinit  var mSheet: BottomSheetBehavior<LinearLayout>
    var disposable= CompositeDisposable()
    var disposableCancel= CompositeDisposable()


    var isLoading=false
    lateinit var imageYourRating: ImageView
    /*      mSheet = BottomSheetBehavior.from<LinearLayout>(bottom_sheet)
        mSheet.setState(BottomSheetBehavior.STATE_HIDDEN)*/
    private var anim: Animation? = null
    private var anim1: Animation? = null
    private var anim2: Animation? = null
    override fun set_layout() {
        setContentView(R.layout.activity_requset_details)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun init_views() {
        mSheet = BottomSheetBehavior.from<LinearLayout>(root_feedback)
        mSheet.setState(BottomSheetBehavior.STATE_HIDDEN)

        checkProvider()
        checkCategory()
        checkImages()
        checkCanCancel()
        checkCost()
    //    checkYourReview()

        tv_details.setText(TemplateActivity.selectedRequset?.details)



        if(BasicTools.isDeviceLanEn()){
        tv_title_toolbar.setText(TemplateActivity.selectedRequset?.category?.name)
            tv_name_services.setText(TemplateActivity.selectedRequset?.category?.name)

        }
        else {
            tv_title_toolbar.setText(TemplateActivity.selectedRequset?.category?.nameAr)
            tv_name_services.setText(TemplateActivity.selectedRequset?.category?.nameAr)

        }

        BasicTools.loadImage(BasicTools.getUrlImg(this@RequsetDetailsActivity
            , TemplateActivity.selectedRequset?.category?.image.toString()),iv_category
            ,object :
                DownloadListener {
                override fun completed(status: Boolean, bitmap: Bitmap) {
                    anim = AnimationUtils.loadAnimation(this@RequsetDetailsActivity,
                        android.R.anim.fade_in)
                    anim!!.setDuration(1000)
                    iv_category.animation= anim
                }
            })


    }



    @SuppressLint("SetTextI18n")
    override fun init_events() {

        tv_id.setText("ID = ${TemplateActivity.selectedRequset!!.id.toString()}")

        iv_back.setOnClickListener {

            BasicTools.exitActivity(this@RequsetDetailsActivity)
        }

        card_apply_feedback.setOnClickListener {

            var rating=ratingBar_feedback.rating.toString()
            var feedText=edit_desc.text.toString()

            ratingRQ(rating,feedText,TemplateActivity.selectedRequset!!.providerId.toString(),
            selectedRequset!!.userId.toString())



        }

        ratingBar_feedback.setOnRatingBarChangeListener(object : RatingBar.OnRatingBarChangeListener {
            override fun onRatingChanged(ratingBar: RatingBar?, rating: Float, fromUser: Boolean) {


            }

        })


        card_cancel.setOnClickListener {
            cancelServices()
        }

        card_offers.setOnClickListener {

            TemplateActivity.itemToShowOffersID=TemplateActivity.selectedRequset!!.id!!
            BasicTools.openActivity(this@RequsetDetailsActivity,OffersListAcitivty::class.java,false)
        }

    }

    override fun set_fragment_place() {

    }


    fun checkProvider(){
        if(TemplateActivity.selectedRequset?.provider?.id!=null){


            tv_provider_hint.visibility= View.VISIBLE
            root_provider.visibility= View.VISIBLE

            tv_name_provider.setText(TemplateActivity.selectedRequset?.provider?.name)
            if(TemplateActivity.selectedRequset?.provider?.rate!=null){
            tv_rating_provider.setText("(${TemplateActivity.selectedRequset?.provider?.rate.toString()})")
            ratingBar_provider.rating= TemplateActivity.selectedRequset?.provider?.rate.toString().toFloat()}

            BasicTools.loadImage(BasicTools.getUrlImg(this@RequsetDetailsActivity
                , TemplateActivity.selectedRequset?.provider?.image.toString()),iv_profile_provider
                ,object :
                DownloadListener {
                override fun completed(status: Boolean, bitmap: Bitmap) {
                    anim = AnimationUtils.loadAnimation(this@RequsetDetailsActivity,
                        android.R.anim.fade_in)
                    anim!!.setDuration(1000)
                    iv_profile_provider.animation= anim
                }
            })



            }else{
            tv_provider_hint.visibility= View.GONE
            root_provider.visibility= View.GONE
        }

    }

    fun checkCategory(){
        tv_date.setText(TemplateActivity.selectedRequset?.createdAt!!.substring(0,18).replace(Regex("T")," "))

        var state=TemplateActivity.selectedRequset?.serviceStatus

        if(state.equals(Constants.PENDING)){
            root_done.gone()
            root_cancel.gone()
            root_inprogress.visible()


        }else if(state.equals(Constants.CANCELLED)){
            root_done.gone()
            root_cancel.visible()
            root_inprogress.gone()

        }else if(state.equals(Constants.FINISHED)){
            root_done.visible()
            root_cancel.gone()
            root_inprogress.gone()

        }
    }

    private fun checkImages() {
        layoutManager= LinearLayoutManager(this@RequsetDetailsActivity,LinearLayoutManager.HORIZONTAL,false)
        imageAdapter= ImageRequsetDetailsAdapter(
            this@RequsetDetailsActivity,
            TemplateActivity.selectedRequset?.serviceMedia!!,
            this)

        rv_photos.layoutManager=layoutManager
        rv_photos.adapter=imageAdapter
    }

    @SuppressLint("SetTextI18n")
    fun checkCost(){
        if(TemplateActivity.selectedRequset?.cost!=null){
            root_price.visible()
            tv_price.setText("${TemplateActivity.selectedRequset?.cost} SAR")
        }
        else{
            root_price.gone()
        }
    }


    @SuppressLint("SetTextI18n")
    private fun checkCanCancel() {

        if(TemplateActivity.selectedRequset!!.providerId==null &&
            TemplateActivity.selectedRequset?.serviceStatus.equals(Constants.PENDING)){
            root_category.visibility=View.GONE
            root_your_reivews.visibility=View.GONE
            root_offers_number.visibility=View.VISIBLE
            root_cancel_button.visibility=View.VISIBLE

            getOffersNumber()

        }
    }

    fun checkYourReview(){
        if(TemplateActivity.selectedRequset!!.user!=null
            &&
            TemplateActivity.selectedRequset!!.user!!.userRate!=null
            &&
            TemplateActivity.selectedRequset!!.user!!.userRate!!.rate!=null

            ){

            root_your_reivews.visible()
            card_feedback.visibility=View.GONE
            mSheet.setState(BottomSheetBehavior.STATE_HIDDEN)



            tv_provider_name_rating.setText(TemplateActivity.selectedRequset?.user?.name)
            tv_provider_name_feedback.setText(TemplateActivity.selectedRequset?.user?.name)


                ratingBar_rating.rating= TemplateActivity.selectedRequset?.user?.userRate?.rate.toString().toFloat()
            ratingBar_feedback.rating= TemplateActivity.selectedRequset?.user?.userRate?.rate.toString().toFloat()

            BasicTools.loadImage(BasicTools.getUrlImg(this@RequsetDetailsActivity
                , TemplateActivity.selectedRequset?.user?.image.toString()),iv_profile_rating
                ,object :
                    DownloadListener {
                    override fun completed(status: Boolean, bitmap: Bitmap) {
                        Log.i("TEST_TEST", BasicTools.getUrlImg(this@RequsetDetailsActivity
                            , TemplateActivity.selectedRequset?.user?.image.toString())
                        )
                     /*   anim = AnimationUtils.loadAnimation(this@RequsetDetailsActivity,
                            android.R.anim.fade_in)
                        anim!!.setDuration(1000)
                        iv_profile_rating.animation= anim*/
                    }
                })

            BasicTools.loadImage(BasicTools.getUrlImg(this@RequsetDetailsActivity
                , TemplateActivity.selectedRequset?.user?.image.toString()),iv_profile_feedback
                ,object :
                    DownloadListener {
                    override fun completed(status: Boolean, bitmap: Bitmap) {
                   /*     anim = AnimationUtils.loadAnimation(this@RequsetDetailsActivity,
                            android.R.anim.fade_in)
                        anim!!.setDuration(1000)
                        iv_profile_rating.animation= anim*/
                    }
                })


        }else if(TemplateActivity.selectedRequset?.serviceStatus.equals(Constants.FINISHED)){
            root_your_reivews.gone()
            card_feedback.visibility=View.VISIBLE
            mSheet.setState(BottomSheetBehavior.STATE_EXPANDED)
        }
    }


    fun ratingRQ(rating:String,feedbackText:String,
    providerId:String,id:String){

        if (BasicTools.isConnected(this@RequsetDetailsActivity)) {
            var map = HashMap<String, String>()
            map.put("rate",rating)
            map.put("provider_id",providerId)
            map.put("feedback",feedbackText)
            map.put("service_request_id",id)
            isLoading=true
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@RequsetDetailsActivity),
                BasicTools.getProtocol(this@RequsetDetailsActivity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.addFeedback(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<FeedbackResponse>(this@RequsetDetailsActivity) {
                        override fun onSuccess(result: FeedbackResponse) {


                            isLoading=false






                        }
                        override fun onFailed(status: Int) {

                            isLoading=false

                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            isLoading=false

            showToastMessage(R.string.no_connection)}
    }




    fun getOffersNumber(){

        if (BasicTools.isConnected(this@RequsetDetailsActivity)) {

            BasicTools.showShimmer(card_offers,shimmer_wait_offers,true)
            isLoading=true
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@RequsetDetailsActivity),
                BasicTools.getProtocol(this@RequsetDetailsActivity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.
            getOffersByID(TemplateActivity.selectedRequset!!.id.toString())
            disposableCancel.clear()
            disposableCancel.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<OffersNumberResponse>(this@RequsetDetailsActivity) {
                        @SuppressLint("SetTextI18n")
                        override fun onSuccess(result: OffersNumberResponse) {


                            isLoading=false
                            tv_offers_number.
                            setText("${resources.getString(R.string.see_all)} ${result.offerCount} ${resources.getString(R.string.offers)}")


                            BasicTools.showShimmer(card_offers,shimmer_wait_offers,false)



                        }
                        override fun onFailed(status: Int) {

                            isLoading=false
                            BasicTools.showShimmer(card_offers,shimmer_wait_offers,false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            isLoading=false
            BasicTools.showShimmer(card_offers,shimmer_wait_offers,false)
            showToastMessage(R.string.no_connection)}
    }




    fun cancelServices(){

        if (BasicTools.isConnected(this@RequsetDetailsActivity)) {

            BasicTools.showShimmer(card_cancel,shimmer_cancel,true)
            isLoading=true

            var map=HashMap<String,String>()
            map.put("service_request_id",TemplateActivity.selectedRequset!!.id.toString())
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@RequsetDetailsActivity),
                BasicTools.getProtocol(this@RequsetDetailsActivity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.
            cancelSerices(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ResponseBody>(this@RequsetDetailsActivity) {
                        @SuppressLint("SetTextI18n")
                        override fun onSuccess(result: ResponseBody) {

                            BasicTools.showShimmer(card_cancel,shimmer_cancel,false)
                            isLoading=false



                            BasicTools.clearAllActivity(this@RequsetDetailsActivity,HomeActivity::class.java)





                        }
                        override fun onFailed(status: Int) {

                            isLoading=false
                            BasicTools.showShimmer(card_cancel,shimmer_cancel,false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            isLoading=false
            BasicTools.showShimmer(card_cancel,shimmer_cancel,false)
            showToastMessage(R.string.no_connection)}
    }


}
