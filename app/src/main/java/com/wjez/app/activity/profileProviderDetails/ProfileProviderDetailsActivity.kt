package com.wjez.app.activity.profileProviderDetails


import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.appbar.AppBarLayout
import com.wjez.app.R
import com.wjez.app.activity.allReviewActivity.AllReviewAcitivty
import com.wjez.app.adapter.ReviewsProfilePageAdapter
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.dialog.BlockDialog
import com.wjez.app.dialog.ReportDialog
import com.wjez.app.model.project16.HomeResponse
import com.wjez.app.model.project16.ProviderWithReviewResposne
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.DownloadListener
import com.wjez.app.tools.TemplateActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_profile_provider_details.*
import java.lang.Math.abs


class ProfileProviderDetailsActivity : TemplateActivity() {
    lateinit var blockDialog:BlockDialog
    lateinit var reportkDialog:ReportDialog
    lateinit var imageProfile:ImageView
    private var anim: Animation? = null
    var disposable= CompositeDisposable()
    lateinit var swip: SwipeRefreshLayout
    lateinit var tvNoData: TextView
    lateinit var rv: RecyclerView
    lateinit var adapter: ReviewsProfilePageAdapter
    lateinit var layoutManager: LinearLayoutManager

    override fun set_layout() {
        setContentView(R.layout.activity_profile_provider_details)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.profile_menu, menu)
        return true
    }





    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.getItemId()) {
            R.id.block -> {

                blockDialog=BlockDialog(this@ProfileProviderDetailsActivity)
                blockDialog.window!!.attributes.windowAnimations=R.style.dialogAnim
                blockDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                blockDialog.show()
                true
            }
            R.id.report -> {
                reportkDialog=ReportDialog(this@ProfileProviderDetailsActivity)
                reportkDialog.window!!.attributes.windowAnimations=R.style.dialogAnim
                reportkDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                reportkDialog.show()
                true
            }

            android.R.id.home->{
                BasicTools.exitActivity(this@ProfileProviderDetailsActivity)
                true
            }


            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun init_views() {
        setSupportActionBar(toolbar_profile)
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()?.setDisplayShowHomeEnabled(true)

        swip=swip_provider_review
        tvNoData=tv_no_data
        rv=rv_review
        toolbar_profile.setTitle(TemplateActivity.selectedProvider?.name)
        toolbar_layout.setTitle(TemplateActivity.selectedProvider?.name)

     var x=ContextCompat.getDrawable(this@ProfileProviderDetailsActivity, R.drawable.ic_arrow_wjiz_v2)
        getSupportActionBar()?.setHomeAsUpIndicator(x)



        imageProfile=iv_profile


    }

    override fun init_events() {

        layoutManager=LinearLayoutManager(this@ProfileProviderDetailsActivity,LinearLayoutManager.VERTICAL,false)
        adapter= ReviewsProfilePageAdapter(this@ProfileProviderDetailsActivity,ArrayList())
        rv.layoutManager=layoutManager
        rv.adapter=adapter



        tv_provider_name.setText(TemplateActivity.selectedProvider?.name)
        iv_online.visibility=View.INVISIBLE
        tv_provider_type.setText(TemplateActivity.selectedProvider?.userType)


        //ratingBar.rating= item?.provider?.rate!!.toFloat()

        if(TemplateActivity?.selectedProvider?.image!=null)
        BasicTools.loadImage(BasicTools.getUrlImg(this@ProfileProviderDetailsActivity,TemplateActivity?.selectedProvider?.image!!),imageProfile,object :
            DownloadListener {
            override fun completed(status: Boolean, bitmap: Bitmap) {
                anim = AnimationUtils.loadAnimation(this@ProfileProviderDetailsActivity, android.R.anim.fade_in)
                anim!!.setDuration(1000)
                imageProfile.animation= anim
            }
        })



        app_bar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->

            when {
                //  State Expanded
                verticalOffset == 0 -> root_card_details.visibility=View.VISIBLE//  Do anything for Expanded State
                    //  State Collapsed
                    abs(verticalOffset) >= appBarLayout.totalScrollRange ->root_card_details.visibility=View.GONE
                else -> root_card_details.visibility=View.VISIBLE
            }
        })




        swip.setOnRefreshListener { getAllData() }



        getAllData()





        tv_view_all_reviews.setOnClickListener {
            BasicTools.openActivity(this@ProfileProviderDetailsActivity,AllReviewAcitivty::class.java,false)
        }




    }

    override fun set_fragment_place() {

    }


    fun getAllData(){
        if (BasicTools.isConnected(this@ProfileProviderDetailsActivity)) {
            swip.isRefreshing=true
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@ProfileProviderDetailsActivity),
                BasicTools.getProtocol(this@ProfileProviderDetailsActivity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getReviews(TemplateActivity.selectedProvider!!.id.toString())
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ProviderWithReviewResposne>(this@ProfileProviderDetailsActivity) {
                        override fun onSuccess(result: ProviderWithReviewResposne) {

                            swip.isRefreshing=false

                                if(result.reviews!=null&&result.reviews!!.isNotEmpty()){
                                    root_see_all.visibility=View.VISIBLE
                                    tvNoData.visibility=View.GONE
                                    adapter.change_data(result.reviews!!)
                                    rv.adapter=adapter

                                }
                            else{
                                 //   root_see_all.visibility=View.GONE
                                    tvNoData.visibility=View.VISIBLE
                                }
                        }
                        override fun onFailed(status: Int) {
                            swip.isRefreshing=false
                          showToastMessage(R.string.faild)
                            //BasicTools.logOut(this@ProfileProviderDetailsActivity)
                        }
                    }))

        }
        else {

            swip.isRefreshing=false
            showToastMessage(R.string.no_connection)}
    }


}
