package com.wjez.app.activity.updateProfileProvider

interface IUpdateProfileProviderActivity {

    fun changeIndex(index:Int)
    fun changeIndexSub(index:Int)
}