package com.wjez.app.activity.signUp


import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.hbb20.CountryCodePicker
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword
import com.mobsandgeeks.saripaar.annotation.Email
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import com.mobsandgeeks.saripaar.annotation.Password
import com.wjez.app.R

import com.wjez.app.activity.homeActivity.HomeActivity
import com.wjez.app.activity.home_provider.HomeProvider
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.model.project16.TokenResponse
import com.wjez.app.model.project16.UserInfoResponse
import com.wjez.app.model.project21.MessageModel
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.Constants
import com.wjez.app.tools.TemplateActivity
import com.wjez.shop.tools.hide
import com.wjez.shop.tools.visible
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_signup_wjez.*

import kotlinx.android.synthetic.main.toolbar_backpress1.*
import okhttp3.ResponseBody

class SignUpAcitivty : TemplateActivity(), Validator.ValidationListener{
    lateinit var firebaseAuth: FirebaseAuth
    lateinit var refernce: DatabaseReference
    var disposable= CompositeDisposable()
    private var mAddImages =  ArrayList<Uri>()
    lateinit var ivProfile:ImageView
    var items=ArrayList<Uri>()
    var validator: Validator? = null


    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    @Password
    lateinit var editPasswrod: EditText

    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    @ConfirmPassword(messageResId = R.string.password_donot_match)
    lateinit var editRePasswrod: EditText

        @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
        lateinit var editfullName: EditText

        @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
        lateinit var editphone:EditText

    lateinit var countryCode: CountryCodePicker
    override fun set_layout() {
        setContentView(R.layout.activity_signup_wjez)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }



    fun showShimmerBtn(state: Boolean){

        if(state){
            card_sign_up.visibility= View.GONE
            shimmer_signup.visible()
        }else{
            card_sign_up.visibility= View.VISIBLE
            shimmer_signup.hide()
        }
    }

    override fun init_views() {


        editPasswrod=edit_password
        editfullName=edit_full_name
        editphone=edit_phone_number
        editRePasswrod=edit_re_password
        countryCode=country_code
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        // Called when you request permission to read and write to external storage
        when (requestCode) {
            Constants.REQUEST_READ_WRITE_STORAGE_PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    launchGallery()
                } else {
                    Toast.makeText(this, "permission_denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun launchGallery() {
        mAddImages = java.util.ArrayList()
        FilePickerBuilder.instance.setMaxCount(1)
            .setSelectedFiles(mAddImages)
            .setActivityTheme(R.style.filePickerActivityTheme)
            .pickPhoto(this@SignUpAcitivty)
    }

    override fun init_events() {


        firebaseAuth=FirebaseAuth.getInstance()

        validator = Validator(this)
        validator!!.setValidationListener(this)



        tv_login.setOnClickListener {
            BasicTools.exitActivity(this@SignUpAcitivty)
        }


        iv_back_signup.setOnClickListener {
            BasicTools.exitActivity(this)
        }
        eye_password.setOnClickListener {
            if(edit_password.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){


                eye_password.setImageResource(R.drawable.visibility_3x)
                edit_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                eye_password.setImageResource(R.drawable.visibility_off_3x)
                edit_password.setTransformationMethod(PasswordTransformationMethod.getInstance());

            }
        }



        eye_re_password.setOnClickListener {
            if(editRePasswrod.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){


                eye_re_password.setImageResource(R.drawable.visibility_3x)
                editRePasswrod.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                eye_re_password.setImageResource(R.drawable.visibility_off_3x)
                editRePasswrod.setTransformationMethod(PasswordTransformationMethod.getInstance());

            }
        }


        card_sign_up.setOnClickListener{
            validator!!.validate()

        }

     /*   iv_select_img.setOnClickListener {
            pickGalleryImages()

        }*/

    }


    private fun pickGalleryImages() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                Constants.REQUEST_READ_WRITE_STORAGE_PERMISSION_CODE
            )
        } else
            launchGallery()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // If the image capture activity was called and was successful
        if (requestCode == FilePickerConst.REQUEST_CODE_PHOTO) {
            if (resultCode == Activity.RESULT_OK && data != null) {


                //  mAddImages!!.clear()
                mAddImages.addAll(data.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_MEDIA) as ArrayList<Uri>)

                items = data.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_MEDIA)!!
                if(items==null){
                    Log.i("signup","IMAGES=NULL")
                }else{


                    if (items.isNotEmpty()){
                    Glide.with(this@SignUpAcitivty).load(items.get(0)).placeholder(
                        R.drawable.wjez_logo_v1
                    ).error(R.drawable.wjez_logo_v2).into(ivProfile)
                    TemplateActivity.photosUri.clear()
                    TemplateActivity.photosUri.add(items.get(0))
                    }

//                    for (item in items)
//                        imageAdapter.addItem(item, 0)

                }

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)

        }
    }

    override fun set_fragment_place() {

    }

    override fun onValidationFailed(errors: List<ValidationError>?) {
        BasicTools.showValidationErrorMessagesForFields(errors!!, this@SignUpAcitivty)
    }

    override fun onValidationSucceeded() {
       checkPhoneRQ(editphone.text.trim().toString())

      //  signUp()


    }

    fun checkPhoneRQ(txt:String){
        if (BasicTools.isConnected(this@SignUpAcitivty)) {



            showShimmerBtn(true)

            var map = HashMap<String, String>()
            map.put("phone",txt)
            val shopApi = ApiClient.getClient(BasicTools.getProtocol(this@SignUpAcitivty).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.checkPhoneWjez(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ResponseBody>(this@SignUpAcitivty) {
                        override fun onSuccess(result: ResponseBody) {


                            showShimmerBtn(false)
                            signUp()



                        }
                        override fun onFailed(status: Int) {

                            showShimmerBtn(false)
                            if(status==400){
                                showToastMessage(R.string.check_phone_hint)
                            }else
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            showShimmerBtn(false)
            showToastMessage(R.string.no_connection)}
    }



    fun signUp(){
        if (BasicTools.isConnected(this@SignUpAcitivty)) {



            showShimmerBtn(true)

            var map = HashMap<String, String>()
            map.put("user_type",Constants.LOGIN_TYPE_CONSTOMER)
            map.put("name",editfullName.text.toString().trim())
            map.put("phone",editphone.text.toString().trim())
            map.put("password",editPasswrod.text.toString().trim())
            map.put("c_password",editRePasswrod.text.toString().trim())
            map.put("country_code",countryCode.selectedCountryCode)

            signupCustomer=HashMap<String,String>()
            signupCustomer!!.putAll(map)
            val shopApi = ApiClient.getClient(
               BasicTools.getProtocol(this@SignUpAcitivty).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.signUpNormal(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<TokenResponse>(this@SignUpAcitivty) {
                        override fun onSuccess(result: TokenResponse) {


                            showShimmerBtn(false)

                            if(result.status!!){

                                BasicTools.setToken(this@SignUpAcitivty,result.token.toString())
                                BasicTools.setPhoneUser(this@SignUpAcitivty,editphone.text.toString().trim())
                                BasicTools.setPassword(this@SignUpAcitivty,editPasswrod.text.toString().trim())
                                getUserInfo(editPasswrod.text.toString())
                      /*          SignUpModel.email=editphone.text.trim().toString()
                                SignUpModel.passwordString=editPasswrod.text.trim().toString()
                                SignUpModel.fullName=editfullName.text.trim().toString()*/
                             //   BasicTools.openActivity(this@SignUpAcitivty, CompleteDataAcitivty::class.java,false)

                            }


                        }
                        override fun onFailed(status: Int) {

                            showShimmerBtn(false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            showShimmerBtn(false)
            showToastMessage(R.string.no_connection)}
    }

    private fun saveUserToFirebase(result: UserInfoResponse,pass:String) {
        firebaseAuth.createUserWithEmailAndPassword(result.phone!!,pass).addOnCompleteListener{

            if(it.isSuccessful){
                var firebaseUserID=firebaseAuth.currentUser!!.uid
                refernce= FirebaseDatabase.getInstance().reference.child("users").child(firebaseUserID)
                var mapUser=HashMap<String,Any>()
                mapUser.put("firebaseID",firebaseUserID)
                mapUser.put("username",result.phone!!)
                mapUser.put("password",pass)
                mapUser.put("name",result.name!!)
                mapUser.put("online",false)
                mapUser.put("user_type",Constants.LOGIN_TYPE_CONSTOMER)
                mapUser.put("server_id",result?.id.toString())
              //  mapUser.put("image",result?.image.toString())


                refernce.updateChildren(mapUser).addOnCompleteListener {

                    if(it.isSuccessful){


                        saveFirebaseUUID(firebaseUserID,result)


                    }else{

                        showToastMessage(R.string.faild)
                        Log.i("TEST_TEST","${it.exception}")
                    }




                }
            }else{
                showToastMessage(R.string.faild)
                Log.i("TEST_TEST",it.exception.toString())
            }


        }



    }

    fun getUserInfo(pass:String){
        if (BasicTools.isConnected(this@SignUpAcitivty)) {

            showShimmerBtn(true)
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@SignUpAcitivty),  BasicTools.getProtocol(this@SignUpAcitivty).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getUserInfo()
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<UserInfoResponse>(this@SignUpAcitivty) {
                        override fun onSuccess(result: UserInfoResponse) {
                            showShimmerBtn(false)
                         TemplateActivity.wjezUser=result

                          //  BasicTools.setLoginType(this@SignUpAcitivty,Constants.LOGIN_TYPE_CONSTOMER)

                            BasicTools.setUserID(this@SignUpAcitivty,result.id.toString())


                            BasicTools.setLoginProviderCustomer(this@SignUpAcitivty,result!!.userType.toString())

                           // saveUserToFirebase(result,pass)



                                if(result.userType.toString().equals(Constants.LOGIN_TYPE_CONSTOMER))
                                BasicTools.openActivity(this@SignUpAcitivty,
                                    HomeActivity::class.java,true)
                            else if(result.userType.toString().equals(Constants.LOGIN_TYPE_PROVIDER))
                                BasicTools.openActivity(this@SignUpAcitivty,
                                    HomeProvider::class.java,true)



                         /*   BasicTools.openActivity(this@SignUpAcitivty,
                                HomeActivity::class.java,true)*/


                        }
                        override fun onFailed(status: Int) {

                            showShimmerBtn(false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            showShimmerBtn(false)
            showToastMessage(R.string.no_connection)}
    }




    fun saveFirebaseUUID(uuid:String,data: UserInfoResponse){
        if (BasicTools.isConnected(this@SignUpAcitivty)) {

            showShimmerBtn(true)

            var map=HashMap<String,String>()
            map.put("firebase_uuid","$uuid")
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@SignUpAcitivty),
                BasicTools.getProtocol(this@SignUpAcitivty).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.addUUIDToFirebase(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ResponseBody>(this@SignUpAcitivty) {
                        override fun onSuccess(result: ResponseBody) {
                            showShimmerBtn(false)


                        /*    if(data.userType.toString().equals(Constants.LOGIN_TYPE_CONSTOMER))
                                BasicTools.openActivity(this@SignUpAcitivty,
                                    HomeActivity::class.java,true)
                            else if(data.userType.toString().equals(Constants.LOGIN_TYPE_PROVIDER))
                                BasicTools.openActivity(this@SignUpAcitivty,
                                    HomeProvider::class.java,true)*/


                        }
                        override fun onFailed(status: Int) {

                            showShimmerBtn(false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            showShimmerBtn(false)
            showToastMessage(R.string.no_connection)}
    }



}
