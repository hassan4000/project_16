package com.wjez.app.activity.completeTaskPhone


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.cardview.widget.CardView
import com.wjez.app.R
import com.wjez.app.activity.homeActivity.HomeActivity
import com.wjez.app.activity.home_provider.HomeProvider
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.BasicTools.getLoginProviderCustomer
import com.wjez.app.tools.Constants
import com.wjez.app.tools.TemplateActivity
import kotlinx.android.synthetic.main.activity_verified_successfully.*

class CompleteTaskPhoneActivity : TemplateActivity() {
    lateinit var btnNext:CardView
    override fun set_layout() {
        setContentView(R.layout.activity_verified_successfully)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun init_views() {
        btnNext=card_contintue

    }

    override fun init_events() {


        btnNext.setOnClickListener {


            var result=getLoginProviderCustomer(this@CompleteTaskPhoneActivity)


            if(result.toString().equals(Constants.LOGIN_TYPE_CONSTOMER))
                BasicTools.openActivity(this@CompleteTaskPhoneActivity,
                    HomeActivity::class.java,true)
            else if(result.toString().equals(Constants.LOGIN_TYPE_PROVIDER))
                BasicTools.openActivity(this@CompleteTaskPhoneActivity,
                    HomeProvider::class.java,true)
        }

    }

    override fun set_fragment_place() {

    }


}
