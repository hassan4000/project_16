package com.wjez.app.activity.verificationSignupCustomer


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import android.widget.ImageView
import androidx.cardview.widget.CardView
import com.facebook.shimmer.ShimmerFrameLayout

import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.wjez.app.R
import com.wjez.app.activity.homeActivity.HomeActivity
import com.wjez.app.activity.home_provider.HomeProvider
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.model.project16.TokenResponse
import com.wjez.app.model.project16.UserInfoResponse
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.Constants
import com.wjez.app.tools.TemplateActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_v_code_phone.*
import java.util.concurrent.TimeUnit

class VerificationSignupCustomer : TemplateActivity() {
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    var auth: FirebaseAuth?=null
    lateinit var btnNext: CardView
    lateinit var editCode: EditText
    lateinit var shimmer:ShimmerFrameLayout
    lateinit var ivBack:ImageView

    var disposable= CompositeDisposable()
    private fun startPhoneNumberVerification(phoneNumber: String) {
        // [START start_phone_auth]


        Log.i("TEST_TEST",phoneNumber)
        val options = PhoneAuthOptions.newBuilder(auth!!)
            .setPhoneNumber("+$phoneNumber") // Phone number to verify
            .setTimeout(120L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(this@VerificationSignupCustomer) // Activity (for callback binding)
            .setCallbacks(callbacks) // OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
        // [END start_phone_auth]
    }


    private fun reStartPhoneNumberVerification(phoneNumber: String) {
        // [START start_phone_auth]
        Log.i("TEST_TEST",phoneNumber)
        val options = PhoneAuthOptions.newBuilder(auth!!)
            .setPhoneNumber("+$phoneNumber") // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(this@VerificationSignupCustomer) // Activity (for callback binding)
            .setCallbacks(callbacks)
            .setForceResendingToken(TemplateActivity.tokenResendOtp!!)// OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
        // [END start_phone_auth]
    }


    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        BasicTools.showShimmer(btnNext,shimmer,true)
        auth!!.signInWithCredential(credential)
            .addOnCompleteListener(this@VerificationSignupCustomer) { task ->
                if (task.isSuccessful) {

                    Log.d("TEST_TEST", "signInWithCredential:success")
                    BasicTools.showShimmer(btnNext,shimmer,false)
//                    val user = task.result?.user


                    signUp()
                    //counterDown!!.cancel()

                } else {
                    BasicTools.showShimmer(btnNext,shimmer,false)
                    // Sign in failed, display a message and update the UI
                    Log.w("TEST_TEST", "signInWithCredential:failure", task.exception)
                    showToastMessage("Sign in failed")

                }
            }
    }


    override fun onStart() {
        super.onStart()

    }

    override fun onStop() {
        super.onStop()

    }

    override fun set_layout() {
        setContentView(R.layout.activity_v_code_phone)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun init_views() {

        editCode=edit_code
        btnNext=card_contintue
        shimmer=shimmer_continue
        ivBack=iv_back
    }

    override fun init_events() {
        auth = Firebase.auth


        ivBack.setOnClickListener {
            BasicTools.exitActivity(this@VerificationSignupCustomer)
        }


        btnNext.setOnClickListener {


            var code=editCode.text.toString()

            if(code.isNullOrEmpty()){
                showToastMessage(R.string.enter_code)
                return@setOnClickListener  }

            Log.i("TEST_TEST","code $code / id ${TemplateActivity.storedVerificationId}")
            val credential = PhoneAuthProvider.getCredential(TemplateActivity.storedVerificationId!!, code)

            signInWithPhoneAuthCredential(credential)
        }


        callbacks =
            object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {


                override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                    Log.d("TEST_TEST", "onVerificationCompleted:$credential")
                    signInWithPhoneAuthCredential(credential)
                }

                override fun onVerificationFailed(e: FirebaseException) {
                    BasicTools.showShimmer(btnNext,shimmer,false)
                    Log.w("TEST_TEST", "onVerificationFailed", e)

                    when (e) {
                        is FirebaseAuthInvalidCredentialsException -> {
                            // Invalid request
                            showToastMessage("Invalid request")

                        }
                        is FirebaseTooManyRequestsException -> {
                            // The SMS quota for the project has been exceeded
                            showToastMessage("the SMS quota for the project has been exceeded")

                        }
                        else -> {
                            Log.e("TEST","error_here")
                            showToastMessage("${e.localizedMessage}")

                        }
                    }


                }

                override fun onCodeSent(
                    verificationId: String,
                    token: PhoneAuthProvider.ForceResendingToken
                ) {

                    Log.d("TEST_TEST", "onCodeSent:$verificationId")
                    TemplateActivity.storedVerificationId=verificationId
                    TemplateActivity.tokenResendOtp= token

                    storedVerificationId = verificationId
                   // resendToken = token



                }
            }



       var countrycode= signupCustomer!!.get("country_code")
        var phone= signupCustomer!!.get("phone")

        Log.i("TEST_TEST","${countrycode}${phone}")
        startPhoneNumberVerification("${countrycode}${TemplateActivity.phoneNumber}")


    }

    override fun set_fragment_place() {

    }

    fun signUp(){
        if (BasicTools.isConnected(this@VerificationSignupCustomer)) {



            BasicTools.showShimmer(btnNext,shimmer,true)



            val shopApi = ApiClient.getClient(
                BasicTools.getProtocol(this@VerificationSignupCustomer).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.signUpNormal(signupCustomer!!)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<TokenResponse>(this@VerificationSignupCustomer) {
                        override fun onSuccess(result: TokenResponse) {


                            BasicTools.showShimmer(btnNext,shimmer,false)

                            if(result.status!!){

                                BasicTools.setToken(this@VerificationSignupCustomer,result.token.toString())
                                BasicTools.setPhoneUser(this@VerificationSignupCustomer,
                                    signupCustomer!!.get("phone").toString().trim())
                                BasicTools.setPassword(this@VerificationSignupCustomer,signupCustomer!!.get("password").toString().trim())
                                getUserInfo(signupCustomer!!.get("password").toString())
                                /*          SignUpModel.email=editphone.text.trim().toString()


                                          SignUpModel.passwordString=editPasswrod.text.trim().toString()
                                          SignUpModel.fullName=editfullName.text.trim().toString()*/
                                //   BasicTools.openActivity(this@VerificationSignupCustomer, CompleteDataAcitivty::class.java,false)

                            }


                        }
                        override fun onFailed(status: Int) {

                            BasicTools.showShimmer(btnNext,shimmer,false)
                            BasicTools.exitActivity(this@VerificationSignupCustomer)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {

            showToastMessage(R.string.no_connection)}
    }


    fun getUserInfo(pass:String){
        if (BasicTools.isConnected(this@VerificationSignupCustomer)) {

            BasicTools.showShimmer(btnNext,shimmer,true)
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@VerificationSignupCustomer),  BasicTools.getProtocol(this@VerificationSignupCustomer).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getUserInfo()
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<UserInfoResponse>(this@VerificationSignupCustomer) {
                        override fun onSuccess(result: UserInfoResponse) {
                            BasicTools.showShimmer(btnNext,shimmer,false)
                            TemplateActivity.wjezUser=result

                            //  BasicTools.setLoginType(this@VerificationSignupCustomer,Constants.LOGIN_TYPE_CONSTOMER)

                            BasicTools.setUserID(this@VerificationSignupCustomer,result.id.toString())


                            BasicTools.setLoginProviderCustomer(this@VerificationSignupCustomer,result!!.userType.toString())

                            // saveUserToFirebase(result,pass)



                            if(result.userType.toString().equals(Constants.LOGIN_TYPE_CONSTOMER))
                                BasicTools.openActivity(this@VerificationSignupCustomer,
                                    HomeActivity::class.java,true)
                            else if(result.userType.toString().equals(Constants.LOGIN_TYPE_PROVIDER))
                                BasicTools.openActivity(this@VerificationSignupCustomer,
                                    HomeProvider::class.java,true)



                            /*   BasicTools.openActivity(this@VerificationSignupCustomer,
                                   HomeActivity::class.java,true)*/


                        }
                        override fun onFailed(status: Int) {

                            BasicTools.showShimmer(btnNext,shimmer,false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {

            showToastMessage(R.string.no_connection)}
    }


}
