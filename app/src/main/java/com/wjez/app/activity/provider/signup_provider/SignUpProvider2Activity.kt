package com.wjez.app.activity.provider.signup_provider


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.util.Log
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import com.wjez.app.R
import com.wjez.app.activity.homeActivity.HomeActivity
import com.wjez.app.activity.home_provider.HomeProvider
import com.wjez.app.adapter.CategoryGridAdapter
import com.wjez.app.adapter.CountrySpinnerAdapter
import com.wjez.app.adapter.SpacesItemDecoration
import com.wjez.app.adapter.SubCategoryHorizantleAdapter
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.model.project16.*
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.Constants
import com.wjez.app.tools.TemplateActivity
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


import kotlinx.android.synthetic.main.activity_signup2_provider.*
import kotlinx.android.synthetic.main.activity_signup2_provider.shimmer_next
import kotlinx.android.synthetic.main.activity_signup2_provider.shimmer_wait_spinner
import kotlinx.android.synthetic.main.activity_signup2_provider.spinner_cities



import kotlinx.android.synthetic.main.toolbar_signup_provider.*
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class SignUpProvider2Activity : TemplateActivity(), Validator.ValidationListener,ISignUpProvider2Activity{


    lateinit var firebaseAuth: FirebaseAuth
    lateinit var refernce: DatabaseReference


    var disposable= CompositeDisposable()
    var disposableCategory= CompositeDisposable()
    var disposableCity= CompositeDisposable()
    var mainCategoryId=""
    var subCategoryIds=ArrayList<String>()
    private var mAddImages =  ArrayList<Uri>()
    lateinit var ivProfile: ImageView
    var items=ArrayList<Uri>()
    var validator: Validator? = null
    var selectPic=false

    lateinit var spinnerAdapter: CountrySpinnerAdapter
    lateinit var rvCategoryList:RecyclerView
    lateinit var rvSubCategoryList:RecyclerView
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var linearLayoutManager:LinearLayoutManager

    lateinit var gridAdapter: CategoryGridAdapter
    lateinit var linearAdapter: SubCategoryHorizantleAdapter


    @SuppressLint("NonConstantResourceId")
    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    lateinit var editLang: EditText

    @SuppressLint("NonConstantResourceId")
    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    lateinit var editBio: EditText
    override fun set_layout() {
        setContentView(R.layout.activity_signup2_provider)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun init_views() {
        spinnerAdapter= CountrySpinnerAdapter(this@SignUpProvider2Activity, ArrayList())
        spinner_cities.adapter=spinnerAdapter
        editLang=edit_lang
        editBio=edit_bio

        rvCategoryList=rv_category
        rvSubCategoryList=rv_sub_category

        gridAdapter= CategoryGridAdapter(this,ArrayList(),this)
        linearAdapter= SubCategoryHorizantleAdapter(this,ArrayList(),this)

        rvCategoryList.adapter=gridAdapter
        rvSubCategoryList.adapter=linearAdapter

        gridLayoutManager=GridLayoutManager(this@SignUpProvider2Activity,2)
        linearLayoutManager= LinearLayoutManager(this@SignUpProvider2Activity,LinearLayoutManager.HORIZONTAL,false)

        rvCategoryList.layoutManager=gridLayoutManager
        rvSubCategoryList.layoutManager=linearLayoutManager

        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.d0_2)
        val spacingInPixels2 = resources.getDimensionPixelSize(R.dimen.d0_8)
        rvCategoryList.addItemDecoration( SpacesItemDecoration(spacingInPixels))
     //   rvSubCategoryList.addItemDecoration( SpacesItemDecoration(spacingInPixels2))

    }

    override fun init_events() {

        firebaseAuth=FirebaseAuth.getInstance()


        TemplateActivity.photosUri.clear()
        validator = Validator(this)
        validator!!.setValidationListener(this)
        iv_back.setOnClickListener {
            BasicTools.exitActivity(this@SignUpProvider2Activity)
        }

        card_next2.setOnClickListener {
            validator!!.validate()
        }

        iv_select_img_provider.setOnClickListener {
            pickGalleryImages()
        }

        getCategory()
        getCities()


    }

    override fun set_fragment_place() {

    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        // Called when you request permission to read and write to external storage
        when (requestCode) {
            Constants.REQUEST_READ_WRITE_STORAGE_PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    launchGallery()
                } else {
                    Toast.makeText(this, "permission_denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun launchGallery() {
        mAddImages = java.util.ArrayList()
        FilePickerBuilder.instance.setMaxCount(1)
            .setSelectedFiles(mAddImages)
            .setActivityTheme(R.style.filePickerActivityTheme)
            .pickPhoto(this@SignUpProvider2Activity)
    }

    private fun pickGalleryImages() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                Constants.REQUEST_READ_WRITE_STORAGE_PERMISSION_CODE
            )
        } else
            launchGallery()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // If the image capture activity was called and was successful
        if (requestCode == FilePickerConst.REQUEST_CODE_PHOTO) {
            if (resultCode == Activity.RESULT_OK && data != null) {


                //  mAddImages!!.clear()
                mAddImages.addAll(data.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_MEDIA) as ArrayList<Uri>)

                items = data.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_MEDIA)!!
                if(items==null){
                    Log.i("signup","IMAGES=NULL")
                }else{


                    if (items.isNotEmpty()){
                        Glide.with(this@SignUpProvider2Activity).load(items.get(0)).placeholder(
                            R.drawable.wjez_logo_v2
                        ).error(R.drawable.wjez_logo_v2).into(iv_profile_provider)



                        selectPic=true
                        TemplateActivity.photosUri.clear()
                        TemplateActivity.photosUri.add(items.get(0))
                    }

//                    for (item in items)
//                        imageAdapter.addItem(item, 0)

                }

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)

        }
    }

    override fun onValidationFailed(errors: MutableList<ValidationError>?) {
        BasicTools.showValidationErrorMessagesForFields(errors!!, this@SignUpProvider2Activity)
    }

    fun getSelectedMainCategory():String{

        var listCatrgoey=ArrayList<HomeResponse.Data.Category>()
        listCatrgoey.addAll(gridAdapter.getList())

        for(i in listCatrgoey){
            if(i.selected!!) {
                return i.id.toString()}

        }

        return  ""

    }

    fun getSelectedSubMainCategory():ArrayList<String>{


        var listSub=ArrayList<HomeResponse.Data.Category>()
        var listSubSelected=ArrayList<String>()
        listSub.addAll(linearAdapter.getList())

        if(listSub.get(0).selected!!)
            return  ArrayList<String>()

        else
        for(i in listSub){
            if(i.selected!!)
                listSubSelected.add(i.id.toString())

        }

        return  listSubSelected

    }

    override fun onValidationSucceeded() {


        if(spinner_cities.selectedItem==null) {

            showToastMessage(R.string.plz_select_location)
            return
        }

        if(TemplateActivity.photosUri.isNullOrEmpty()) {

            showToastMessage(R.string.select_photo)
            return
        }

       var  citiesID=((spinner_cities.selectedItem) as CountryModel).countryId


        mainCategoryId=getSelectedMainCategory()
        if(mainCategoryId.isEmpty()){
            showToastMessage(R.string.select_categoey)
            return
        }

        subCategoryIds.clear()
        subCategoryIds.addAll(getSelectedSubMainCategory())





        updatePic()


    }

    private fun getFile(uri: Uri): File?{
        val parcelFileDescriptor = contentResolver.openFileDescriptor(uri, "r", null)

        parcelFileDescriptor?.let {
            val inputStream = FileInputStream(parcelFileDescriptor.fileDescriptor)
            val file = File(cacheDir, getFileName(uri))
            val outputStream = FileOutputStream(file)

            inputStream.copyTo(outputStream)
            inputStream.close()
            outputStream.close()
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                FileUtils.copy(inputStream,outputStream)
            }*/
            return file
        }
        return null
    }

    fun getFileName(fileUri: Uri): String {

        var name = ""
        val returnCursor = contentResolver.query(fileUri, null, null, null, null)
        if (returnCursor != null) {
            val nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
            returnCursor.moveToFirst()
            name = returnCursor.getString(nameIndex)
            returnCursor.close()
        }

        return name
    }



    fun getCategory(){
        if (BasicTools.isConnected(this@SignUpProvider2Activity)) {





            BasicTools.showShimmer(rvCategoryList,shimmer_category_grid,true)
            val shopApi = ApiClient.getClient(
                BasicTools.getProtocol(this@SignUpProvider2Activity).toString())
                ?.create(AppApi::class.java)

            val observable= shopApi!!.getCategoryList()
            disposableCategory.clear()
            disposableCategory.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<CategeoryListResponse>(this@SignUpProvider2Activity) {
                        override fun onSuccess(result: CategeoryListResponse) {


                            BasicTools.showShimmer(rvCategoryList,shimmer_category_grid,false)


                            gridAdapter.change_data(result!!.data!!)






                        }
                        override fun onFailed(status: Int) {
                            BasicTools.showShimmer(rvCategoryList,shimmer_category_grid,false)

                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            BasicTools.showShimmer(rvCategoryList,shimmer_category_grid,false)
            showToastMessage(R.string.no_connection)}
    }

    override fun changeIndex(index: Int) {

        var list=ArrayList<HomeResponse.Data.Category>()

        list.addAll(gridAdapter.getList())
        for(i in list)
            i.selected=false



        Log.i("LIST","---------------------------")
        var item=list.get(index)
        item.selected=true
        list.set(index,item)


        gridAdapter.change_data(list)
        rvCategoryList.adapter=gridAdapter




        var allItem=HomeResponse.Data.Category(-1,null,"","All","الكل","", ArrayList(),true)
        if(item.subCategory==null||item.subCategory.isNullOrEmpty()){
            item.subCategory= ArrayList()
        item.subCategory!!.add(allItem)}


        else{
            if(item.subCategory!!.get(0).id!=-1)
        item.subCategory!!.add(0,allItem)
        else
                item.subCategory!!.set(0,allItem)
        }

        linearAdapter.change_data(item.subCategory!!)


    }

    override fun changeIndexSub(index: Int) {

        var list=ArrayList<HomeResponse.Data.Category>()

        list.addAll(linearAdapter.getList())

        var item=list.get(0)
        var itemIndex=list.get(index)

        if(index==0){
            for(i in list)
                i.selected=false
        item.selected=true
        list.set(0,item)


        }else{
            itemIndex.selected=!itemIndex.selected!!
            item.selected=false
            list.set(0,item)
            list.set(index,itemIndex)

        }


        linearAdapter.change_data(list)


    }


    fun getCities(){
        if (BasicTools.isConnected(this@SignUpProvider2Activity)) {
            BasicTools.showShimmer(spinner_cities,shimmer_wait_spinner,true)


            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@SignUpProvider2Activity),
                BasicTools.getProtocol(this@SignUpProvider2Activity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getCountryWjez()
            disposableCity.clear()
            disposableCity.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<CountryResponseModel>(this@SignUpProvider2Activity) {
                        override fun onSuccess(result: CountryResponseModel) {
                            BasicTools.showShimmer(spinner_cities,shimmer_wait_spinner,false)

                            spinnerAdapter= CountrySpinnerAdapter(this@SignUpProvider2Activity,result.data!!)
                            spinner_cities.adapter=spinnerAdapter

                        }
                        override fun onFailed(status: Int) {

                            BasicTools.showShimmer(spinner_cities,shimmer_wait_spinner,false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            BasicTools.showShimmer(spinner_cities,shimmer_wait_spinner,false)
            showToastMessage(R.string.no_connection)}
    }



    fun signUp(){
        if (BasicTools.isConnected(this@SignUpProvider2Activity)) {




            var body=SignupBody()
            body.name=TemplateActivity.nameProvider
            body.phone=TemplateActivity.phoneNumberProvider
            body.password=TemplateActivity.passwordProvider
            body.c_password=TemplateActivity.repasswordProvider
            body.user_type=Constants.LOGIN_TYPE_PROVIDER
            body.talking_lang=editLang.text.toString().trim()
            body.document_num=TemplateActivity.idNumberProvider
            body.document_id=TemplateActivity.docIDProvider
            body.bio=editBio.text.toString()
            body.birthday=TemplateActivity.birthdayProvider
            body.city_id=((spinner_cities.selectedItem) as CountryModel).countryId.toString()
            body.image_id=TemplateActivity.pictureIDProvider

            if(subCategoryIds.isNullOrEmpty())
                body.main_category_id=mainCategoryId
            else {
                body.sub_category_id=ArrayList()
                body.sub_category_id!!.addAll(subCategoryIds)
            }







            var map= HashMap<String,Any>()
            map.put("name",TemplateActivity.nameProvider)
            map.put("phone",TemplateActivity.phoneNumberProvider)
            map.put("password",TemplateActivity.passwordProvider)
            map.put("c_password",TemplateActivity.repasswordProvider)
            map.put("user_type",Constants.LOGIN_TYPE_PROVIDER)
            map.put("talking_lang",editLang.text.toString().trim())
            map.put("document_num",TemplateActivity.idNumberProvider)
            map.put("document_id",TemplateActivity.docIDProvider)
            map.put("sub_category_id",linearAdapter.getList())
            map.put("main_category_id",gridAdapter.getList())
            map.put("bio",editBio.text.toString())
            map.put("birthday",TemplateActivity.birthdayProvider)
            map.put("city_id",((spinner_cities.selectedItem) as CountryModel).countryId.toString())


            BasicTools.showShimmer(card_next2,shimmer_next,true)
            val shopApi = ApiClient.getClient(
                BasicTools.getProtocol(this@SignUpProvider2Activity).toString())
                ?.create(AppApi::class.java)

            val observable= shopApi!!.signupProvider(body)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<TokenResponse>(this@SignUpProvider2Activity) {
                        override fun onSuccess(result: TokenResponse) {




                            BasicTools.setToken(this@SignUpProvider2Activity,result.token.toString())
                            BasicTools.setPhoneUser(this@SignUpProvider2Activity,TemplateActivity.phoneNumberProvider)
                            BasicTools.setPassword(this@SignUpProvider2Activity,TemplateActivity.passwordProvider)


                            getUserInfoProvider()






                            BasicTools.showShimmer(card_next2,shimmer_next,false)
                        }
                        override fun onFailed(status: Int) {

                            BasicTools.showShimmer(card_next2,shimmer_next,false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            BasicTools.showShimmer(card_next2,shimmer_next,false)
            showToastMessage(R.string.no_connection)}
    }




    fun getUserInfoProvider(){
        if (BasicTools.isConnected(this@SignUpProvider2Activity)) {



            BasicTools.showShimmer(card_next2,shimmer_next,true)


            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@SignUpProvider2Activity),  BasicTools.getProtocol(this@SignUpProvider2Activity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getUserProviderInfo()
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<UserInfoDataResponse>(this@SignUpProvider2Activity) {
                        override fun onSuccess(result: UserInfoDataResponse) {
                            BasicTools.showShimmer(card_next2,shimmer_next,false)

                            TemplateActivity.wjezUser=result.data
                            BasicTools.setLoginProviderCustomer(this@SignUpProvider2Activity,result.data!!.userType.toString())



                            BasicTools.setUserID(this@SignUpProvider2Activity,result.data?.id.toString())


                             nameProvider=""
                             phoneNumberProvider=""
                             birthdayProvider=""
                             idNumberProvider=""
                             docIDProvider=""
                             pictureIDProvider=""
                             passwordProvider=""
                             repasswordProvider=""
                             cityIdProvider=""
                             langProvider=""
                             bioProvider=""


                            Log.i("TEST_TEST","${result.data!!.userType}/${Constants.LOGIN_TYPE_PROVIDER}")
                            Log.i("TEST_TEST","${result.data!!.userType.toString().equals(Constants.LOGIN_TYPE_PROVIDER)}")



                            if(result.data!!.userType.toString().equals(Constants.LOGIN_TYPE_CONSTOMER))
                                BasicTools.openActivity(this@SignUpProvider2Activity,
                                    HomeActivity::class.java,true)
                            else if(result.data!!.userType.toString().equals(Constants.LOGIN_TYPE_PROVIDER))
                                BasicTools.openActivity(this@SignUpProvider2Activity,
                                    HomeProvider::class.java,true)

                          //  saveUserToFirebase(result.data!!,TemplateActivity.passwordProvider)




                        }
                        override fun onFailed(status: Int) {

                            BasicTools.showShimmer(card_next2,shimmer_next,false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            BasicTools.showShimmer(card_next2,shimmer_next,false)
            showToastMessage(R.string.no_connection)}
    }

    private fun saveUserToFirebase(result: UserInfoResponse,pass:String) {





        firebaseAuth.createUserWithEmailAndPassword(result.phone!!,pass).addOnCompleteListener{

            if(it.isSuccessful){
                var firebaseUserID=firebaseAuth.currentUser!!.uid
                refernce= FirebaseDatabase.getInstance().reference.child("users").child(firebaseUserID)
                var mapUser=HashMap<String,Any>()
                mapUser.put("firebaseID",firebaseUserID)
                mapUser.put("username",result.phone!!)
                mapUser.put("password",pass)
                mapUser.put("name",result.name!!)
                mapUser.put("online",false)
                mapUser.put("user_type",Constants.LOGIN_TYPE_PROVIDER)
                mapUser.put("server_id",result?.id.toString())
                mapUser.put("image",result?.image.toString())


                refernce.updateChildren(mapUser).addOnCompleteListener {

                    if(it.isSuccessful){


                      //  saveFirebaseUUID(firebaseUserID,result)
             /*           if(result.userType.toString().equals(Constants.LOGIN_TYPE_CONSTOMER))
                            BasicTools.openActivity(this@SignUpProvider2Activity,
                                HomeActivity::class.java,true)
                        else if(result.userType.toString().equals(Constants.LOGIN_TYPE_PROVIDER))
                            BasicTools.openActivity(this@SignUpProvider2Activity,
                                HomeProvider::class.java,true)*/

                    }else{

                        showToastMessage(R.string.faild)
                        Log.i("TEST_TEST","${it.exception}")
                    }




                }
            }else{
                showToastMessage(R.string.faild)
                Log.i("TEST_TEST",it.exception.toString())
            }


        }



    }


    fun saveFirebaseUUID(uuid:String,data: UserInfoResponse){

        if (BasicTools.isConnected(this@SignUpProvider2Activity)) {



            BasicTools.showShimmer(card_next2,shimmer_next,true)


            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@SignUpProvider2Activity),
                BasicTools.getProtocol(this@SignUpProvider2Activity).toString())
                ?.create(AppApi::class.java)

            var map=HashMap<String,String>()
            map.put("firebase_uuid","$uuid")
            val observable= shopApi!!.addUUIDToFirebase(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ResponseBody>(this@SignUpProvider2Activity) {
                        override fun onSuccess(result: ResponseBody) {
                            BasicTools.showShimmer(card_next2,shimmer_next,false)
                            if(data.userType.toString().equals(Constants.LOGIN_TYPE_CONSTOMER))
                                BasicTools.openActivity(this@SignUpProvider2Activity,
                                    HomeActivity::class.java,true)
                            else if(data.userType.toString().equals(Constants.LOGIN_TYPE_PROVIDER))
                                BasicTools.openActivity(this@SignUpProvider2Activity,
                                    HomeProvider::class.java,true)



                        }
                        override fun onFailed(status: Int) {

                            BasicTools.showShimmer(card_next2,shimmer_next,false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            BasicTools.showShimmer(card_next2,shimmer_next,false)
            showToastMessage(R.string.no_connection)}
    }


    fun updatePic(){
        if (BasicTools.isConnected(this@SignUpProvider2Activity)) {


            val file = getFile(TemplateActivity.photosUri.get(0))!!
            val requestFile = RequestBody.Companion.create("multipart/form-data".toMediaTypeOrNull(), file)
            val bodyImage = MultipartBody.Part.createFormData("image", file.getName(), requestFile)






            BasicTools.showShimmer(card_next2,shimmer_next,true)
            val shopApi = ApiClient.getClient(
                BasicTools.getProtocol(this@SignUpProvider2Activity).toString())
                ?.create(AppApi::class.java)

            val observable= shopApi!!.uploadProviderPic(bodyImage)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<DocumnetIdResponse>(this@SignUpProvider2Activity) {
                        override fun onSuccess(result: DocumnetIdResponse) {







                            pictureIDProvider=result.data?.id.toString()
                            BasicTools.showShimmer(card_next2,shimmer_next,false)


                            signUp()

                        }
                        override fun onFailed(status: Int) {

                            BasicTools.showShimmer(card_next2,shimmer_next,false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            BasicTools.showShimmer(card_next2,shimmer_next,false)
            showToastMessage(R.string.no_connection)}
    }


}
