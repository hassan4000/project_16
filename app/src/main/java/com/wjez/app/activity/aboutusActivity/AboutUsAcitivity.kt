package com.wjez.app.activity.aboutusActivity


import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.widget.TextView
import com.wjez.app.R
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.fragment.project21.settingModel.SettingModel
import com.wjez.app.model.project16.AboutUsModel
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.TemplateActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_about_us.*
import kotlinx.android.synthetic.main.toolbar_about_us.*


class AboutUsAcitivity : TemplateActivity() {
    var disposable= CompositeDisposable()
    lateinit var   tvVersion:TextView
    lateinit var   tvEmail:TextView
    override fun set_layout() {
        setContentView(R.layout.activity_about_us)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun init_views() {
        tvVersion=tv_version
        tvEmail=tv_email
    }

    @SuppressLint("SetTextI18n")
    override fun init_events() {

        iv_back.setOnClickListener {
            BasicTools.exitActivity(this@AboutUsAcitivity)
        }


        val context: Context = applicationContext // or activity.getApplicationContext()

        val packageManager: PackageManager = context.getPackageManager()
        val packageName: String = context.getPackageName()

        var myVersionName: String? = "" // initialize String


        try {
            myVersionName = packageManager.getPackageInfo(packageName, 0).versionName

            tvVersion.setText("${resources.getString(R.string.version_txt)} $myVersionName")


        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }


        RQ()
        swip_about_us.setOnRefreshListener {
            RQ()
        }

    }

    override fun set_fragment_place() {

    }




    fun RQ(){
        if (BasicTools.isConnected(this@AboutUsAcitivity)) {
            swip_about_us.isRefreshing=true
            val shopApi = ApiClient.getClient(
              BasicTools.getProtocol(this@AboutUsAcitivity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getAboutUs()
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<AboutUsModel>(this@AboutUsAcitivity) {
                        override fun onSuccess(result: AboutUsModel) {


                            swip_about_us.isRefreshing=false



                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                if(!BasicTools.isDeviceLanEn())
                                tv_about_us.setText(Html.fromHtml(result.ar?.information.toString(), Html.FROM_HTML_MODE_COMPACT))
                                else   tv_about_us.setText(Html.fromHtml(result.en?.information.toString(), Html.FROM_HTML_MODE_COMPACT))
                            } else {
                                if(!BasicTools.isDeviceLanEn())
                                tv_about_us.setText(Html.fromHtml(result.ar?.information.toString()))
                                else  tv_about_us.setText(Html.fromHtml(result.en?.information.toString()))
                            }



                            tvEmail.setText(result.en?.email)





                        }
                        override fun onFailed(status: Int) {

                            swip_about_us.isRefreshing=false

                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            swip_about_us.isRefreshing=false

            showToastMessage(R.string.no_connection)}
    }
}
