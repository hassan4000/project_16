package com.wjez.app.activity.login_type


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.wjez.app.R
import com.wjez.app.activity.loginActivity.LoginAcitivty
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.Constants
import com.wjez.app.tools.TemplateActivity
import kotlinx.android.synthetic.main.activity_login_type.*

class LoginTypeActivity : TemplateActivity() {
    override fun set_layout() {
        setContentView(R.layout.activity_login_type)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun init_views() {

        root_customer.setOnClickListener {
            TemplateActivity.loginType=Constants.LOGIN_TYPE_CONSTOMER

            BasicTools.openActivity(this@LoginTypeActivity,LoginAcitivty::class.java,false)
        }

        root_provider.setOnClickListener {
            TemplateActivity.loginType=Constants.LOGIN_TYPE_PROVIDER
            BasicTools.openActivity(this@LoginTypeActivity,LoginAcitivty::class.java,false)

        }

    }

    override fun init_events() {

    }

    override fun set_fragment_place() {

    }


}
