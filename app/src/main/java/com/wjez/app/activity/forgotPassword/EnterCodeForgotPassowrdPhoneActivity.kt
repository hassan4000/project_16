package com.wjez.app.activity.forgotPassword


import android.annotation.SuppressLint
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import com.wjez.app.R
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.TemplateActivity
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import kotlinx.android.synthetic.main.activity_enter_code.*
import kotlinx.android.synthetic.main.toolbar_backpress1.*
import java.util.concurrent.TimeUnit

class EnterCodeForgotPassowrdPhoneActivity : TemplateActivity() {
    var auth: FirebaseAuth?=null
    var counterDown: CountDownTimer?=null
    override fun set_layout() {
        setContentView(R.layout.activity_enter_code)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    fun firebasePhone(){


        btn_next.visibility=View.GONE
        card_resend.visibility=View.GONE
        counterDown!!.cancel()
        counterDown!!.start()
        Log.i("TEST_TEST","${TemplateActivity.SignUpModel.countryCode}${TemplateActivity.SignUpModel.phoneNumber}")
        auth = FirebaseAuth.getInstance()
        val firebaseAuthSettings = auth!!.firebaseAuthSettings
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            "${TemplateActivity.countryCodeForgotPasswrod}${TemplateActivity.phoneForgotPasswrod}", // Phone number to verify
            60, // Timeout duration
            TimeUnit.SECONDS, // Unit of timeout
            this, // Activity (for callback binding)
            object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                    // This callback will be invoked in two situations:
                    // 1 - Instant verification. In some cases the phone number can be instantly
                    //     verified without needing to send or enter a verification code.
                    // 2 - Auto-retrieval. On some devices Google Play services can automatically
                    //     detect the incoming verification SMS and perform verification without
                    //     user action.
                    Log.d("TAG", "onVerificationCompleted:$credential")

                    auth!!.signInWithCredential(credential).addOnCompleteListener(this@EnterCodeForgotPassowrdPhoneActivity) { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("TAG", "signInWithCredential:success")




                            val user = task.result?.user

                            TemplateActivity.SignUpModel.smsCode=credential.smsCode.toString()
                            edit_1.setText(credential.smsCode.toString().substring(0,1))
                            edit_2.setText(credential.smsCode.toString().substring(1,2))
                            edit_3.setText(credential.smsCode.toString().substring(2,3))
                            edit_4.setText(credential.smsCode.toString().substring(3,4))
                            edit_5.setText(credential.smsCode.toString().substring(4,5))
                            edit_6.setText(credential.smsCode.toString().substring(5,6))
                            counterDown!!.cancel()

                            BasicTools.openActivity(this@EnterCodeForgotPassowrdPhoneActivity,ForgotPassowrdActivity::class.java,true)
                            // ...
                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w("TAG", "signInWithCredential:failure", task.exception)
                            showToastMessage("signInWithCredential:failure  ${task.exception}")
                            if (task.exception is FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid


                            }
                        }
                    }



                    //  signInWithPhoneAuthCredential(credential)
                }




                override fun onVerificationFailed(e: FirebaseException) {
                    // This callback is invoked in an invalid request for verification is made,
                    // for instance if the the phone number format is not valid.
                    Log.w("TAG", "onVerificationFailed", e)
                    showToastMessage("the phone number format is not valid")

                    if (e is FirebaseAuthInvalidCredentialsException) {
                        Log.i("TEST_TEST",e.message.toString())
                        // Invalid request
                        showToastMessage(" Invalid request ${e.message.toString()}")
                        // ...
                    } else if (e is FirebaseTooManyRequestsException) {

                        // The SMS quota for the project has been exceeded
                           showToastMessage("  SMS quota for the project has been exceeded")
                    }

                    // Show a message and update the UI
                    // ...
                }

                override fun onCodeSent(
                    verificationId: String,
                    token: PhoneAuthProvider.ForceResendingToken
                ) {
                    // The SMS verification code has been sent to the provided phone number, we
                    // now need to ask the user to enter the code and then construct a credential
                    // by combining the code with a verification ID.
                    Log.d("TAG", "onCodeSent:$verificationId")


                    // Save verification ID and resending token so we can use them later
                    // storedVerificationId = verificationId
                    //  resendToken = token

                    Log.i("TEST_TEST","token"+token)
                    Log.i("TEST_TEST","verificationId"+verificationId)




                    // ...
                }
            })
    }
    override fun init_views() {



    }


    @SuppressLint("SetTextI18n")
    override fun init_events() {

        tv_number.setText("${TemplateActivity.SignUpModel.countryCode}${TemplateActivity.SignUpModel.phoneNumber}")

        card_resend.setOnClickListener {
            firebasePhone()
        }

        counterDown=object : CountDownTimer(60000,1000){
            override fun onFinish() {
                tv_resend.setText(" 00:00 ")
                btn_next.visibility= View.VISIBLE
                card_resend.visibility= View.VISIBLE

                /*     if(BasicTools.isConnected(parent!!))
                     getverifyCodeRQ()
                     else
                         parent!!.showToastMessage(R.string.no_connection)*/

            }

            @SuppressLint("SetTextI18n")
            override fun onTick(p0: Long) {

                val seconds :Int= (p0 / 1000).toInt()
                tv_resend.setText("00:"+  BasicTools.normalNumber(seconds.toString())+" ")
            }

        }.start()


        iv_back.setOnClickListener {
            TemplateActivity.SignUpModel.smsCode=""
            BasicTools.exitActivity(this@EnterCodeForgotPassowrdPhoneActivity)
        }
        btn_next.setOnClickListener {

            showToastMessage(TemplateActivity.SignUpModel.smsCode.length.toString())
            var code=edit_1.text.toString().trim()+edit_2.text.toString().trim()+edit_3.text.toString().trim()+edit_4.text.toString().trim()+edit_5.text.toString().trim()+edit_6.text.toString().trim()

            if(TemplateActivity.SignUpModel.smsCode.isNotEmpty()) {

                counterDown!!.cancel()
                  BasicTools.openActivity(this@EnterCodeForgotPassowrdPhoneActivity,ForgotPassowrdActivity::class.java,true)
            }

            else if(TemplateActivity.SignUpModel.smsCode.equals(code)&&code.isNotEmpty()){
                counterDown!!.cancel()

                BasicTools.openActivity(this@EnterCodeForgotPassowrdPhoneActivity,ForgotPassowrdActivity::class.java,true)
            }
            else if(!TemplateActivity.SignUpModel.smsCode.equals(code)){

                showToastMessage(R.string.enter_code_wrong_code)
            }

            else {

            }

        }

        firebasePhone()

    }

    override fun set_fragment_place() {

    }


}
