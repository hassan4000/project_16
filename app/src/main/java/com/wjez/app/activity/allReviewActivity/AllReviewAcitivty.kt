package com.wjez.app.activity.allReviewActivity


import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.wjez.app.R
import com.wjez.app.adapter.ReviewsProfilePageAdapter
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.model.project16.ProviderWithReviewResposne
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.TemplateActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_all_review_list.*
import kotlinx.android.synthetic.main.activity_all_review_list.rv_review
import kotlinx.android.synthetic.main.activity_all_review_list.tv_no_data


import kotlinx.android.synthetic.main.toolbar_all_reviews.*

class AllReviewAcitivty : TemplateActivity() {
    var disposable= CompositeDisposable()
    lateinit var swip: SwipeRefreshLayout
    lateinit var tvNoData: TextView
    lateinit var rv: RecyclerView
    lateinit var adapter: ReviewsProfilePageAdapter
    lateinit var layoutManager: LinearLayoutManager
    override fun set_layout() {
        setContentView(R.layout.activity_all_review_list)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    @SuppressLint("SetTextI18n")
    override fun init_views() {

        tv_title_toolbar.setText("${TemplateActivity.selectedProvider?.name} ${resources.getString(R.string.reviews)}")

        tvNoData=tv_no_data
        swip=swip_reviews
        rv=rv_review

    }

    override fun init_events() {

        iv_back.setOnClickListener {
            BasicTools.exitActivity(this)

        }

        layoutManager=LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
        adapter= ReviewsProfilePageAdapter(this,ArrayList())
        rv.layoutManager=layoutManager
        rv.adapter=adapter

        swip.setOnRefreshListener { getAllData() }



        getAllData()



    }

    override fun set_fragment_place() {

    }


    fun getAllData(){
        if (BasicTools.isConnected(this)) {
            swip.isRefreshing=true
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this),
                BasicTools.getProtocol(this).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getReviews(TemplateActivity.selectedProvider!!.id.toString())
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ProviderWithReviewResposne>(this) {
                        override fun onSuccess(result: ProviderWithReviewResposne) {

                            swip.isRefreshing=false

                            if(result.reviews!=null&&result.reviews!!.isNotEmpty()){

                                tvNoData.visibility= View.GONE
                                adapter.change_data(result.reviews!!)
                                rv.adapter=adapter

                            }
                            else{
                                //   root_see_all.visibility=View.GONE
                                tvNoData.visibility= View.VISIBLE
                            }
                        }
                        override fun onFailed(status: Int) {
                            swip.isRefreshing=false
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(this@ProfileProviderDetailsActivity)
                        }
                    }))

        }
        else {

            swip.isRefreshing=false
            showToastMessage(R.string.no_connection)}
    }


}
