package com.wjez.app.activity.offers_list_activity

import com.wjez.app.model.project16.HomeResponse

interface IOffersListAcitivty {

    fun openPage(item: HomeResponse.Data.Offer)
}