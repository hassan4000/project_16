package com.wjez.app.activity.policyActivity


import android.os.Build
import android.os.Bundle
import android.text.Html
import com.wjez.app.R
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.fragment.project21.settingModel.SettingModel
import com.wjez.app.model.project16.PrivacyPolicyModel
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.TemplateActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_about_us.*
import kotlinx.android.synthetic.main.activity_usage_policiy.*
import kotlinx.android.synthetic.main.toolbar_usage_policy.iv_back

class UsagePoliciyActivity : TemplateActivity() {
    var disposable= CompositeDisposable()
    override fun set_layout() {
        setContentView(R.layout.activity_usage_policiy)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun init_views() {

    }

    override fun init_events() {


        iv_back.setOnClickListener { 
            BasicTools.exitActivity(this@UsagePoliciyActivity)
        }

        RQ()
        swip_usage.setOnRefreshListener { RQ() }

    }

    override fun set_fragment_place() {

    }

    fun RQ(){
        if (BasicTools.isConnected(this@UsagePoliciyActivity)) {



            swip_usage.isRefreshing=true

            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@UsagePoliciyActivity),  BasicTools.getProtocol(this@UsagePoliciyActivity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getprivacyPolicy()
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<PrivacyPolicyModel>(this@UsagePoliciyActivity) {
                        override fun onSuccess(result: PrivacyPolicyModel) {


                            swip_usage.isRefreshing=false



                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                if(!BasicTools.isDeviceLanEn())
                                    tv_usage.setText(Html.fromHtml(result.ar.toString(), Html.FROM_HTML_MODE_COMPACT))
                                else   tv_usage.setText(Html.fromHtml(result.en.toString(), Html.FROM_HTML_MODE_COMPACT))
                            } else {
                                if(!BasicTools.isDeviceLanEn())
                                    tv_usage.setText(Html.fromHtml(result.ar.toString()))
                                else  tv_usage.setText(Html.fromHtml(result.en.toString()))
                            }









                        }
                        override fun onFailed(status: Int) {

                            swip_usage.isRefreshing=false

                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            swip_usage.isRefreshing=false

            showToastMessage(R.string.no_connection)}
    }


}
