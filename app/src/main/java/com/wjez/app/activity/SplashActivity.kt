package com.wjez.app.activity


import android.content.Intent
import android.graphics.BitmapFactory

import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.animation.Animation
import android.view.animation.AnimationUtils

import com.wjez.app.R
import com.wjez.app.activity.homeActivity.HomeActivity


import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.TemplateActivity

import com.wjez.app.activity.home_provider.HomeProvider
import com.wjez.app.activity.login_type.LoginTypeActivity
import com.wjez.app.model.project16.TokenResponse
import com.wjez.app.model.project16.UserInfoDataResponse
import com.wjez.app.model.project16.UserInfoResponse
import com.wjez.app.tools.Constants
import com.wjez.app.tools.Constants.LOGIN_TYPE_CONSTOMER
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : TemplateActivity() {
    var disposable: CompositeDisposable = CompositeDisposable()
    private var anim: Animation? = null
    var counterDown:CountDownTimer?=null
    override fun set_layout() {
        setContentView(R.layout.activity_splash)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }


    override fun onStop() {
        counterDown?.cancel()
        super.onStop()
    }

    override fun init_views() {
       // AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_UNSPECIFIED)
        BasicTools.initialize_image_loader(this@SplashActivity)
        iv_splash.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.wjez_logo_v1))
        anim = AnimationUtils.loadAnimation(this@SplashActivity, android.R.anim.fade_in)
        
        anim!!.setDuration(2000)
        iv_splash.animation= anim


        counterDown=object : CountDownTimer(3000,1000){
            override fun onFinish() {

             var i=Intent(this@SplashActivity,LoginTypeActivity::class.java)
                startActivity(i)
                finish()

            }

            override fun onTick(p0: Long) {



            }

        }
        


    }

    override fun init_events() {



        if(BasicTools.getToken(this@SplashActivity).isEmpty()){

            Log.i("TEST_TEST","notTOKEN")
            counterDown!!.start()

        }
        else {

            Log.i("TEST_TEST","TOKEN1")
            Log.i("TEST_TEST","TOKEN1")
            Log.i("TEST_TEST",BasicTools.getLoginType(this@SplashActivity))


            if(BasicTools.getLoginProviderCustomer(this@SplashActivity).equals(Constants.LOGIN_TYPE_CONSTOMER)){
                Log.i("TEST_TEST","TOKEN2")
                loginRQ()

            }
            else if(BasicTools.getLoginProviderCustomer(this@SplashActivity).equals(Constants.LOGIN_TYPE_PROVIDER)) {
                Log.i("TEST_TEST","TOKEN3")
                loginProviderRQ()
            }
            else {
                Log.i("TEST_TEST", BasicTools.getLoginProviderCustomer(this@SplashActivity))
            }


            Log.i("TEST_TEST","TOKEN7")


        }




    }

    override fun set_fragment_place() {

    }

    fun loginRQ(){
        if (BasicTools.isConnected(this@SplashActivity)) {





            var map = HashMap<String, String>()
            map.put("password",BasicTools.getPassword(this@SplashActivity))
            map.put("phone",BasicTools.getPhoneUser(this@SplashActivity))


            val shopApi = ApiClient.getClient(BasicTools.getProtocol(this@SplashActivity).toString())
                ?.create(AppApi::class.java)

            val observable= shopApi!!.loginNormal(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<TokenResponse>(this@SplashActivity) {
                        override fun onSuccess(result: TokenResponse) {



                            if(result.status!!) {



                                BasicTools.setToken(
                                    this@SplashActivity,
                                    result.token!!.toString()
                                )



                                getUserInfo(result.token.toString())
                            }




                            else{

                                BasicTools.logOut(this@SplashActivity)
                                showToastMessage(R.string.faild_to_login)
                                BasicTools.openActivity(this@SplashActivity,
                                    LoginTypeActivity::class.java,true)
                            }


                        }
                        override fun onFailed(status: Int) {

                          //  showShimmerLoginBtn(false)
                            BasicTools.logOut(this@SplashActivity)
                            showToastMessage(R.string.faild_to_login)
                            BasicTools.openActivity(this@SplashActivity,
                                LoginTypeActivity::class.java,true)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {

            showToastMessage(R.string.no_connection)
            BasicTools.logOut(this@SplashActivity)
            BasicTools.openActivity(this@SplashActivity,
                LoginTypeActivity::class.java,true)


        }
    }



    fun loginProviderRQ(){
        if (BasicTools.isConnected(this@SplashActivity)) {





            var map = HashMap<String, String>()
            map.put("password",BasicTools.getPassword(this@SplashActivity))
            map.put("phone",BasicTools.getPhoneUser(this@SplashActivity))


            val shopApi = ApiClient.getClient(BasicTools.getProtocol(this@SplashActivity).toString())
                ?.create(AppApi::class.java)

            val observable= shopApi!!.loginProvider(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<TokenResponse>(this@SplashActivity) {
                        override fun onSuccess(result: TokenResponse) {



                            if(result.status!!) {



                                BasicTools.setToken(
                                    this@SplashActivity,
                                    result.token!!.toString()
                                )



                                getUserInfoProvider(result.token.toString())
                            }




                            else{

                                BasicTools.logOut(this@SplashActivity)
                                showToastMessage(R.string.faild_to_login)
                                BasicTools.openActivity(this@SplashActivity,
                                    LoginTypeActivity::class.java,true)
                            }


                        }
                        override fun onFailed(status: Int) {

                            //  showShimmerLoginBtn(false)
                            BasicTools.logOut(this@SplashActivity)
                            showToastMessage(R.string.faild_to_login)
                            BasicTools.openActivity(this@SplashActivity,
                                LoginTypeActivity::class.java,true)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {

            showToastMessage(R.string.no_connection)
            BasicTools.logOut(this@SplashActivity)
            BasicTools.openActivity(this@SplashActivity,
                LoginTypeActivity::class.java,true)


        }
    }

    fun getUserInfo(token: String) {
        if (BasicTools.isConnected(this@SplashActivity)) {






            val shopApi = ApiClient.getClientJwt(
                token,
                BasicTools.getProtocol(this@SplashActivity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getUserInfo()
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<UserInfoResponse>(this@SplashActivity) {
                        override fun onSuccess(result: UserInfoResponse) {



                            TemplateActivity.wjezUser=result
                            BasicTools.setUserID(this@SplashActivity,result.id.toString())

                            BasicTools.setLoginProviderCustomer(this@SplashActivity,result.userType.toString())
                            if(result.userType==LOGIN_TYPE_CONSTOMER)
                            BasicTools.openActivity(
                                this@SplashActivity,
                                HomeActivity::class.java, true
                            )

                            else{
                                BasicTools.openActivity(
                                    this@SplashActivity,
                                    HomeActivity::class.java, true
                                )
                            }


                        }
                        override fun onFailed(status: Int) {


                            showToastMessage(R.string.faild)
                            BasicTools.logOut(this@SplashActivity)
                            BasicTools.openActivity(this@SplashActivity,
                                LoginTypeActivity::class.java,true)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {

            showToastMessage(R.string.no_connection)
            BasicTools.logOut(this@SplashActivity)
            BasicTools.openActivity(this@SplashActivity,
                LoginTypeActivity::class.java,true)
        }
    }


    fun getUserInfoProvider(token: String) {
        if (BasicTools.isConnected(this@SplashActivity)) {






            val shopApi = ApiClient.getClientJwt(
                token,
                BasicTools.getProtocol(this@SplashActivity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getUserProviderInfo()
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<UserInfoDataResponse>(this@SplashActivity) {
                        override fun onSuccess(result: UserInfoDataResponse) {



                            TemplateActivity.wjezUser=result.data
                            BasicTools.setUserID(this@SplashActivity,result.data?.id.toString())

                            BasicTools.setLoginProviderCustomer(this@SplashActivity,result.data!!.userType.toString())
                            if(result.data!!.userType.equals(LOGIN_TYPE_CONSTOMER))
                                BasicTools.openActivity(
                                    this@SplashActivity,
                                    HomeActivity::class.java, true
                                )

                            else if(result.data!!.userType.equals(Constants.LOGIN_TYPE_PROVIDER)){
                                BasicTools.openActivity(
                                    this@SplashActivity,
                                    HomeProvider::class.java, true
                                )
                            }


                        }
                        override fun onFailed(status: Int) {


                            showToastMessage(R.string.faild)
                            BasicTools.logOut(this@SplashActivity)
                            BasicTools.openActivity(this@SplashActivity,
                                LoginTypeActivity::class.java,true)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {

            showToastMessage(R.string.no_connection)
            BasicTools.logOut(this@SplashActivity)
            BasicTools.openActivity(this@SplashActivity,
                LoginTypeActivity::class.java,true)
        }
    }




}
