package com.wjez.app.activity.addSerivcesCustomerAcitivty


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.wjez.app.R
import com.wjez.app.activity.homeActivity.HomeActivity
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.TemplateActivity
import kotlinx.android.synthetic.main.activity_request_confirmation.*

class AddServicesActivityV3 : TemplateActivity() {
    override fun set_layout() {
        setContentView(R.layout.activity_request_confirmation)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun init_views() {

    }

    override fun init_events() {

        card_next.setOnClickListener {
             BasicTools.clearAllActivity(this@AddServicesActivityV3,HomeActivity::class.java)
        }

    }

    override fun set_fragment_place() {

    }


}
