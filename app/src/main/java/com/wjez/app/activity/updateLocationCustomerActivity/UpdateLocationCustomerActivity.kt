package com.wjez.app.activity.updateLocationCustomerActivity


import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.*
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import com.google.maps.DirectionsApiRequest
import com.google.maps.GeoApiContext
import com.google.maps.internal.PolylineEncoding
import com.google.maps.model.DirectionsResult
import com.wjez.app.R
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.model.PolylineData
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.Constants
import com.wjez.app.tools.TemplateActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_update_location.*
import kotlinx.android.synthetic.main.activity_update_location.shimmer_wait
import kotlinx.android.synthetic.main.dialog_block.*
import kotlinx.android.synthetic.main.toolbar_location.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class UpdateLocationCustomerActivity : TemplateActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    var mPolyLinesData : ArrayList<PolylineData> = ArrayList<PolylineData>()
    private val TAG = UpdateLocationCustomerActivity::class.java.name
    private var mMapView: MapView? = null
    var disposable:CompositeDisposable?=null
    var address=""
    private var mMapBoundary: LatLngBounds? = null
    private var mGoogleMap: GoogleMap? = null
    var mylLocation: Location?=null
    var markerList:ArrayList<Marker>?= ArrayList()
    var mFusedLocationClient: FusedLocationProviderClient? = null

    var mGeoApiContext: GeoApiContext?=null

    private var mapRedy: Boolean = false
    override fun set_layout() {
        setContentView(R.layout.activity_update_location)
    }

    override fun init_activity(savedInstanceState: Bundle?) {
        mMapView = findViewById(R.id.map_location)
        initGoogleMap(savedInstanceState)


    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        var mapViewBundle = outState.getBundle(Constants.MAPVIEW_BUNDLE_KEY)
        if (mapViewBundle == null) {
            mapViewBundle = Bundle()
            outState.putBundle(Constants.MAPVIEW_BUNDLE_KEY, mapViewBundle)
        }
        mMapView!!.onSaveInstanceState(mapViewBundle)
    }

    override fun init_views() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        disposable= CompositeDisposable()

    }

    override fun init_events() {

        iv_back.setOnClickListener {
            BasicTools.exitActivity(this@UpdateLocationCustomerActivity)
        }

        card_ok.setOnClickListener {

            if(markerList!!.size==1){
                update()
            }

        }
    }

    override fun set_fragment_place() {

    }



    override fun onMarkerClick(p0: Marker?): Boolean {











        return  true
    }


    fun update(){
        if (BasicTools.isConnected(this@UpdateLocationCustomerActivity)) {
            BasicTools.showShimmer(card_ok,shimmer_wait,true)
            var map=HashMap<String,String>()
            map.put("lat",markerList!!.get(0).position.latitude.toString())
            map.put("long",markerList!!.get(0).position.longitude.toString())
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@UpdateLocationCustomerActivity),
                BasicTools.getProtocol(this@UpdateLocationCustomerActivity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.updateLocation(map)
            disposable!!.clear()
            disposable!!.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ResponseBody>(this@UpdateLocationCustomerActivity) {
                        override fun onSuccess(result: ResponseBody) {


                            showToastMessage(R.string.success)
                            TemplateActivity.wjezUser!!.lat=markerList!!.get(0).position.latitude.toString()
                            TemplateActivity.wjezUser!!.long=markerList!!.get(0).position.longitude.toString()
                            BasicTools.exitActivity(this@UpdateLocationCustomerActivity)


                        }
                        override fun onFailed(status: Int) {
                            BasicTools.showShimmer(card_ok,shimmer_wait,false)
                          showToastMessage(R.string.faild)
                            //BasicTools.logOut(context)
                        }
                    }))

        }
        else {

            BasicTools.showShimmer(card_ok,shimmer_wait,false)
           showToastMessage(R.string.no_connection)}
    }


     fun addPolylinesToMap(result: DirectionsResult) {
        Handler(Looper.getMainLooper()).post {
            Log.d(TAG, "run: result routes: " + result.routes.size)

            if(mPolyLinesData.size > 0){
                for(polylineData in  mPolyLinesData){
                    polylineData.polyline.remove()
                }
                mPolyLinesData.clear()
                mPolyLinesData =  ArrayList<PolylineData>()

            }

            for (route in result.routes) {
                Log.d(TAG, "run: leg: " + route.legs[0].toString())
                val decodedPath = PolylineEncoding.decode(route.overviewPolyline.encodedPath)

                val newDecodedPath = java.util.ArrayList<LatLng>()

                // This loops through all the LatLng coordinates of ONE polyline.
                for (latLng in decodedPath) {

                    //                        Log.d(TAG, "run: latlng: " + latLng.toString());

                    newDecodedPath.add(
                        LatLng(
                            latLng.lat,
                            latLng.lng
                        )
                    )
                }
                val polyline = mGoogleMap!!.addPolyline(PolylineOptions().addAll(newDecodedPath))
                polyline.color = ContextCompat.getColor(this@UpdateLocationCustomerActivity, R.color.colorPrimary)
                mPolyLinesData.add(PolylineData(polyline, route.legs[0]));


            }
        }
    }


    suspend private fun getAddress(latLng: LatLng): String {
        // 1
        val geocoder = Geocoder(this)
        val addresses: List<Address>?
        val address: Address?
        var addressText = ""

        try {
            // 2
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
            // 3
            if (null != addresses && !addresses.isEmpty()) {
                address = addresses[0]
                for (i in 0 until address.maxAddressLineIndex) {
                    addressText += if (i == 0) address.getAddressLine(i) else "\n" + address.getAddressLine(i)
                }
            }
        } catch (e: IOException) {
            Log.e("TEST_TEST", e.localizedMessage)
        }

        Log.i("TEST_TEST",addressText)
        tv_address.setText(addressText)
        return addressText
    }

    /*-----------------------------calculateDirections---------------------------------*/
     fun calculateDirections(marker: Marker) {
        var destination:com.google.maps.model.LatLng =com.google.maps.model.LatLng(
            marker.getPosition().latitude,
            marker.getPosition().longitude
        )
        var directions: DirectionsApiRequest =  DirectionsApiRequest(mGeoApiContext)
        directions.alternatives(true)

        directions.origin(
            com.google.maps.model.LatLng(
                mylLocation!!.getLatitude(),
                mylLocation!!.getLongitude()
            ))
        Log.d(TAG, "calculateDirections: destination: " + destination.toString());

        directions.destination(destination)
            .setCallback(object : com.google.maps.PendingResult.Callback<DirectionsResult> {
                override fun onResult(result: DirectionsResult) {
                    Log.d(TAG, "onResult: routes: " + result.routes[0].toString())
                    Log.d(TAG,"onResult: geocodedWayPoints: " + result.geocodedWaypoints[0].toString()
                    )
                    addPolylinesToMap(result)
                }

                override fun onFailure(e: Throwable) {
                    Log.e(TAG, "onFailure: " + e.message)

                }
            })


    }
    /*-----------------------onMapReady -----------------------*/
    override fun onMapReady(map: GoogleMap) {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            return
        }
        map.isMyLocationEnabled = true
        mGoogleMap = map
        mGoogleMap!!.setOnMarkerClickListener(this)
        mGoogleMap!!.setMyLocationEnabled(true)
        mGoogleMap!!.getUiSettings().setMyLocationButtonEnabled(true)
      //  mGoogleMap!!.uiSettings.isZoomControlsEnabled=true


        //MAP REDY------------------------------------------------------------------------------------------
        mGoogleMap!!.setOnMapLoadedCallback {
            mapRedy = true

            // save_markes()

           // getLastKnownLocation()
            // addMapMarkers();
            /*        if (MainActivity.myloc != null)
                        mylLocation =
                            LatLng(MainActivity.myloc.getLatitude(), MainActivity.myloc.getLongitude())*/
            // addMapMarkers();
        }

        //-MAP CLICK--------------------------------------------------------------------------------------
        mGoogleMap!!.setOnMapClickListener { latLng ->
            Log.e("TEST_TEST:", "" + latLng.latitude)
            Log.e("TEST_TEST:", "" + latLng.longitude)
            Log.e("TEST_TEST:", "-------------------------")



            mGoogleMap?.clear()
            markerList?.clear()
            val locat=LatLng(latLng.latitude,latLng.longitude)

            var marker:Marker=mGoogleMap!!.addMarker(MarkerOptions().position(locat).title(""))

            marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
            markerList!!.add(marker)



            val task1 = UpdateLocationCustomerActivity.CountryTask(this@UpdateLocationCustomerActivity)
            task1.execute(locat)

           tv_address.setText(task1.get())

         /*   CoroutineScope(Dispatchers.Main).launch {
                var address=getAddress(locat)

            }
*/





            // Creating a marker
            // markerOptions = MarkerOptions()
            // Setting the position for the marker
            // markerOptions!!.position(latLng)


            /*
                        if (point_list.size() == 1) {
                            //Add first marker to the map
                            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                        } else if(point_list.size()==2){
                            PolylineOptions polylineOptions =  new PolylineOptions();

                            polylineOptions.add(point_list.get(0),point_list.get(1));
                            polylineOptions.width(15);
                            polylineOptions.color(Color.BLUE);
                            polylineOptions.geodesic(true);
                            mGoogleMap.addPolyline(polylineOptions);
                            //Add second marker to the map
                            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                        }*/





            // Setting the title for the marker.
            // This will be displayed on taping the marker
            // markerOptions!!.title(latLng.latitude.toString() + " : " + latLng.longitude)

            // Clears the previously touched position
            // mGoogleMap.clear();

            // Animating to the touched position
            mGoogleMap!!.animateCamera(CameraUpdateFactory.newLatLng(latLng))

            // Placing a marker on the touched position
            // mGoogleMap.addMarker(markerOptions);

            /*   if (MainActivity.myloc != null)
                   mylLocation =
                       LatLng(MainActivity.myloc.getLatitude(), MainActivity.myloc.getLongitude())
               Log.d(TAG, "setOnMapClickListener: longitude: " + latLng.longitude)
               Log.d(TAG, "setOnMapClickListener: latitude: " + latLng.latitude)
               Log.d(TAG, "myLocation: longitude: " + mylLocation.longitude)
               Log.d(TAG, "myLocation: " + mylLocation.latitude)*/
        }

    }

    /*----------------------- setCameraView-----------------------*/
    private fun setCameraView(loc:Location) {

        // Set a boundary to start
        val bottomBoundary = loc.latitude - .01
        val leftBoundary = loc.longitude - .01
        val topBoundary = loc.latitude + .010
        val rightBoundary = loc.longitude + .010

        mMapBoundary = LatLngBounds(
            LatLng(bottomBoundary, leftBoundary),
            LatLng(topBoundary, rightBoundary)
        )

        mGoogleMap!!.moveCamera(CameraUpdateFactory.newLatLngBounds(mMapBoundary,0))
        // mGoogleMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(loc.latitude,loc.latitude),0F))
    }

    /*-----------------------initGoogleMap -----------------------*/
    private fun initGoogleMap(savedInstanceState: Bundle?) {
        // *** IMPORTANT ***
        // MapView requires that the Bundle you pass contain _ONLY_ MapView SDK
        // objects or sub-Bundles.
        var mapViewBundle: Bundle? = null
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(Constants.MAPVIEW_BUNDLE_KEY)
        }
        mMapView!!.onCreate(mapViewBundle)
        mMapView!!.getMapAsync(this)

        if(mGeoApiContext == null){
            mGeoApiContext =  GeoApiContext.Builder()
                .apiKey(getString(R.string.google_maps_key))
                .build();
        }


    }


    override fun onResume() {
        super.onResume()
        mMapView!!.onResume()
    }


    override fun onStart() {
        super.onStart()
        mMapView!!.onStart()
    }


    override fun onStop() {
        super.onStop()
        mMapView!!.onStop()
    }

    override fun onPause() {
        mMapView!!.onPause()
        super.onPause()
    }

    override fun onDestroy() {

        mMapView!!.onDestroy()
        super.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMapView!!.onLowMemory()
    }


    class CountryTask internal constructor(private val context: Context) :
        AsyncTask<LatLng, Void, String>() {

        override fun doInBackground(vararg locations: LatLng): String {
            var country = ""
            try {
                val geocoder = Geocoder(context, Locale.getDefault())
                var addresses: List<Address>? = ArrayList()
                addresses =
                    geocoder.getFromLocation(locations[0].latitude, locations[0].longitude, 1)
                if (addresses != null && addresses.size > 0) {
                    val address = addresses[0]


                    country = address.countryCode + ":" + address.countryName
                    //  Toast.makeText(context, "" + country, Toast.LENGTH_SHORT).show()


                }
            } catch (e: IOException) {
                Log.i("TEST_TEST1", e.message!!)
            }
            Log.i("TEST_TEST1", country)
            return country
        }

        override fun onPostExecute(data: String) {


        }
    }
}
