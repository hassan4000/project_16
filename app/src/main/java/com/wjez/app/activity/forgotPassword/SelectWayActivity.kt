package com.wjez.app.activity.forgotPassword


import android.os.Bundle
import com.wjez.app.R
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.TemplateActivity
import kotlinx.android.synthetic.main.activty_select_way.*
import kotlinx.android.synthetic.main.toolbar_backpress1.*

class SelectWayActivity : TemplateActivity() {
    override fun set_layout() {
        setContentView(R.layout.activty_select_way)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun init_views() {

    }

    override fun init_events() {
        iv_back.setOnClickListener {
            BasicTools.exitActivity(this@SelectWayActivity)
        }


        root_email_way.setOnClickListener {

            BasicTools.openActivity(this@SelectWayActivity,EnterEmailActivityForgotPassword::class.java,false)
        }

        root_sms_way.setOnClickListener {
            BasicTools.openActivity(this@SelectWayActivity,EnterPhoneActivityForgotPassword::class.java,false)

        }

    }

    override fun set_fragment_place() {

    }


}
