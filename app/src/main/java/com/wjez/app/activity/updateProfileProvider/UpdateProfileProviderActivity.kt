package com.wjez.app.activity.updateProfileProvider


import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.annotation.Email
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import com.wjez.app.R
import com.wjez.app.adapter.CategoryGridAdapter
import com.wjez.app.adapter.CountrySpinnerAdapter
import com.wjez.app.adapter.SpacesItemDecoration
import com.wjez.app.adapter.SubCategoryHorizantleAdapter
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.model.project16.CategeoryListResponse
import com.wjez.app.model.project16.CountryResponseModel
import com.wjez.app.model.project16.HomeResponse
import com.wjez.app.tools.*
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

import kotlinx.android.synthetic.main.activity_update_profile_provider.*
import kotlinx.android.synthetic.main.activity_update_profile_provider.edit_bio
import kotlinx.android.synthetic.main.activity_update_profile_provider.edit_lang
import kotlinx.android.synthetic.main.activity_update_profile_provider.rv_category
import kotlinx.android.synthetic.main.activity_update_profile_provider.rv_sub_category
import kotlinx.android.synthetic.main.activity_update_profile_provider.shimmer_category_grid
import kotlinx.android.synthetic.main.activity_update_profile_provider.spinner_cities


import kotlinx.android.synthetic.main.toolbar_general.*
import okhttp3.ResponseBody

class UpdateProfileProviderActivity : TemplateActivity(),
    IUpdateProfileProviderActivity, Validator.ValidationListener{


    var selectImage=false

    private var mAddImages =  ArrayList<Uri>()
    var items=ArrayList<Uri>()

    private var anim: Animation? = null
    var validator: Validator? = null

    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    lateinit var editfullName: EditText

    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    lateinit var editBio: EditText

    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    lateinit var editLang: EditText

    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    lateinit var editphone: EditText

    @Email(sequence = 1, messageResId = R.string.enter_a_valid_email_address)
    lateinit var editEmail: EditText

    var selectPic=false

    var disposable= CompositeDisposable()
    var disposableCategory= CompositeDisposable()
    var disposableCity= CompositeDisposable()


    var mainCategoryId=""
    var subCategoryIds=ArrayList<String>()



    lateinit var spinnerAdapter: CountrySpinnerAdapter
    lateinit var rvCategoryList: RecyclerView
    lateinit var rvSubCategoryList: RecyclerView
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var linearLayoutManager: LinearLayoutManager

    lateinit var gridAdapter: CategoryGridAdapter
    lateinit var linearAdapter: SubCategoryHorizantleAdapter

    override fun set_layout() {
        setContentView(R.layout.activity_update_profile_provider)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun init_views() {

        validator = Validator(this)
        validator!!.setValidationListener(this)

        tv_title_toolbar.setText(resources.getString(R.string.edit_info))



        if (TemplateActivity.wjezUser!!.image != null){
            BasicTools.loadImage(
                BasicTools.getUrlImg(this@UpdateProfileProviderActivity, TemplateActivity.wjezUser!!.image!!.toString()),
                iv_profile_user,
                object : DownloadListener {
                    override fun completed(status: Boolean, bitmap: Bitmap) {
                        anim = AnimationUtils.loadAnimation(this@UpdateProfileProviderActivity, android.R.anim.fade_in)
                        anim!!.setDuration(1000)
                        iv_profile_user.animation = anim
                    }
                })

        }


        editfullName=edit_full_name
        editBio=edit_bio
        editEmail=edit_email
        editLang=edit_lang
        editphone=edit_phone_number



        editfullName.setText(TemplateActivity.wjezUser?.name)
        editphone.setText(TemplateActivity.wjezUser?.phone)
        editEmail.setText(TemplateActivity.wjezUser?.email )
        editLang.setText(TemplateActivity.wjezUser?.talkingLang.toString() )
        editBio.setText(TemplateActivity.wjezUser?.bio.toString() )




        spinnerAdapter= CountrySpinnerAdapter(this@UpdateProfileProviderActivity, ArrayList())
        spinner_cities.adapter=spinnerAdapter
        rvCategoryList=rv_category
        rvSubCategoryList=rv_sub_category

        gridAdapter= CategoryGridAdapter(this,ArrayList(),this)
        linearAdapter= SubCategoryHorizantleAdapter(this,ArrayList(),this)


        rvCategoryList.adapter=gridAdapter
        rvSubCategoryList.adapter=linearAdapter

        gridLayoutManager=GridLayoutManager(this@UpdateProfileProviderActivity,2)
        linearLayoutManager= LinearLayoutManager(this@UpdateProfileProviderActivity,LinearLayoutManager.HORIZONTAL,false)

        rvCategoryList.layoutManager=gridLayoutManager
        rvSubCategoryList.layoutManager=linearLayoutManager

        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.d0_2)
        val spacingInPixels2 = resources.getDimensionPixelSize(R.dimen.d0_8)
        rvCategoryList.addItemDecoration( SpacesItemDecoration(spacingInPixels))
        //   rvSubCategoryList.addItemDecoration( SpacesItemDecoration(spacingInPixels2))

        getCategory()
        getCities()


    }

    override fun init_events() {


        TemplateActivity.photosUri.clear()
        iv_back.setOnClickListener {
            BasicTools.exitActivity(this)
        }


        card_save.setOnClickListener {
            validator!!.validate()
        }

        iv_select_img.setOnClickListener {

            pickGalleryImages()
        }






        for((index,value) in gridAdapter.getList().withIndex()){
            if(value.selected!!) {
                changeIndex(index)
                return
            }
        }







    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        // Called when you request permission to read and write to external storage
        when (requestCode) {
            Constants.REQUEST_READ_WRITE_STORAGE_PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    launchGallery()
                } else {
                    Toast.makeText(this, "permission_denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun launchGallery() {
        mAddImages = java.util.ArrayList()
        FilePickerBuilder.instance.setMaxCount(1)
            .setSelectedFiles(mAddImages)
            .setActivityTheme(R.style.filePickerActivityTheme)
            .pickPhoto(this@UpdateProfileProviderActivity)
    }

    private fun pickGalleryImages() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                Constants.REQUEST_READ_WRITE_STORAGE_PERMISSION_CODE
            )
        } else
            launchGallery()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // If the image capture activity was called and was successful
        if (requestCode == FilePickerConst.REQUEST_CODE_PHOTO) {
            if (resultCode == Activity.RESULT_OK && data != null) {


                //  mAddImages!!.clear()
                mAddImages.addAll(data.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_MEDIA) as ArrayList<Uri>)

                items = data.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_MEDIA)!!
                if(items==null){
                    Log.i("signup","IMAGES=NULL")
                }else{


                    if (items.isNotEmpty()){
                        Glide.with(this@UpdateProfileProviderActivity).load(items.get(0)).placeholder(
                            R.drawable.wjez_logo_v2
                        ).error(R.drawable.wjez_logo_v2).into(iv_profile_user)

                        selectPic=true
                        TemplateActivity.photosUri.clear()
                        TemplateActivity.photosUri.add(items.get(0))
                    }

//                    for (item in items)
//                        imageAdapter.addItem(item, 0)

                }

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)

        }
    }

    override fun set_fragment_place() {

    }


    override fun changeIndex(index: Int) {

        var list=ArrayList<HomeResponse.Data.Category>()

        list.addAll(gridAdapter.getList())
        for(i in list)
            i.selected=false



        if(list.isNullOrEmpty())
        Log.i("LIST","---------------------------")
        var item=list.get(index)
        item.selected=true
        list.set(index,item)


        gridAdapter.change_data(list)
        rvCategoryList.adapter=gridAdapter




        var allItem= HomeResponse.Data.Category(-1,null,"","All","الكل","", ArrayList(),true)

        if(item.subCategory==null||item.subCategory.isNullOrEmpty()){
            allItem.selected=true
            item.subCategory= ArrayList()
            item.subCategory!!.add(allItem)}


        else{
            allItem.selected=false
            if(item.subCategory!!.get(0).id!=-1)
                item.subCategory!!.add(0,allItem)
           /* else
                item.subCategory!!.set(0,allItem)*/
        }

        linearAdapter.change_data(item.subCategory!!)


    }

    override fun changeIndexSub(index: Int) {

        var list=ArrayList<HomeResponse.Data.Category>()

        list.addAll(linearAdapter.getList())

        var item=list.get(0)
        var itemIndex=list.get(index)

        if(index==0){
            for(i in list)
                i.selected=false
            item.selected=true
            list.set(0,item)


        }else{
            itemIndex.selected=!itemIndex.selected!!
            item.selected=false
            list.set(0,item)
            list.set(index,itemIndex)

        }


        linearAdapter.change_data(list)


    }

    override fun onValidationFailed(errors: MutableList<ValidationError>?) {
        BasicTools.showValidationErrorMessagesForFields(errors!!, this@UpdateProfileProviderActivity)
    }

    fun getSelectedMainCategory():String{

        var listCatrgoey=ArrayList<HomeResponse.Data.Category>()
        listCatrgoey.addAll(gridAdapter.getList())

        for(i in listCatrgoey){
            if(i.selected!!) {
                return i.id.toString()}

        }

        return  ""

    }

    fun getSelectedSubMainCategory():ArrayList<String>{


        var listSub=ArrayList<HomeResponse.Data.Category>()
        var listSubSelected=ArrayList<String>()
        listSub.addAll(linearAdapter.getList())

        if(listSub.get(0).selected!!)
            return  ArrayList<String>()

        else
            for(i in listSub){
                if(i.selected!!)
                    listSubSelected.add(i.id.toString())

            }

        return  listSubSelected

    }

    override fun onValidationSucceeded() {

        mainCategoryId=getSelectedMainCategory()
        if(mainCategoryId.isEmpty()){
            showToastMessage(R.string.select_categoey)
            return
        }

        subCategoryIds.clear()
        subCategoryIds.addAll(getSelectedSubMainCategory())


        if(TemplateActivity.wjezUser!!.phone.equals(editphone.text.toString().trim()))

            if(!editEmail.text.toString().trim().equals(TemplateActivity.wjezUser!!.email.toString().trim()))
                checkEmailRQ(editEmail.text.toString().trim())
            else checkIfSelected()
        else{
            checkPhoneRQ(editphone.text.toString().trim())
        }

    }

    fun checkIfSelected(){
        if(selectImage)
            updateInfoWithPhoto()
        else updateInfo()
    }


    fun checkPhoneRQ(txt:String){
        if (BasicTools.isConnected(this@UpdateProfileProviderActivity)) {



            BasicTools.showShimmer(card_save,shimmer_wait,true)

            var map = HashMap<String, String>()
            map.put("phone",txt)
            val shopApi = ApiClient.getClient(BasicTools.getProtocol(this@UpdateProfileProviderActivity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.checkPhoneWjez(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ResponseBody>(this@UpdateProfileProviderActivity) {
                        override fun onSuccess(result: ResponseBody) {


                            BasicTools.showShimmer(card_save,shimmer_wait,false)


                            if(!editEmail.text.isNullOrEmpty()){

                                Log.i("TEST_TEST","${editEmail.text.toString().trim()}")
                                Log.i("TEST_TEST","${TemplateActivity.wjezUser!!.email.toString().trim()}")
                                if(!editEmail.text.toString().trim().equals(TemplateActivity.wjezUser!!.email.toString().trim()))
                                    checkEmailRQ(editEmail.text.toString().trim())
                                else checkIfSelected()

                            }else{

                                checkIfSelected()
                            }


                        }
                        override fun onFailed(status: Int) {

                            BasicTools.showShimmer(card_save,shimmer_wait,false)
                            if(status==400){
                                showToastMessage(R.string.check_phone_hint)
                            }else
                                showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            BasicTools.showShimmer(card_save,shimmer_wait,false)
            showToastMessage(R.string.no_connection)}
    }


    fun checkEmailRQ(txt:String){
        if (BasicTools.isConnected(this@UpdateProfileProviderActivity)) {



            BasicTools.showShimmer(card_save,shimmer_wait,true)

            var map = HashMap<String, String>()
            map.put("email",txt)
            val shopApi = ApiClient.getClient(BasicTools.getProtocol(this@UpdateProfileProviderActivity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.checkEmailWjez(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ResponseBody>(this@UpdateProfileProviderActivity) {
                        override fun onSuccess(result: ResponseBody) {


                            BasicTools.showShimmer(card_save,shimmer_wait,false)
                            checkIfSelected()



                        }
                        override fun onFailed(status: Int) {

                            BasicTools.showShimmer(card_save,shimmer_wait,false)
                            if(status==400){
                                showToastMessage(R.string.check_email_hint)
                            }else
                                showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            BasicTools.showShimmer(card_save,shimmer_wait,false)
            showToastMessage(R.string.no_connection)}
    }


    fun getCategory(){
        if (BasicTools.isConnected(this@UpdateProfileProviderActivity)) {

            BasicTools.showShimmer(rvCategoryList,shimmer_category_grid,true)
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@UpdateProfileProviderActivity),
                BasicTools.getProtocol(this@UpdateProfileProviderActivity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getCategoryListSelected()
            disposableCategory.clear()
            disposableCategory.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<CategeoryListResponse>(this@UpdateProfileProviderActivity) {
                        override fun onSuccess(result: CategeoryListResponse) {


                            BasicTools.showShimmer(rvCategoryList,shimmer_category_grid,false)
                            gridAdapter.change_data(result!!.data!!)

                            gridAdapter.notifyDataSetChanged()
                            rvCategoryList.adapter=gridAdapter








                        }
                        override fun onFailed(status: Int) {
                            BasicTools.showShimmer(rvCategoryList,shimmer_category_grid,false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            BasicTools.showShimmer(rvCategoryList,shimmer_category_grid,false)
            showToastMessage(R.string.no_connection)}
    }

    fun getCities(){
        if (BasicTools.isConnected(this@UpdateProfileProviderActivity)) {
            BasicTools.showShimmer(spinner_cities,shimmer_wait_spinner,true)


            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@UpdateProfileProviderActivity),
                BasicTools.getProtocol(this@UpdateProfileProviderActivity).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getCountryWjez()
            disposableCity.clear()
            disposableCity.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<CountryResponseModel>(this@UpdateProfileProviderActivity) {
                        override fun onSuccess(result: CountryResponseModel) {
                            BasicTools.showShimmer(spinner_cities,shimmer_wait_spinner,false)

                            spinnerAdapter= CountrySpinnerAdapter(this@UpdateProfileProviderActivity,result.data!!)
                            spinner_cities.adapter=spinnerAdapter


                            for((index,value) in result.data!!.withIndex()){
                                if(TemplateActivity.wjezUser?.cityId!!.equals(value.id.toString())){
                                    spinner_cities.setSelection(index)

                                }
                            }

                        }
                        override fun onFailed(status: Int) {

                            BasicTools.showShimmer(spinner_cities,shimmer_wait_spinner,false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            BasicTools.showShimmer(spinner_cities,shimmer_wait_spinner,false)
            showToastMessage(R.string.no_connection)}
    }



    private fun updateInfo() {

    }

    private fun updateInfoWithPhoto() {

    }


}
