package com.wjez.app.activity.changeActivityPassProvider


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.EditText
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import com.mobsandgeeks.saripaar.annotation.Password
import com.wjez.app.R
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.model.project21.MessageModel
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.TemplateActivity
import com.wjez.shop.tools.hide
import com.wjez.shop.tools.visible
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_change_passwrod.*


class ChangePasswordProvider : TemplateActivity(), Validator.ValidationListener {

    var disposable= CompositeDisposable()

    var validator: Validator? = null
    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    lateinit var editOldPassword: EditText
    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    @Password
    lateinit var editNewPassword: EditText
    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    @ConfirmPassword(messageResId = R.string.password_donot_match)
    lateinit var editRePassword: EditText
    override fun set_layout() {
        setContentView(R.layout.activity_change_passwrod)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun init_views() {
        validator = Validator(this)
        validator!!.setValidationListener(this)
        editOldPassword=edit_old_password
        editNewPassword=edit_password
        editRePassword=edit_re_password

    }

    override fun init_events() {

        iv_back.setOnClickListener {
            BasicTools.exitActivity(this@ChangePasswordProvider)
        }

        /*old*/
        eye_old_password.setOnClickListener {
            if(editOldPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){


                eye_old_password.setImageResource(R.drawable.visibility_3x)
                editOldPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                eye_old_password.setImageResource(R.drawable.visibility_off_3x)
                editOldPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

            }
        }


        /*new */

        eye_re_password.setOnClickListener {
            if(editRePassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){


                eye_re_password.setImageResource(R.drawable.visibility_3x)
                editRePassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                eye_re_password.setImageResource(R.drawable.visibility_off_3x)
                editRePassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

            }
        }





        /*re Enter */

        eye_password.setOnClickListener {
            if(editNewPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){


                eye_password.setImageResource(R.drawable.visibility_3x)
                editNewPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                eye_password.setImageResource(R.drawable.visibility_off_3x)
                editNewPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

            }
        }


        card_save.setOnClickListener{
            validator!!.validate()

        }


    }

    override fun set_fragment_place() {

    }

    override fun onValidationFailed(errors:List<ValidationError>?) {
        BasicTools.showValidationErrorMessagesForFields(errors!!, this@ChangePasswordProvider)
    }




    fun showShimmerBtn(state: Boolean){

        if(state){
            card_save.visibility= View.GONE
            shimmer_wait.visible()
        }else{
            card_save.visibility= View.VISIBLE
            shimmer_wait.hide()
        }
    }

    override fun onValidationSucceeded() {

        if(!editOldPassword.text.trim().toString().equals(BasicTools.getPassword(this@ChangePasswordProvider))){
            showToastMessage(R.string.old_password_wrong)
            return
        }

        RQ()



    }



    fun RQ(){
        if (BasicTools.isConnected(this@ChangePasswordProvider)) {



            showShimmerBtn(true)

            var map = HashMap<String, String>()
            map.put("old_password",editOldPassword.text.trim().toString())
            map.put("password",editNewPassword.text.trim().toString())
            map.put("c_password",editRePassword.text.trim().toString())
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(this@ChangePasswordProvider),  BasicTools.getProtocol(this@ChangePasswordProvider).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.editPasswordProvider(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<MessageModel>(this@ChangePasswordProvider) {
                        override fun onSuccess(result: MessageModel) {


                            showShimmerBtn(false)


                            if(result.status!!){
                                showToastMessage(R.string.success)
                                BasicTools.setPassword(this@ChangePasswordProvider,editNewPassword.text.trim().toString())
                                BasicTools.exitActivity(this@ChangePasswordProvider)
                            }


                        }
                        override fun onFailed(status: Int) {

                            showShimmerBtn(false)
                            showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {
            showShimmerBtn(false)
            showToastMessage(R.string.no_connection)}
    }


}
