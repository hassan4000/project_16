package com.wjez.app.activity.addSerivcesCustomerAcitivty


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.provider.OpenableColumns
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import com.wjez.app.R
import com.wjez.app.adapter.ImageAddProductAdapter
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.Constants
import com.wjez.app.tools.TemplateActivity
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_add_requset_v2.*
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import java.io.*
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class AddServicesActivityV2 : TemplateActivity() , Validator.ValidationListener{
    var disposable= CompositeDisposable()
    var items=ArrayList<Uri>()
    var validator: Validator? = null
    lateinit var rv:RecyclerView
    lateinit var imageAdapter:ImageAddProductAdapter
    lateinit var layoutManager: LinearLayoutManager
    private var player: MediaPlayer? = null

    var runnable: Runnable? = null
    private var handler = Handler()

    lateinit var seekBar:SeekBar
    lateinit var btnPlay:ImageView
    private var mAddImages =  ArrayList<Uri>()
    lateinit var recordButton:ImageView
    @NotEmpty(sequence = 1, messageResId = R.string.please_fill_out)
    lateinit var editdesc: EditText


    var isUploadingFromDeviceFile = false

    private val LOG_TAG = "AudioRecordTest"
    private val REQUEST_RECORD_AUDIO_PERMISSION = 6546
    var mStartRecording = true
    var mStartPlaying = true
    var tempname = ""
    private var fileName: String = ""
    private var recorder: MediaRecorder? = null
    lateinit var tvRecordingStatus:TextView
    lateinit var btnRecording:ImageView
    var is_recorded: Boolean = false
    var is_recording: Boolean = false

    lateinit var ivBack:ImageView

    override fun set_layout() {
        setContentView(R.layout.activity_add_requset_v2)
    }

    override fun init_activity(savedInstanceState: Bundle?) {

    }

    override fun init_views() {

        ivBack=iv_back
        seekBar=seek_bar
        editdesc=edit_desc
        btnPlay=ic_play_music
        recordButton=iv_recording
        rv=rv_image
        tvRecordingStatus=tv_recording_status
        btnRecording=iv_recording
        fileName =
            "${externalCacheDir!!.absolutePath}/audiorecordtest${Calendar.getInstance().timeInMillis}.3gp"
        tempname = "audiorecordtest" + (Calendar.getInstance().timeInMillis).toString()
        fileName = "${externalCacheDir!!.absolutePath}/${tempname}.3gp"


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // If the image capture activity was called and was successful
        if (requestCode == FilePickerConst.REQUEST_CODE_PHOTO) {
            if (resultCode == Activity.RESULT_OK && data != null) {


                //  mAddImages!!.clear()
                mAddImages.addAll(data.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_MEDIA) as ArrayList<Uri>)

                items = data.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_MEDIA)!!
                if(items==null){
                    Log.i("TEST_TEST","IMAGES=NULL")
                }else{
                    for (item in items){
                        imageAdapter.addItem(item, 0)
                      }







                }

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)

        }
    }

    private fun pickGalleryImages() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                Constants.REQUEST_READ_WRITE_STORAGE_PERMISSION_CODE
            )
        } else
            launchGallery()
    }

    private fun launchGallery() {
        mAddImages = java.util.ArrayList()
        FilePickerBuilder.instance.setMaxCount(5)
            .setSelectedFiles(mAddImages)
            .setActivityTheme(R.style.filePickerActivityTheme)
            .pickPhoto(this@AddServicesActivityV2)
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        // Called when you request permission to read and write to external storage
        when (requestCode) {
            Constants.REQUEST_READ_WRITE_STORAGE_PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    launchGallery()
                } else {
                    Toast.makeText(this, "permission_denied", Toast.LENGTH_SHORT).show()
                }
            }

            REQUEST_RECORD_AUDIO_PERMISSION->{
                onRecord(mStartRecording)
                mStartRecording = !mStartRecording



            }
        }
    }

    override fun init_events() {

        ivBack.setOnClickListener {
            BasicTools.exitActivity(this@AddServicesActivityV2)
        }
        validator = Validator(this)
        validator!!.setValidationListener(this)


        runnable = Runnable {
            seekBar.progress = player!!.currentPosition
            handler.postDelayed(runnable!!, 1000)
        }
        card_next.setOnClickListener {
            validator!!.validate()
        }


        layoutManager= LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        rv.layoutManager=layoutManager
        imageAdapter = ImageAddProductAdapter(this@AddServicesActivityV2, ArrayList())
        rv.adapter=imageAdapter


        iv_add_image.setOnClickListener {
            pickGalleryImages()
        }



        btnRecording.setOnClickListener {
            if (!isUploadingFromDeviceFile) {



                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(
                        Manifest.permission.RECORD_AUDIO
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    Log.i("TEST_TEST", "FRIST")
                    requestPermissions(
                        arrayOf(Manifest.permission.RECORD_AUDIO),
                        REQUEST_RECORD_AUDIO_PERMISSION
                    )
                    //callback onRequestPermissionsResult
                } else {


                    onRecord(mStartRecording)
                    mStartRecording = !mStartRecording

                }
            }
        }

   /*     btnPlay.setOnClickListener {
            if (!isUploadingFromDeviceFile) {
                onPlay(mStartPlaying)
                mStartPlaying = !mStartPlaying

            }
        }*/

        btnPlay.setOnClickListener {
            if (is_recorded) {


                if(player==null)
                    Log.i("TEST_TSET","1")
                    else
                    Log.i("TEST_TSET","2")



                onPlay(mStartPlaying)
                mStartPlaying = !mStartPlaying


             /*   if (player != null && !player!!.isPlaying && runnable != null) {
                  //  player?.start()
                   // handler.postDelayed(runnable!!, 1000)
                    btnPlay.setImageResource(R.drawable.ic_pause)

                    Log.i("TEST_TSET","QWER")

                } else {
                    btnPlay.setImageResource(R.drawable.ic_play)

                    Log.i("TEST_TSET","1234")
                  //  player?.pause()


                }*/
            }
        }


        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (player != null) {
//                    runnable = Runnable {
//                        seekBar?.progress = mediaPlayer!!.currentPosition
//                        handler.postDelayed(runnable!!, 1000)
//                    }
                    player!!.seekTo(progress)


                }


            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

        })
    }

    override fun set_fragment_place() {

    }

    override fun onValidationFailed(errors: MutableList<ValidationError>?) {
        BasicTools.showValidationErrorMessagesForFields(errors!!, this@AddServicesActivityV2)
    }

    override fun onValidationSucceeded() {

        if(validatePhoto())
        uploadPhotoRQ()
    }


    fun getFileName(fileUri: Uri): String {

        var name = ""
        val returnCursor = contentResolver.query(fileUri, null, null, null, null)
        if (returnCursor != null) {
            val nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
            returnCursor.moveToFirst()
            name = returnCursor.getString(nameIndex)
            returnCursor.close()
        }

        return name
    }

    private fun getFile(uri:Uri): File?{
        val parcelFileDescriptor = contentResolver.openFileDescriptor(uri, "r", null)

        parcelFileDescriptor?.let {
            val inputStream = FileInputStream(parcelFileDescriptor.fileDescriptor)
            val file = File(cacheDir, getFileName(uri))
            val outputStream = FileOutputStream(file)

            inputStream.copyTo(outputStream)
            inputStream.close()
            outputStream.close()
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                FileUtils.copy(inputStream,outputStream)
            }*/
            return file
        }
        return null
    }

    private fun validatePhoto(): Boolean {
        if (imageAdapter.getList().size == 0) {
            showToastMessage(R.string.add_item_validate_no_photo)
            return false
        } else return true
    }

    fun uploadPhotoRQ() {
        val bodyList = ArrayList<MultipartBody.Part>()
        try {

            var bodySound: MultipartBody.Part? = null
            for (i in 0 until imageAdapter.itemCount) {
                val file = getFile(imageAdapter.getUserDataAt(i)!!)!!
                Log.i("TEST_TEST", "FILE_PATH adapter=${imageAdapter.getUserDataAt(i)!!.path}")
                Log.i("TEST_TEST", "FILE_PATH file=${file.absolutePath}")
                val requestFile = RequestBody.Companion.create("multipart/form-data".toMediaTypeOrNull(), file)
                val body = MultipartBody.Part.createFormData("image[]", file.getName(), requestFile)
                bodyList.add(body)

            }

            if(is_recorded&&player!=null){
                val file = File(fileName)
                val requestFile = RequestBody.Companion.create(
                    "multipart/form-data".toMediaTypeOrNull(),
                    file
                )
                bodySound = MultipartBody.Part.createFormData(
                    "record_file",
                    file.getName(),
                    requestFile
                )
            }


            if (BasicTools.isConnected(this)) {
                BasicTools.showShimmer(card_next,shimmer_wait,true)

                var map = HashMap<String, String>()
                map.put("name", addServicesName.toString())
                map.put("phone", addServicesPhone.toString())
                map.put("details", editdesc.text.toString().trim())
                map.put("category_id", selectedSubCategory!!.id.toString())
                map.put("lat", addServicesLat.toString())
                map.put("long", addServiceslon.toString())
                map.put("city_id", addServicesCitiesID.toString())

                val shopApi = ApiClient.getClientJwt(
                    BasicTools.getToken(this),
                    BasicTools.getProtocol(this)!!
                )
                    ?.create(AppApi::class.java)
                val observable = shopApi!!.addSerivces(bodyList, map,bodySound!!)
                disposable.clear()
                disposable.add(
                    observable.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : AppObservable<ResponseBody>(this) {
                            override fun onSuccess(result: ResponseBody) {

                                BasicTools.showShimmer(card_next,shimmer_wait,false)

                                showToastMessage(R.string.success)

                                BasicTools.openActivity(this@AddServicesActivityV2,AddServicesActivityV3::class.java,true)

                            }

                            override fun onFailed(status: Int) {
                                Log.e("error", "error=$status")

                                BasicTools.showShimmer(card_next,shimmer_wait,false)
                                showToastMessage(R.string.faild)
                            }
                        })
                )

            } else {
                showToastMessage(R.string.no_connection)
            }
        } catch (e: FileNotFoundException) {
            showToastMessage("error in parse image ")
        }


    }



    private fun onRecord(start: Boolean) = if (start) {
        recordButton?.setImageResource(R.drawable.ic_baseline_stop_circle_24)
        startRecording()
    } else {
        recordButton?.setImageResource(R.drawable.ic_record_voice)
        stopRecording()
    }

    private fun onPlay(start: Boolean) = if (start) {
        startPlaying()



        // barVisualization(player)
    } else {
        stopPlaying()


    }


    private fun startPlaying() {

        player = MediaPlayer().apply {
            try {
                setDataSource(fileName)
                prepare()
                start()
                btnPlay.setImageResource(R.drawable.ic_pause)

            } catch (e: IOException) {

                Log.e(LOG_TAG, "prepare() failed  startPlaying")
                Log.e(LOG_TAG, e.localizedMessage)
            }
        }


        handler.postDelayed(runnable!!, 1000)

        if (player != null)
            player!!.setOnCompletionListener {
                seekBar.progress = 0
                handler.removeCallbacks(runnable!!)
                handler.removeCallbacksAndMessages(null)
                onPlay(mStartPlaying)
                mStartPlaying = !mStartPlaying
                Log.i("TEST_TEST","setSeekBarData1")
            }


        setSeekBarData()

    }

    private fun stopPlaying() {

        if(player==null)
            Log.i("TEST_TEST","PlayerNull")
        else  Log.i("TEST_TEST","PlayerNotNull")
            player?.release()
        Log.i("TEST_TEST", fileName)

        btnPlay.setImageResource(R.drawable.ic_play)
        seekBar.progress = 0
        if(player==null)
            Log.i("TEST_TEST","PlayerNull")
        player = null
    }

    override fun onStop() {
        super.onStop()
        recorder?.release()
        recorder = null
       // player?.release()
       // player = null
        btnPlay.setImageResource(R.drawable.ic_play)
    }

    private  fun recordingTxtStatus(){

        if(is_recorded){

            if(is_recording) tvRecordingStatus.setText(R.string.you_are_now_recording)
            else  tvRecordingStatus.setText(R.string.no_audio_file_found)

        }else{
            tvRecordingStatus.setText(R.string.no_audio_file_found)
        }
    }


    private fun startRecording() {

        recorder = MediaRecorder().apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
            setOutputFile(fileName)
            setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)

            try {
                Log.i(LOG_TAG, "success")
                prepare()
            } catch (e: IOException) {
                Log.e(LOG_TAG, "prepare() failed startRecording")
            }

            is_recording = true
            start()


        }


    }


    private fun stopRecording() {
        if(null !=recorder) {
            try {

                recorder!!.stop()
                recorder!!.release()
                is_recorded = true
                is_recording = false
                recorder = null




            } catch (e: Exception) {

            }
        }
    }

    private fun setSeekBarData() {
        player?.setOnPreparedListener(MediaPlayer.OnPreparedListener {
            seekBar.max = player!!.duration

            Log.i("TEST_TEST","setSeekBarData123")
        })
      //  player?.prepareAsync()

    }


}
