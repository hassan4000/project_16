package com.wjez.app.fragment.providerSubCategory

import com.wjez.app.model.project16.HomeResponse

interface IProviderSubCategoryFragment {

    fun openProfileFile(item:HomeResponse.Data.Provider)
}