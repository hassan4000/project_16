package com.wjez.app.fragment.tabMyRequsetProvider.MyRequestupcomingProvider

import com.wjez.app.model.project16.MyRequsetWjezModel

interface IMyRequestupcomingProviderFragment {

    fun openPage(item: MyRequsetWjezModel.Upcoming)
}