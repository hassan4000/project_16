package com.wjez.app.fragment.homeProvider

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.wjez.app.R
import com.wjez.app.activity.activity_requset_details_provider.RequsetDetailsProivider
import com.wjez.app.activity.updateLocationCustomerActivity.UpdateLocationCustomerActivity
import com.wjez.app.activity.updateLocationProvider.UpdateLocationProvider
import com.wjez.app.activity.update_profile.UpdateProfileActivity
import com.wjez.app.adapter.AllRequsetHomeAdapter
import com.wjez.app.adapter.OffersHomeAdapter
import com.wjez.app.adapter.ServicesHomeAdapter
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.fragment.allRequsetProviderFragment.AllRequsetProviderFragment
import com.wjez.app.model.project16.HomeResponse
import com.wjez.app.model.project16.HomeResponseProvider
import com.wjez.app.model.project16.MyRequsetWjezModel
import com.wjez.app.tools.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

import kotlinx.android.synthetic.main.home_provider.*
import kotlinx.android.synthetic.main.toolbar_home_page_provider.*

class HomeProviderWjiz : TemplateFragment() ,IHomeProvider{
    var disposable= CompositeDisposable()
    lateinit var tvNumRequset:TextView
    lateinit var tvProviderName:TextView
    lateinit var tvPriceHomeProvider:TextView
    lateinit var rvRquest: RecyclerView
    lateinit var linearLayoutManagerRequset: LinearLayoutManager

    lateinit var requsetAdapter:AllRequsetHomeAdapter
    lateinit var swip: SwipeRefreshLayout
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mLocationPermissionGranted = false
    lateinit var ivLocation: ImageView
    lateinit var ivNotification: ImageView
    lateinit var tvSeeAll:TextView
    lateinit var ivEarningsHistory:ImageView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.home_provider, container, false)


        return view
    }

    override fun init_views() {
        ivEarningsHistory=iv_go_earnigns_history
        tvPriceHomeProvider=tv_price_home_provider
        tvProviderName=tv_name_provider
        swip=swip_home_fragment_provider
        tvNumRequset=tv_num_requset
        tvSeeAll=tv_view_all

        ivLocation=iv_location_provider
        ivNotification=iv_notification_provider
        rvRquest=rv_requset

    }

    override fun init_events() {


        tvProviderName.setText(TemplateActivity.wjezUser?.name)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(parent!!)
        linearLayoutManagerRequset= LinearLayoutManager(parent!!,LinearLayoutManager.VERTICAL,false)



        if (TemplateActivity.wjezUser!!.image != null){
            BasicTools.loadImage(
                BasicTools.getUrlImg(parent!!, TemplateActivity.wjezUser!!.image!!.toString()),
                iv_profile_provider,
                object : DownloadListener {
                    override fun completed(status: Boolean, bitmap: Bitmap) {
                         var anim: Animation? = null
                        anim = AnimationUtils.loadAnimation(parent!!, android.R.anim.fade_in)
                        anim!!.setDuration(1000)
                        iv_profile_provider.animation = anim
                    }
                })

        }


        if(TemplateActivity.homeProviderData?.requests.isNullOrEmpty()){
            requsetAdapter= AllRequsetHomeAdapter(parent!!, ArrayList(),this)


            getAllData()

        }else{
            requsetAdapter= AllRequsetHomeAdapter(parent!!, TemplateActivity.homeProviderData?.requests!!,this)

            tvNumRequset.setText(TemplateActivity.homeProviderData?.requestCount.toString())
            tvPriceHomeProvider.setText("${TemplateActivity.homeProviderData?.totalEarnings.toString()} SAR")

        }
        rvRquest.adapter=requsetAdapter
        rvRquest.layoutManager=linearLayoutManagerRequset




        swip.setOnRefreshListener {
            getAllData()
        }







        tvSeeAll.setOnClickListener {
            if(!swip.isRefreshing){


                var f= AllRequsetProviderFragment()
                parent!!.show_fragment2(f,false,false)

            }
        }


        ivLocation.setOnClickListener {

            if (checkMapServices()) {
                if (!mLocationPermissionGranted) {
                    getLocationPermission() // get Permission
                } else {
                    //open map



                    BasicTools.openActivity(parent!!,UpdateLocationProvider::class.java,false)

                }
            }
        }


        ivNotification.setOnClickListener {



        }

        ivEarningsHistory.setOnClickListener {

        }

    }

    override fun init_fragment(savedInstanceState: Bundle?) {

    }

    fun getAllData(){
        if (BasicTools.isConnected(parent!!)) {

            swip.isRefreshing=true

            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(parent!!),  BasicTools.getProtocol(parent!!).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getDashBoardProvider()
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<HomeResponseProvider>(parent!!) {
                        override fun onSuccess(result: HomeResponseProvider) {


                            swip.isRefreshing=false
                            if(result.status!!){

                                if(result.data!=null){
                                    TemplateActivity.homeProviderData=result.data

                                    requsetAdapter.change_data(result.data?.requests!!)

                                    rvRquest.adapter=requsetAdapter

                                    tvNumRequset.setText(result.data?.requestCount.toString())
                                    tvPriceHomeProvider.setText("${result.data?.totalEarnings.toString()} SAR")



                                    //     rvOffers.adapter=adapterOffers

                                }


                            }


                        }
                        override fun onFailed(status: Int) {


                            swip.isRefreshing=false
                            parent!!.showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {

            swip.isRefreshing=false
            parent!!.showToastMessage(R.string.no_connection)}
    }

    override fun on_back_pressed(): Boolean {
        return true
    }

    fun buildAlertMessageNoGps() {
        val builder = AlertDialog.Builder(parent!!) //alert dialog object (yes,no)
        builder.setMessage(resources.getString(R.string.gps_hint))
            .setCancelable(true)
            .setPositiveButton(resources.getString(R.string.yes), DialogInterface.OnClickListener { dialog, id ->
                // K Button
                val enableGpsIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivityForResult(enableGpsIntent, Constants.PERMISSIONS_REQUEST_ENABLE_GPS)
            }).setNegativeButton(resources.getString(R.string.no), DialogInterface.OnClickListener{ dialog, id ->
                dialog.dismiss()
            })
        val alert = builder.create()
        alert.show()

    }

    fun isServicesOK(): Boolean {
        Log.d("TEST_TEST", "isServicesOK: checking google services version")

        val available =
            GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(parent!!)

        if (available == ConnectionResult.SUCCESS) {
            //everything is fine and the user can make map requests
            Log.d("TEST_TEST", "isServicesOK: Google Play Services is working")
            return true
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            //an error occured but we can resolve it
            Log.d("TEST_TEST", "isServicesOK: an error occured but we can fix it")
            val dialog = GoogleApiAvailability.getInstance()
                .getErrorDialog(parent!!, available, Constants.ERROR_DIALOG_REQUEST)
            dialog?.show()
        } else {
            parent!!.showToastMessage("You can't make map requests")
        }
        return false
    }

    fun isMapsEnabled(): Boolean {

        //get LOCATION_SERVICE object : service state
        val manager =parent!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        //if the GPS service not enable show the alert Dialog
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps()
            return false
        }
        return true
    }




    fun checkMapServices(): Boolean {
        if (isServicesOK()) {  //
            if (isMapsEnabled()) {
                return true
            }
        }
        return false

    }

    fun getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         */
        if (ContextCompat.checkSelfPermission(
                parent!!,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) { //PERMISSION_GRANTED : the device accept it the fist run at  lolipop and above and accept in installation below lolipop
            mLocationPermissionGranted = true //GPS Permission is ok

            getLastKnownLocation()
            //TODO open MAp PAGE


            BasicTools.openActivity(parent!!, UpdateLocationProvider::class.java,false)

        } else { //runtime Permission
            ActivityCompat.requestPermissions(
                parent!!,
                arrayOf<String>(android.Manifest.permission.ACCESS_FINE_LOCATION),
                Constants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }

    }

    fun getLastKnownLocation() {
        Log.d("TEST_TEST", "getLastKnownLocation: called.")
        if (ActivityCompat.checkSelfPermission(
                parent!!,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                parent!!,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            return
        }
        mFusedLocationClient!!.getLastLocation().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val location = task.result
                if (location != null) {
                    // myloc = location
                }

                if (location != null) {
                    Log.d("TEST_TEST", "onComplete: latitude: " + location.latitude)
                    Log.d("TEST_TEST", "onComplete: longitude: " + location.longitude)
                }
            }
        }


    }

    override fun openDetails(item: MyRequsetWjezModel.Upcoming) {
        TemplateActivity.fromDashboradProvider=true
        TemplateActivity.selectedRequsetProivder=item
        BasicTools.openActivity(parent!!,RequsetDetailsProivider::class.java,false)
    }
}