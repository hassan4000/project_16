package com.wjez.app.fragment.tabMyRequset.myRequestUpcoming

import com.wjez.app.model.project16.MyRequsetWjezModel

interface IMyRequestUpcomingFragment {

    fun openPage(item: MyRequsetWjezModel.Upcoming)
}