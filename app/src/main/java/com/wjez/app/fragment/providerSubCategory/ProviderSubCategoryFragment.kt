package com.wjez.app.fragment.providerSubCategory

import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView

import androidx.recyclerview.widget.LinearLayoutManager

import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.wjez.app.R
import com.wjez.app.activity.addSerivcesCustomerAcitivty.AddServicesActivity
import com.wjez.app.activity.addSerivcesCustomerAcitivty.AddServicesActivityV2
import com.wjez.app.activity.profileProviderDetails.ProfileProviderDetailsActivity
import com.wjez.app.adapter.ProviderSubCategoryAdapter

import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.model.project16.HomeResponse
import com.wjez.app.model.project16.ProviderNearByResponse

import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools

import com.wjez.app.tools.TemplateActivity
import com.wjez.app.tools.TemplateFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_provider_sub_category.*



class ProviderSubCategoryFragment : TemplateFragment() ,
    IProviderSubCategoryFragment {

    var disposable= CompositeDisposable()
    lateinit var swip: SwipeRefreshLayout
    lateinit var tvNoData: TextView
    lateinit var rv: RecyclerView
    lateinit var adapter: ProviderSubCategoryAdapter
    lateinit var layoutManager: LinearLayoutManager

    lateinit var cardAddRequset:CardView
    lateinit var ivBack:ImageView
    lateinit var tvToolbarName:TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_provider_sub_category, container, false)


        return view
    }

    override fun init_views() {
        ivBack=iv_back
        swip=swip_provider_sub_category
        tvNoData=tv_no_data
        rv=rv_provider_sub_category
        tvToolbarName=tv_title_toolbar
        cardAddRequset=card_requset_services

    }

    override fun init_events() {
        if (BasicTools.isDeviceLanEn()) {
            tvToolbarName.setText(TemplateActivity.selectedSubCategory?.name)
        } else {
            tvToolbarName.setText(TemplateActivity.selectedSubCategory?.nameAr)
        }
        layoutManager= LinearLayoutManager(parent!!,LinearLayoutManager.VERTICAL,false)
        rv.layoutManager=layoutManager
        ivBack.setOnClickListener {


            TemplateActivity.selectedSubCategory=null
            TemplateActivity.providerSubCategoryList=null
            parent!!.onBackPressed()
        }



        if(TemplateActivity.providerSubCategoryList.isNullOrEmpty()){
            adapter= ProviderSubCategoryAdapter(parent!!, ArrayList(),this)
            getAllData()

        }
        else{
            adapter= ProviderSubCategoryAdapter(parent!!, TemplateActivity.providerSubCategoryList!!,this)
        }
        rv.adapter=adapter

        swip.setOnRefreshListener {
            getAllData()
        }


        cardAddRequset.setOnClickListener {
            parent!!.resetRequest()
            BasicTools.openActivity(parent!!,AddServicesActivityV2::class.java,false)
        }

    }

    override fun init_fragment(savedInstanceState: Bundle?) {

    }



    override fun on_back_pressed(): Boolean {
        TemplateActivity.selectedSubCategory=null
        TemplateActivity.providerSubCategoryList=null
        return true
    }

    fun getAllData(){
        if (BasicTools.isConnected(parent!!)) {

            swip.isRefreshing=true

            var map=HashMap<String,String>()
            map.put("lat",TemplateActivity.wjezUser!!.lat.toString())
            map.put("long",TemplateActivity.wjezUser!!.long.toString())
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(parent!!),  BasicTools.getProtocol(parent!!).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getPoviderNear(TemplateActivity.selectedSubCategory!!.id.toString(),map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ProviderNearByResponse>(parent!!) {
                        override fun onSuccess(result: ProviderNearByResponse) {


                            swip.isRefreshing=false
                            if(result.status!!){

                                if(result.data!=null){
                                    TemplateActivity.providerSubCategoryList=result.data

                                    adapter.change_data(result.data!!)

                                    rv.adapter=adapter



                                    //     rvOffers.adapter=adapterOffers

                                }


                            }


                        }
                        override fun onFailed(status: Int) {


                            swip.isRefreshing=false
                            parent!!.showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {

            swip.isRefreshing=false
            parent!!.showToastMessage(R.string.no_connection)}
    }



    override fun openProfileFile(item: HomeResponse.Data.Provider) {

        TemplateActivity.selectedProvider=item
        BasicTools.openActivity(parent!!,ProfileProviderDetailsActivity::class.java,false)


    }
}