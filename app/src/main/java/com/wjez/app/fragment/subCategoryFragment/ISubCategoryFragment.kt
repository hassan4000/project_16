package com.wjez.app.fragment.subCategoryFragment

import com.wjez.app.model.project16.HomeResponse

interface ISubCategoryFragment {

    fun openProviderSubCategory(item:HomeResponse.Data.Category)
}