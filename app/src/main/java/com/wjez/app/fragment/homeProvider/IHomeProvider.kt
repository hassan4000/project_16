package com.wjez.app.fragment.homeProvider

import com.wjez.app.model.project16.HomeResponseProvider
import com.wjez.app.model.project16.MyRequsetWjezModel

interface IHomeProvider {

    fun openDetails(item: MyRequsetWjezModel.Upcoming)
}