package com.wjez.app.fragment.tabMyRequset.myRequestUpcoming

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.wjez.app.R
import com.wjez.app.activity.activity_requset_details.RequsetDetailsActivity

import com.wjez.app.adapter.MyRequestUpcomingAdapter
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.model.project16.MyRequsetWjezModel
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.TemplateActivity
import com.wjez.app.tools.TemplateFragment
import com.wjez.shop.tools.gone
import com.wjez.shop.tools.visible
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

import kotlinx.android.synthetic.main.fragment_my_requset_upcoming_list.*
import kotlinx.android.synthetic.main.fragment_my_requset_upcoming_list.tv_no_data

class MyRequsetUpComingFragment : TemplateFragment(),IMyRequestUpcomingFragment {

    var disposable= CompositeDisposable()
    lateinit var swip: SwipeRefreshLayout
    lateinit var tvNoData: TextView
    lateinit var rv: RecyclerView
    lateinit var adapter: MyRequestUpcomingAdapter
    lateinit var layoutManager: LinearLayoutManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_my_requset_upcoming_list, container, false)


        return view
    }

    override fun init_views() {

        swip=swip_my_requset_upcoming
        rv=rv_my_requset_upcoming
        tvNoData=tv_no_data
    }

    override fun init_events() {

        if(TemplateActivity.myRequsetUpcomingList.isNullOrEmpty()){
            adapter= MyRequestUpcomingAdapter(parent!!, ArrayList(),this)
            getAllData()

        }
        else{
            adapter=MyRequestUpcomingAdapter(parent!!, TemplateActivity.myRequsetUpcomingList!!,this)
        }


        layoutManager= LinearLayoutManager(parent!!,LinearLayoutManager.VERTICAL,false)
        rv.layoutManager=layoutManager
        rv.adapter=adapter

        swip.setOnRefreshListener {

            getAllData()
        }

    }

    override fun init_fragment(savedInstanceState: Bundle?) {

    }

    override fun openPage(item: MyRequsetWjezModel.Upcoming) {
        TemplateActivity.selectedRequset=item
        BasicTools.openActivity(parent!!, RequsetDetailsActivity::class.java,false);
    }

    override fun on_back_pressed(): Boolean {
        return true
    }

    fun getAllData(){
        if (BasicTools.isConnected(parent!!)) {

            swip.isRefreshing=true

            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(parent!!),  BasicTools.getProtocol(parent!!).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getAllMyRequset()
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<MyRequsetWjezModel>(parent!!) {
                        override fun onSuccess(result: MyRequsetWjezModel) {


                            swip.isRefreshing=false
                            if(result.status!!){
                                if(!result.upcoming.isNullOrEmpty()){
                                    tvNoData.gone()
                                    TemplateActivity.myRequsetUpcomingList=result.upcoming
                                    adapter.change_data(result.upcoming!!)
                                    rv.adapter=adapter


                                }
                                else{
                                    tvNoData.visible()
                                }

                                if(result.previous!=null){
                                    TemplateActivity.myRequsetPrevList=result.previous
                                }


                            }


                        }
                        override fun onFailed(status: Int) {
                            swip.isRefreshing=false
                            parent!!.showToastMessage(R.string.faild)

                        }
                    }))
        }
        else {

            swip.isRefreshing=false
            parent!!.showToastMessage(R.string.no_connection)}
    }
}