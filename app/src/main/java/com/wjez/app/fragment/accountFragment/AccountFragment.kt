package com.wjez.app.fragment.accountFragment

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.wjez.app.R
import com.wjez.app.activity.aboutusActivity.AboutUsAcitivity
import com.wjez.app.activity.changePasswordActivity.ChagnePasswrodActivity

import com.wjez.app.activity.login_type.LoginTypeActivity
import com.wjez.app.activity.policyActivity.UsagePoliciyActivity
import com.wjez.app.activity.updateLocationCustomerActivity.UpdateLocationCustomerActivity
import com.wjez.app.activity.update_profile.UpdateProfileActivity
import com.wjez.app.tools.*



import kotlinx.android.synthetic.main.fragment_account.*
import kotlinx.android.synthetic.main.fragment_account.iv_profile_user

class AccountFragment : TemplateFragment() {

    lateinit var tvUserName:TextView
    lateinit var tvEmail:TextView
    lateinit var ivProfile:ImageView
    lateinit var ivSettings:ImageView
    lateinit var ivGoAccount:ImageView
    lateinit var rootEditAccount:LinearLayout
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mLocationPermissionGranted = false
    lateinit var rootAboutUS:LinearLayout

    lateinit var ivEditProfile:ImageView
    lateinit var rootPolicy:LinearLayout
    lateinit var rootCode:LinearLayout


    lateinit var  rootChangePass:LinearLayout
    lateinit var  rootLocation:LinearLayout
    lateinit var  rootLogout:LinearLayout


    private var anim: Animation? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_account, container, false)


        return view
    }

    override fun init_views() {


        rootEditAccount=root_edit_account

        rootAboutUS=root_about
        rootChangePass=root_change_pass
        rootPolicy=root_policy
        rootLocation=root_location
        rootLogout=root_logout
        rootCode=root_code
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(parent!!)

        ivEditProfile=iv_edit_profile


        if (TemplateActivity.wjezUser!!.image != null){
            BasicTools.loadImage(
                BasicTools.getUrlImg(parent!!, TemplateActivity.wjezUser!!.image!!.toString()),
                iv_profile_user,
                object : DownloadListener {
                    override fun completed(status: Boolean, bitmap: Bitmap) {
                        anim = AnimationUtils.loadAnimation(parent, android.R.anim.fade_in)
                        anim!!.setDuration(1000)
                        iv_profile_user.animation = anim
                    }
                })

        }


    }
    fun buildAlertMessageNoGps() {
        val builder = AlertDialog.Builder(parent!!) //alert dialog object (yes,no)
        builder.setMessage(resources.getString(R.string.gps_hint))
            .setCancelable(true)
            .setPositiveButton(resources.getString(R.string.yes), DialogInterface.OnClickListener { dialog, id ->
                // K Button
                val enableGpsIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivityForResult(enableGpsIntent, Constants.PERMISSIONS_REQUEST_ENABLE_GPS)
            }).setNegativeButton(resources.getString(R.string.no), DialogInterface.OnClickListener{ dialog, id ->
                dialog.dismiss()
            })
        val alert = builder.create()
        alert.show()

    }

    fun isServicesOK(): Boolean {
        Log.d("TEST_TEST", "isServicesOK: checking google services version")

        val available =
            GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(parent!!)

        if (available == ConnectionResult.SUCCESS) {
            //everything is fine and the user can make map requests
            Log.d("TEST_TEST", "isServicesOK: Google Play Services is working")
            return true
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            //an error occured but we can resolve it
            Log.d("TEST_TEST", "isServicesOK: an error occured but we can fix it")
            val dialog = GoogleApiAvailability.getInstance()
                .getErrorDialog(parent!!, available, Constants.ERROR_DIALOG_REQUEST)
            dialog?.show()
        } else {
           parent!!.showToastMessage("You can't make map requests")
        }
        return false
    }

    fun isMapsEnabled(): Boolean {

        //get LOCATION_SERVICE object : service state
        val manager =parent!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        //if the GPS service not enable show the alert Dialog
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps()
            return false
        }
        return true
    }




    fun checkMapServices(): Boolean {
        if (isServicesOK()) {  //
            if (isMapsEnabled()) {
                return true
            }
        }
        return false

    }

    fun getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         */
        if (ContextCompat.checkSelfPermission(
                parent!!,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) { //PERMISSION_GRANTED : the device accept it the fist run at  lolipop and above and accept in installation below lolipop
            mLocationPermissionGranted = true //GPS Permission is ok

            getLastKnownLocation()
            //TODO open MAp PAGE


            BasicTools.openActivity(parent!!,UpdateLocationCustomerActivity::class.java,false)

        } else { //runtime Permission
            ActivityCompat.requestPermissions(
                parent!!,
                arrayOf<String>(android.Manifest.permission.ACCESS_FINE_LOCATION),
                Constants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }

    }

    fun getLastKnownLocation() {
        Log.d("TEST_TEST", "getLastKnownLocation: called.")
        if (ActivityCompat.checkSelfPermission(
                parent!!,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                parent!!,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            return
        }
        mFusedLocationClient!!.getLastLocation().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val location = task.result
                if (location != null) {
                    // myloc = location
                }

                if (location != null) {
                    Log.d("TEST_TEST", "onComplete: latitude: " + location.latitude)
                    Log.d("TEST_TEST", "onComplete: longitude: " + location.longitude)
                }
            }
        }


    }

    /*-----------onRequestPermissionsResult------------------------------*/
    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        mLocationPermissionGranted = false
        when (requestCode) {
            Constants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true
                    getLastKnownLocation()
                    //TODO open MAp PAGE

                    parent!!.showToastMessage("3")
                    /*  hassan
                       var intent = Intent(parent, MapScheduleActivity::class.java)
                            startActivity(intent)
                           this.overridePendingTransition(R.anim.enter, R.anim.exit)*/
                    // inflateUserListFragment();
                }
            }
        }
    }

    override fun init_events() {



        rootCode.setOnClickListener {

        }
        rootEditAccount.setOnClickListener {
            BasicTools.openActivity(parent!!,UpdateProfileActivity::class.java,false)
        }

        rootLocation.setOnClickListener {

            if (checkMapServices()) {
                if (!mLocationPermissionGranted) {
                    getLocationPermission() // get Permission
                } else {
                    //open map



                    BasicTools.openActivity(parent!!,UpdateLocationCustomerActivity::class.java,false)

                }
            }

        }

        rootAboutUS.setOnClickListener {
            BasicTools.openActivity(parent!!, AboutUsAcitivity::class.java,false)
        }

        rootPolicy.setOnClickListener {

            BasicTools.openActivity(parent!!, UsagePoliciyActivity::class.java,false)
        }

        rootChangePass.setOnClickListener {

            BasicTools.openActivity(parent!!, ChagnePasswrodActivity::class.java,false)
        }

        rootLogout.setOnClickListener {
            BasicTools.logOut(parent!!)
            BasicTools.clearAllActivity(parent!!, LoginTypeActivity::class.java)
        }



    }

    override fun init_fragment(savedInstanceState: Bundle?) {

    }

    override fun on_back_pressed(): Boolean {
        return true
    }
}