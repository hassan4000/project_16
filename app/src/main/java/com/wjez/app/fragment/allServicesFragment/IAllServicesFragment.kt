package com.wjez.app.fragment.allServicesFragment

import com.wjez.app.model.project16.HomeResponse

interface IAllServicesFragment {

    fun openSubCategory(item:HomeResponse.Data.Category)
}