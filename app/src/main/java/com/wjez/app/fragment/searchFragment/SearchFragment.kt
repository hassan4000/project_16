package com.wjez.app.fragment.searchFragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.wjez.app.R
import com.wjez.app.adapter.OffersHomeAdapter
import com.wjez.app.adapter.ProvidersSearchAdapter
import com.wjez.app.adapter.ServicesHomeAdapter
import com.wjez.app.adapter.ServicesSearchAdapter
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.model.project16.HomeResponse
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.TemplateActivity

import com.wjez.app.tools.TemplateFragment
import com.wjez.shop.tools.gone
import com.wjez.shop.tools.hide
import com.wjez.shop.tools.visible
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.toolbar_search.*

class SearchFragment : TemplateFragment(),ISearchFragment {

    lateinit var ivBack:ImageView

    var disposable= CompositeDisposable()
    lateinit var rvServices: RecyclerView
    lateinit var rvProviders: RecyclerView



    lateinit var adapterServices: ServicesSearchAdapter
    lateinit var adapterProviders: ProvidersSearchAdapter

    lateinit var linearLayoutManagerServices: LinearLayoutManager
    lateinit var linearLayoutManagerProvider: LinearLayoutManager

    lateinit var swip: SwipeRefreshLayout

    lateinit var ivClear:ImageView

    lateinit var editSearch:EditText

    lateinit var rootDataSearch:LinearLayout
    lateinit var rootHint:LinearLayout

    lateinit var numberServices:TextView
    lateinit var numberProviders:TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_search, container, false)


        return view
    }

    override fun init_views() {
        swip=swip_search_fragment
        editSearch=edit_search
        ivBack=iv_back
        ivClear=iv_search_clear
        rootDataSearch=root_data_search
        rootHint=root_hint_search
        numberServices=tv_number_services
        numberProviders=tv_number_providers

        rvServices=rv_services
        rvProviders=rv_providers
    }

    override fun init_events() {
        linearLayoutManagerServices=LinearLayoutManager(parent!!,LinearLayoutManager.HORIZONTAL,false)
        linearLayoutManagerProvider=LinearLayoutManager(parent!!,LinearLayoutManager.VERTICAL,false)


        if(TemplateActivity.searchResponse?.mainCategorySearch.isNullOrEmpty()){
            adapterServices= ServicesSearchAdapter(parent!!, ArrayList(),this)
            adapterProviders= ProvidersSearchAdapter(parent!!,ArrayList(),this)

          //  getAllData()

        }else{
            adapterServices= ServicesSearchAdapter(parent!!, TemplateActivity.searchResponse?.mainCategorySearch!!,this)
            adapterProviders= ProvidersSearchAdapter(parent!!, TemplateActivity.searchResponse?.providersSearch!!,this)

        }
        rvProviders.adapter=adapterProviders
        rvProviders.layoutManager=linearLayoutManagerProvider

        rvServices.adapter=adapterServices
        rvServices.layoutManager=linearLayoutManagerServices





        ivBack.setOnClickListener {
          parent!!.onBackPressed()
        }



        ivClear.setOnClickListener {
            adapterProviders.change_data(ArrayList())
            adapterServices.change_data(ArrayList())
            TemplateActivity.searchResponse=null
            editSearch.setText("")
        }


        swip.setOnRefreshListener {
            adapterProviders.change_data(ArrayList())
            adapterServices.change_data(ArrayList())
            TemplateActivity.searchResponse=null
            editSearch.setText("")
        }

        editSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

                if(s.isNullOrEmpty()) {
                    ivClear.visibility = View.GONE
                    rootHint.visible()
                    rootDataSearch.gone()

                }
                else{
                    ivClear.visibility=View.VISIBLE
                    rootHint.gone()
                    rootDataSearch.visible()
                    RQ(s.toString())
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

    }

    override fun init_fragment(savedInstanceState: Bundle?) {

    }

    override fun on_back_pressed(): Boolean {
        return true
    }

    fun RQ(txt:String){
        if (BasicTools.isConnected(parent!!)) {

            swip.isRefreshing=true

            var map=HashMap<String,String>()
            map.put("txt",txt)
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(parent!!),  BasicTools.getProtocol(parent!!).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.searchWjez(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<HomeResponse>(parent!!) {
                        override fun onSuccess(result: HomeResponse) {


                            swip.isRefreshing=false
                            if(result.status!!){

                                if(result.data!=null){
                                    TemplateActivity.searchResponse=result.data

                                    adapterServices.change_data(result.data?.mainCategorySearch!!)
                                    adapterProviders.change_data(result.data?.providersSearch!!)
                                

                                    rvProviders.adapter=adapterProviders
                                    rvServices.adapter=adapterServices


                                    numberServices.setText(result.data?.mainCategorySearch?.size.toString())
                                    numberProviders.setText(result.data?.providersSearch?.size.toString())

                                    //     rvProviders.adapter=adapterOffers

                                }


                            }


                        }
                        override fun onFailed(status: Int) {


                            swip.isRefreshing=false
                            parent!!.showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {

            swip.isRefreshing=false
            parent!!.showToastMessage(R.string.no_connection)}
    }
}