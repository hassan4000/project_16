package com.wjez.app.fragment.allRequsetProviderFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.wjez.app.R
import com.wjez.app.activity.activity_requset_details_provider.RequsetDetailsProivider
import com.wjez.app.adapter.AllRequsetListProviderAdapter
import com.wjez.app.adapter.AllServicesAdapter
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.model.project16.HomeResponse
import com.wjez.app.model.project16.HomeResponseProvider
import com.wjez.app.model.project16.MyRequsetWjezModel
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.TemplateActivity
import com.wjez.app.tools.TemplateFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_all_requset_list.*
import kotlinx.android.synthetic.main.toolbar_new_requsets.*

class AllRequsetProviderFragment : TemplateFragment(),IAllRequsetProviderFragment {

    var disposable= CompositeDisposable()
    lateinit var swip: SwipeRefreshLayout
    lateinit var tvNoData: TextView
    lateinit var rv: RecyclerView
    lateinit var adapter: AllRequsetListProviderAdapter
    lateinit var layoutManager: LinearLayoutManager

    lateinit var tvNumberRequset:TextView

    lateinit var ivBack: ImageView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_all_requset_list, container, false)


        return view
    }

    override fun init_views() {
        ivBack=iv_back
        swip=swip_requset
        tvNoData=tv_no_data
        rv=rv_requset
        tvNumberRequset=tv_requset_num


    }

    override fun init_events() {
        layoutManager= LinearLayoutManager(parent!!,LinearLayoutManager.VERTICAL,false)
        rv.layoutManager=layoutManager
        ivBack.setOnClickListener {
            parent!!.onBackPressed()
        }


        if(TemplateActivity.homeProviderData?.requests.isNullOrEmpty()){
            adapter= AllRequsetListProviderAdapter(parent!!, ArrayList(),this)
            getAllData()

        }
        else{
            adapter=AllRequsetListProviderAdapter(parent!!, TemplateActivity.homeProviderData?.requests!!,this)
            tvNumberRequset.setText(TemplateActivity.homeProviderData?.requestCount.toString())
        }




        rv.adapter=adapter

        swip.setOnRefreshListener {

            getAllData()
        }

    }

    override fun init_fragment(savedInstanceState: Bundle?) {

    }

    fun getAllData(){
        if (BasicTools.isConnected(parent!!)) {

            swip.isRefreshing=true

            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(parent!!),  BasicTools.getProtocol(parent!!).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getDashBoardProvider()
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<HomeResponseProvider>(parent!!) {
                        override fun onSuccess(result: HomeResponseProvider) {


                            swip.isRefreshing=false
                            if(result.status!!){

                                if(result.data!=null){
                                    TemplateActivity.homeProviderData=result.data

                                    adapter.change_data(result.data?.requests!!)

                                    rv.adapter=adapter


                                    tvNumberRequset.setText(result.data?.requestCount.toString())

                                    //     rvOffers.adapter=adapterOffers

                                }


                            }


                        }
                        override fun onFailed(status: Int) {


                            swip.isRefreshing=false
                            parent!!.showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {

            swip.isRefreshing=false
            parent!!.showToastMessage(R.string.no_connection)}
    }

    override fun on_back_pressed(): Boolean {
        return true
    }

    override fun openDetails(item: MyRequsetWjezModel.Upcoming) {

        TemplateActivity.fromDashboradProvider=true
        TemplateActivity.selectedRequsetProivder=item
        BasicTools.openActivity(parent!!, RequsetDetailsProivider::class.java,false)
    }
}