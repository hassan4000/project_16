package com.wjez.app.fragment.homeFragment

import com.wjez.app.model.project16.HomeResponse

interface IHomeFragment {

    fun openSubCategory(item: HomeResponse.Data.Category)

    fun openPage(item: HomeResponse.Data.Offer)
    fun funPressOnCategory(item: HomeResponse.Data.Category)
    fun funPressOnSubCategory(item: HomeResponse.Data.Category)

    fun openProfileFile(item:HomeResponse.Data.Provider)
}