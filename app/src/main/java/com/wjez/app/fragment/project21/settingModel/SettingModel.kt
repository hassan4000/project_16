package com.wjez.app.fragment.project21.settingModel


import com.google.gson.annotations.SerializedName

data class SettingModel(
    @SerializedName("data")
    var `data`: Data? = null,
    @SerializedName("message")
    var message: String? = null,
    @SerializedName("status")
    var status: Boolean? = null
) {
    data class Data(
        @SerializedName("about")
        var about: String? = null,
        @SerializedName("email")
        var email: String? = null,
        @SerializedName("facebook_link")
        var facebookLink: String? = null,
        @SerializedName("instagram_link")
        var instagramLink: String? = null,
        @SerializedName("phone")
        var phone: String? = null,
        @SerializedName("privacy")
        var privacy: String? = null,
        @SerializedName("twitter_link")
        var twitterLink: String? = null,
        @SerializedName("whatsapp")
        var whatsapp: String? = null,
        @SerializedName("youtube_link")
        var youtubeLink: String? = null
    )
}