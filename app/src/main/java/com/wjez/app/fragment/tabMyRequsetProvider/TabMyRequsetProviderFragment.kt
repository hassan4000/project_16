package com.wjez.app.fragment.tabMyRequsetProvider

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.wjez.app.R
import com.wjez.app.adapter.TabFragmentAdapter
import com.wjez.app.fragment.tabMyRequset.myRequestPrev.MyRequsetPrevFragment
import com.wjez.app.fragment.tabMyRequset.myRequestUpcoming.MyRequsetUpComingFragment
import com.wjez.app.fragment.tabMyRequsetProvider.MyRequestPrevProvider.MyRequestPrevProviderFragment
import com.wjez.app.fragment.tabMyRequsetProvider.MyRequestupcomingProvider.MyRequestupcomingProviderFragment
import com.wjez.app.tools.TemplateActivity
import com.wjez.app.tools.TemplateFragment
import kotlinx.android.synthetic.main.fragment_tab_my_requset.*

class TabMyRequsetProviderFragment : TemplateFragment() {
    lateinit var tabAdapter: TabFragmentAdapter
    lateinit var viewPager: ViewPager


    lateinit var tvPrev: TextView
    lateinit var tvUpcoming: TextView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_tab_my_requset, container, false)


        return view
    }



    override fun init_views() {

        viewPager=pager_my_requset
        tabAdapter = TabFragmentAdapter(childFragmentManager)
        tvPrev=tv_prev
        tvUpcoming=tv_upcoming


    }

    var viewPagerPageChangeListener: ViewPager.OnPageChangeListener =
        object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {

                if(position==0){


                    tvPrev.setBackgroundResource(R.drawable.rounded_pervious)
                    tvUpcoming.setBackgroundResource(R.drawable.rounded_upcoming)
                    tvPrev.setTextColor(ContextCompat.getColor(parent!!,R.color.text_header))
                    tvUpcoming.setTextColor(ContextCompat.getColor(parent!!,R.color.text_color_add_services))




                }else if(position==1){

                    tvPrev.setBackgroundResource(R.drawable.rounded_upcoming)
                    tvUpcoming.setBackgroundResource(R.drawable.rounded_pervious)
                    tvPrev.setTextColor(ContextCompat.getColor(parent!!,R.color.text_color_add_services))
                    tvUpcoming.setTextColor(ContextCompat.getColor(parent!!,R.color.text_header))
                    viewPager.setCurrentItem(1,true)

                }


            }

            override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {

            }

            override fun onPageScrollStateChanged(arg0: Int) {

            }
        }

    override fun init_events() {

        TemplateActivity.myRequsetUpcomingList=null
        TemplateActivity.myRequsetPrevList=null
        var prevFragment=
            MyRequestPrevProviderFragment()
        var upcomingFragment=
            MyRequestupcomingProviderFragment()

        tabAdapter.addFragment(prevFragment, resources.getString(R.string.previous))
        tabAdapter.addFragment(upcomingFragment, resources.getString(R.string.upcoming))


        viewPager.adapter = tabAdapter
        viewPager.offscreenPageLimit = 2

        viewPager.addOnPageChangeListener(viewPagerPageChangeListener)


        tvPrev.setOnClickListener {
            tvPrev.setBackgroundResource(R.drawable.rounded_pervious)
            tvUpcoming.setBackgroundResource(R.drawable.rounded_upcoming)
            tvPrev.setTextColor(ContextCompat.getColor(parent!!,R.color.text_header))
            tvUpcoming.setTextColor(ContextCompat.getColor(parent!!,R.color.text_color_add_services))
            viewPager.setCurrentItem(0,true)
        }


        tvUpcoming.setOnClickListener {
            tvPrev.setBackgroundResource(R.drawable.rounded_upcoming)
            tvUpcoming.setBackgroundResource(R.drawable.rounded_pervious)
            tvPrev.setTextColor(ContextCompat.getColor(parent!!,R.color.text_color_add_services))
            tvUpcoming.setTextColor(ContextCompat.getColor(parent!!,R.color.text_header))
            viewPager.setCurrentItem(1,true)
        }

    }

    override fun init_fragment(savedInstanceState: Bundle?) {

    }

    override fun on_back_pressed(): Boolean {
        return true
    }
}