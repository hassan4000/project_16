package com.wjez.app.fragment.subCategoryFragment

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager

import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.wjez.app.R
import com.wjez.app.activity.addSerivcesCustomerAcitivty.AddServicesActivity
import com.wjez.app.activity.addSerivcesCustomerAcitivty.AddServicesActivityV2
import com.wjez.app.activity.updateLocationCustomerActivity.UpdateLocationCustomerActivity
import com.wjez.app.adapter.ServicesHomeAdapter
import com.wjez.app.adapter.SubCategoryAdapter
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.fragment.providerSubCategory.ProviderSubCategoryFragment
import com.wjez.app.model.project16.HomeResponse
import com.wjez.app.model.project16.SubCategoryResponse
import com.wjez.app.tools.*

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


import kotlinx.android.synthetic.main.fragment_selected_services.*


class SubCategoryFragment : TemplateFragment() ,ISubCategoryFragment{

    var disposable= CompositeDisposable()
    lateinit var swip: SwipeRefreshLayout
    lateinit var tvNoData: TextView
    lateinit var rv: RecyclerView
    lateinit var adapter: SubCategoryAdapter
    lateinit var layoutManager: LinearLayoutManager
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mLocationPermissionGranted = false
    lateinit var cardAddRequset:CardView
    lateinit var ivBack:ImageView
    lateinit var tvToolbarName:TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_selected_services, container, false)


        return view
    }

    override fun init_views() {
        ivBack=iv_back
        swip=swip_services_selected
        tvNoData=tv_no_data
        rv=rv_services_selected
        tvToolbarName=tv_title_toolbar

        cardAddRequset=card_requset_services
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(parent!!)

    }

    override fun init_events() {
        if (BasicTools.isDeviceLanEn()) {
            tvToolbarName.setText(TemplateActivity.selectedCategory?.name)
        } else {
            tvToolbarName.setText(TemplateActivity.selectedCategory?.nameAr)
        }
        layoutManager= LinearLayoutManager(parent!!,LinearLayoutManager.VERTICAL,false)
        rv.layoutManager=layoutManager
        ivBack.setOnClickListener {


            TemplateActivity.selectedCategory=null
            TemplateActivity.subCategoryList=null
            parent!!.onBackPressed()
        }



        if(TemplateActivity.subCategoryList.isNullOrEmpty()){
            adapter= SubCategoryAdapter(parent!!, ArrayList(),this)
            getAllData()

        }
        else{
            adapter= SubCategoryAdapter(parent!!, TemplateActivity.subCategoryList!!,this)
        }
        rv.adapter=adapter

        swip.setOnRefreshListener {
            getAllData()
        }


        cardAddRequset.setOnClickListener {
            parent!!.resetRequest()
            BasicTools.openActivity(parent!!,AddServicesActivityV2::class.java,false)
        }

    }

    override fun init_fragment(savedInstanceState: Bundle?) {

    }



    override fun on_back_pressed(): Boolean {
        TemplateActivity.selectedCategory=null
        TemplateActivity.subCategoryList=null
        return true
    }

    fun getAllData(){
        if (BasicTools.isConnected(parent!!)) {

            swip.isRefreshing=true

            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(parent!!),  BasicTools.getProtocol(parent!!).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getSubCategory(TemplateActivity.selectedCategory?.id.toString())
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<SubCategoryResponse>(parent!!) {
                        override fun onSuccess(result: SubCategoryResponse) {


                            swip.isRefreshing=false
                            if(result.status!!){

                                if(result.data!=null){
                                    TemplateActivity.subCategoryList=result.data

                                    adapter.change_data(result.data!!)

                                    rv.adapter=adapter



                                    //     rvOffers.adapter=adapterOffers

                                }


                            }


                        }
                        override fun onFailed(status: Int) {


                            swip.isRefreshing=false
                            parent!!.showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {

            swip.isRefreshing=false
            parent!!.showToastMessage(R.string.no_connection)}
    }

    fun buildAlertMessageNoGps() {
        val builder = AlertDialog.Builder(parent!!) //alert dialog object (yes,no)
        builder.setMessage(resources.getString(R.string.gps_hint))
            .setCancelable(true)
            .setPositiveButton(resources.getString(R.string.yes), DialogInterface.OnClickListener { dialog, id ->
                // K Button
                val enableGpsIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivityForResult(enableGpsIntent, Constants.PERMISSIONS_REQUEST_ENABLE_GPS)
            }).setNegativeButton(resources.getString(R.string.no), DialogInterface.OnClickListener{ dialog, id ->
                dialog.dismiss()
            })
        val alert = builder.create()
        alert.show()

    }

    fun isServicesOK(): Boolean {
        Log.d("TEST_TEST", "isServicesOK: checking google services version")

        val available =
            GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(parent!!)

        if (available == ConnectionResult.SUCCESS) {
            //everything is fine and the user can make map requests
            Log.d("TEST_TEST", "isServicesOK: Google Play Services is working")
            return true
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            //an error occured but we can resolve it
            Log.d("TEST_TEST", "isServicesOK: an error occured but we can fix it")
            val dialog = GoogleApiAvailability.getInstance()
                .getErrorDialog(parent!!, available, Constants.ERROR_DIALOG_REQUEST)
            dialog?.show()
        } else {
            parent!!.showToastMessage("You can't make map requests")
        }
        return false
    }

    fun isMapsEnabled(): Boolean {

        //get LOCATION_SERVICE object : service state
        val manager =parent!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        //if the GPS service not enable show the alert Dialog
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps()
            return false
        }
        return true
    }




    fun checkMapServices(): Boolean {
        if (isServicesOK()) {  //
            if (isMapsEnabled()) {
                return true
            }
        }
        return false

    }

    fun getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         */
        if (ContextCompat.checkSelfPermission(
                parent!!,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) { //PERMISSION_GRANTED : the device accept it the fist run at  lolipop and above and accept in installation below lolipop
            mLocationPermissionGranted = true //GPS Permission is ok

            getLastKnownLocation()
            //TODO open MAp PAGE


            BasicTools.openActivity(parent!!,UpdateLocationCustomerActivity::class.java,false)

        } else { //runtime Permission
            ActivityCompat.requestPermissions(
                parent!!,
                arrayOf<String>(android.Manifest.permission.ACCESS_FINE_LOCATION),
                Constants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }

    }

    fun getLastKnownLocation() {
        Log.d("TEST_TEST", "getLastKnownLocation: called.")
        if (ActivityCompat.checkSelfPermission(
                parent!!,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                parent!!,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            return
        }
        mFusedLocationClient!!.getLastLocation().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val location = task.result
                if (location != null) {
                    // myloc = location
                }

                if (location != null) {
                    Log.d("TEST_TEST", "onComplete: latitude: " + location.latitude)
                    Log.d("TEST_TEST", "onComplete: longitude: " + location.longitude)
                }
            }
        }


    }

    /*-----------onRequestPermissionsResult------------------------------*/
    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        mLocationPermissionGranted = false
        when (requestCode) {
            Constants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true
                    getLastKnownLocation()
                    //TODO open MAp PAGE

                    parent!!.showToastMessage("3")
                    /*  hassan
                       var intent = Intent(parent, MapScheduleActivity::class.java)
                            startActivity(intent)
                           this.overridePendingTransition(R.anim.enter, R.anim.exit)*/
                    // inflateUserListFragment();
                }
            }
        }
    }

    override fun openProviderSubCategory(item: HomeResponse.Data.Category) {

        if(TemplateActivity.wjezUser!!.lat==null){
            if (checkMapServices()) {
                if (!mLocationPermissionGranted) {
                    getLocationPermission() // get Permission
                } else {
                    //open map



                    BasicTools.openActivity(parent!!,UpdateLocationCustomerActivity::class.java,false)

                }
            }
          }
        else{
        TemplateActivity.selectedSubCategory=item
        var f=ProviderSubCategoryFragment()
        parent!!.show_fragment2(f,false,false)
        }
    }
}