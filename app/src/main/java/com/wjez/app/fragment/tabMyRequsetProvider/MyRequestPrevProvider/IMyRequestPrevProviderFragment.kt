package com.wjez.app.fragment.tabMyRequsetProvider.MyRequestPrevProvider

import com.wjez.app.model.project16.MyRequsetWjezModel

interface IMyRequestPrevProviderFragment {

    fun openPage(item: MyRequsetWjezModel.Upcoming)
}