package com.wjez.app.fragment.allRequsetProviderFragment

import com.wjez.app.model.project16.HomeResponseProvider
import com.wjez.app.model.project16.MyRequsetWjezModel

interface IAllRequsetProviderFragment {

    fun openDetails(item: MyRequsetWjezModel.Upcoming)
}