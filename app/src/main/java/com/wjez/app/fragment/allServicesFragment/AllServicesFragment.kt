package com.wjez.app.fragment.allServicesFragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.wjez.app.R

import com.wjez.app.adapter.OffersHomeAdapter
import com.wjez.app.adapter.AllServicesAdapter
import com.wjez.app.adapter.SpacesItemDecorationV2
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.fragment.subCategoryFragment.SubCategoryFragment
import com.wjez.app.model.project16.HomeResponse
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.TemplateActivity
import com.wjez.app.tools.TemplateFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_all_services_list.*


class AllServicesFragment : TemplateFragment(),IAllServicesFragment {

    var disposable= CompositeDisposable()
    lateinit var swip: SwipeRefreshLayout
    lateinit var tvNoData: TextView
    lateinit var rv: RecyclerView
    lateinit var adapter: AllServicesAdapter
    lateinit var layoutManager: GridLayoutManager

    lateinit var ivBack:ImageView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_all_services_list, container, false)


        return view
    }

    override fun init_views() {
        ivBack=iv_back
        swip=swip_services
        tvNoData=tv_no_data
        rv=rv_services



    }

    override fun init_events() {
        layoutManager=GridLayoutManager(parent!!,2)
        rv.layoutManager=layoutManager
        
         ivBack.setOnClickListener {
             parent!!.onBackPressed()
         }

        val spacingInPixelsH =parent!!.resources.getDimensionPixelSize(R.dimen.d3)
        val spacingInPixelsV =parent!!.resources.getDimensionPixelSize(R.dimen.d1_8)

        Log.i("TEST_TEST","$spacingInPixelsH")
        Log.i("TEST_TEST","$spacingInPixelsV")
        rv.addItemDecoration( SpacesItemDecorationV2(spacingInPixelsV,spacingInPixelsH))


        if(TemplateActivity.homeResponse?.categories.isNullOrEmpty()){
            adapter= AllServicesAdapter(parent!!, ArrayList(),this)
            getAllData()

        }
        else{
            adapter=AllServicesAdapter(parent!!, TemplateActivity.homeResponse?.categories!!,this)
        }


        layoutManager=GridLayoutManager(parent!!,3)
        rv.layoutManager=layoutManager
        rv.adapter=adapter

        swip.setOnRefreshListener {

            getAllData()
        }

    }

    override fun init_fragment(savedInstanceState: Bundle?) {

    }

    override fun on_back_pressed(): Boolean {
        return true
    }

    fun getAllData(){
        if (BasicTools.isConnected(parent!!)) {

            swip.isRefreshing=true

            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(parent!!),  BasicTools.getProtocol(parent!!).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getHomeData()
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<HomeResponse>(parent!!) {
                        override fun onSuccess(result: HomeResponse) {


                            swip.isRefreshing=false
                            if(result.status!!){

                                if(result.data!=null){
                                    TemplateActivity.homeResponse=result.data

                                    adapter.change_data(result.data?.categories!!)

                                    rv.adapter=adapter



                                    //     rvOffers.adapter=adapterOffers

                                }


                            }


                        }
                        override fun onFailed(status: Int) {


                            swip.isRefreshing=false
                            parent!!.showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {

            swip.isRefreshing=false
            parent!!.showToastMessage(R.string.no_connection)}
    }

    override fun openSubCategory(item: HomeResponse.Data.Category) {
        TemplateActivity.selectedCategory=item
        var f= SubCategoryFragment()
        parent!!.show_fragment2(f,false,false)
    }
}