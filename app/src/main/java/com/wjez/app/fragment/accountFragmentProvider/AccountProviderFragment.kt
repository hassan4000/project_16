package com.wjez.app.fragment.accountFragmentProvider

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.widget.LinearLayout
import androidx.cardview.widget.CardView
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.wjez.app.R
import com.wjez.app.activity.aboutusActivity.AboutUsAcitivity
import com.wjez.app.activity.changeActivityPassProvider.ChangePasswordProvider


import com.wjez.app.activity.login_type.LoginTypeActivity
import com.wjez.app.activity.policyActivity.UsagePoliciyActivity
import com.wjez.app.activity.updateLocationProvider.UpdateLocationProvider
import com.wjez.app.activity.updateProfileProvider.UpdateProfileProviderActivity
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.TemplateFragment
import kotlinx.android.synthetic.main.fragment_account_provider.*

class AccountProviderFragment : TemplateFragment() {


    lateinit var  rootChangePass: LinearLayout
    lateinit var  rootEditProfile: LinearLayout
    lateinit var  rootCreditCard: LinearLayout
    lateinit var  rootLocation: LinearLayout
    lateinit var  rootLogout: LinearLayout
    private var anim: Animation? = null

    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mLocationPermissionGranted = false
    lateinit var rootAboutUS:LinearLayout
    lateinit var rootPolicy:LinearLayout
    lateinit var rootShareBtn:CardView



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_account_provider, container, false)


        return view
    }

    override fun init_views() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(parent!!)

        rootCreditCard=root_credit_card
        rootChangePass=root_change_pass
        rootEditProfile=root_edit_profile
        rootLocation=root_location
        rootLogout=root_logout
        rootAboutUS=root_about
        rootPolicy=root_policy




    }

    override fun init_events() {

        rootCreditCard.setOnClickListener {

        }

        rootChangePass.setOnClickListener {
            BasicTools.openActivity(parent!!, ChangePasswordProvider::class.java,false)
        }

        rootEditProfile.setOnClickListener {

            BasicTools.openActivity(parent!!,UpdateProfileProviderActivity::class.java,false)
        }

        rootLocation.setOnClickListener {
            BasicTools.openActivity(parent!!, UpdateLocationProvider::class.java,false)
        }

        rootLogout.setOnClickListener {

            BasicTools.logOut(parent!!)
            BasicTools.clearAllActivity(parent!!, LoginTypeActivity::class.java)
        }
        rootAboutUS.setOnClickListener {

            BasicTools.openActivity(parent!!, AboutUsAcitivity::class.java,false)
        }

        rootPolicy.setOnClickListener {

            BasicTools
            BasicTools.openActivity(parent!!, UsagePoliciyActivity::class.java,false)

        }

        card_share_app.setOnClickListener {
            BasicTools.share_string("https://test-test.com/",parent!!)
        }

    }

    override fun init_fragment(savedInstanceState: Bundle?) {

    }

    override fun on_back_pressed(): Boolean {
        return true
    }
}