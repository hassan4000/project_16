package com.wjez.app.fragment.homeFragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager2.widget.ViewPager2
import com.facebook.shimmer.ShimmerFrameLayout
import com.wjez.app.R
import com.wjez.app.activity.SubCategoryHomeAdapterCustomer
import com.wjez.app.activity.addSerivcesCustomerAcitivty.AddServicesActivity
import com.wjez.app.activity.offers_details.OffersDetailsActivity
import com.wjez.app.activity.offers_list_activity.OffersListAcitivty
import com.wjez.app.activity.profileProviderDetails.ProfileProviderDetailsActivity
import com.wjez.app.adapter.*
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.fragment.allServicesFragment.AllServicesFragment
import com.wjez.app.fragment.searchFragment.SearchFragment
import com.wjez.app.fragment.subCategoryFragment.SubCategoryFragment
import com.wjez.app.model.project16.HomeResponse
import com.wjez.app.model.project16.ProviderNearByResponse
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools
import com.wjez.app.tools.TemplateActivity
import com.wjez.app.tools.TemplateFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.toolbar_home_page_customer.*

class HomeFragment : TemplateFragment(),IHomeFragment{

    var disposable= CompositeDisposable()

    lateinit var tvEmptyProvider:TextView

    var disposableAllProvider= CompositeDisposable()
    lateinit var rvServices:RecyclerView
    lateinit var rvOffers:RecyclerView

    //sub services
    lateinit var rvSubCategory:RecyclerView

    lateinit var viewPager2: ViewPager2
    lateinit var viewPagerAdapter:HomeViewPagerCustomerAdapter
    lateinit var adapterServices:ServicesHomeAdapter
    lateinit var adapterOffers:OffersHomeAdapter
    lateinit var adapterSubCategory:SubCategoryHomeAdapterCustomer

    lateinit var shimmerProvider:ShimmerFrameLayout

    lateinit var linearLayoutManagerServices: LinearLayoutManager
    lateinit var linearLayoutManagerOffers: LinearLayoutManager
    lateinit var linearLayoutManagerSubCategory: LinearLayoutManager
    lateinit var cardRequsetServices:CardView
    lateinit var swip:SwipeRefreshLayout
    lateinit var ivSearch:ImageView

    lateinit var tvSeeAllServices:TextView
    lateinit var tvSeeAllOffers:TextView


    lateinit var rvProvider: RecyclerView
    lateinit var adapterProvider: ProviderNearbyStoresCustomerAdapter
    lateinit var layoutManagerProvider: LinearLayoutManager


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_home, container, false)
        rvOffers=view.findViewById(R.id.rv_offers)
     //   rvServices=view.findViewById(R.id.rv_services)


        return view
    }

    override fun init_views() {
        rvServices=rv_services

        tvEmptyProvider=tv_empty_provider
        rvSubCategory=rv_sub_category
        rvProvider=rv_provider_nearby
        viewPager2=view_pager
        swip=swip_home_fragment
     //   cardRequsetServices=card_requset_services
      //  ivSearch=iv_search
        tvSeeAllServices=tv_view_all_services
        tvSeeAllOffers=tv_view_all_offers


        shimmerProvider=shimmer_provider




    }

    override fun init_events() {

        var list=ArrayList<String>()
        list.add("1")
        list.add("2")


        viewPagerAdapter= HomeViewPagerCustomerAdapter(parent!!,list,this)
        viewPager2.adapter=viewPagerAdapter
        linearLayoutManagerServices=LinearLayoutManager(parent!!,LinearLayoutManager.HORIZONTAL,false)
        linearLayoutManagerOffers=LinearLayoutManager(parent!!,LinearLayoutManager.HORIZONTAL,false)
        linearLayoutManagerSubCategory=LinearLayoutManager(parent!!,LinearLayoutManager.HORIZONTAL,false)


        layoutManagerProvider=LinearLayoutManager(parent!!,LinearLayoutManager.HORIZONTAL,false)






     /*   ivSearch.setOnClickListener {
            var f=SearchFragment()
            parent!!.show_fragment2(f,false,false)
        }*/


     /*   cardRequsetServices.setOnClickListener {

            tvSeeAllServices.performClick()
        }*/


        adapterSubCategory=SubCategoryHomeAdapterCustomer(parent!!, ArrayList(),this)

        if(TemplateActivity.homeResponse?.categories.isNullOrEmpty()){
            adapterServices= ServicesHomeAdapter(parent!!, ArrayList(),this)
            adapterOffers= OffersHomeAdapter(parent!!,ArrayList(),this)



            getAllData()
        }else{
            adapterServices= ServicesHomeAdapter(parent!!, TemplateActivity.homeResponse?.categories!!,this)

            if(TemplateActivity.homeResponse?.offers!=null)
            adapterOffers= OffersHomeAdapter(parent!!,TemplateActivity.homeResponse?.offers!!,this)
            else
                adapterOffers= OffersHomeAdapter(parent!!,ArrayList(),this)


        }

        if(TemplateActivity.providerSubCategoryList.isNullOrEmpty()){
            adapterProvider= ProviderNearbyStoresCustomerAdapter(parent!!, ArrayList(),this)
           // getAllData()

        }
        else{
            adapterProvider= ProviderNearbyStoresCustomerAdapter(parent!!, TemplateActivity.providerSubCategoryList!!,this)
        }



        rvOffers.adapter=adapterOffers
        rvOffers.layoutManager=linearLayoutManagerOffers

        rvServices.adapter=adapterServices
        rvServices.layoutManager=linearLayoutManagerServices


        rvSubCategory.adapter=adapterSubCategory
        rvSubCategory.layoutManager=linearLayoutManagerSubCategory


        rvProvider.adapter=adapterProvider
        rvProvider.layoutManager=layoutManagerProvider

        swip.setOnRefreshListener {
            getAllData()
        }



        tvSeeAllServices.setOnClickListener {
            var f= AllServicesFragment()
            parent!!.show_fragment2(f,false,false)
        }


        tvSeeAllOffers.setOnClickListener {
            TemplateActivity.itemToShowOffersID=-1
            BasicTools.openActivity(parent!!,
                OffersListAcitivty::class.java,false)
        }

    }

    override fun init_fragment(savedInstanceState: Bundle?) {

    }

    override fun on_back_pressed(): Boolean {
        return true
    }


    fun getAllData(){
        if (BasicTools.isConnected(parent!!)) {


            TemplateActivity.selectedCategoryHomeCustomer=-1
            TemplateActivity.selectedSubCategoryHomeCustomer=-1
            swip.isRefreshing=true

            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(parent!!),  BasicTools.getProtocol(parent!!).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getHomeData()
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<HomeResponse>(parent!!) {
                        override fun onSuccess(result: HomeResponse) {


                            swip.isRefreshing=false
                            if(result.status!!){

                                if(result.data!=null){
                                    TemplateActivity.homeResponse=result.data
                                    if(result.data!!.categories!=null)
                                    adapterServices.change_data(result.data?.categories!!)



                                    if(result.data!!.offers!=null)
                                    adapterOffers.change_data(result.data?.offers!!)
                                 //   adapterOffers=OffersHomeAdapter(parent!!,result.data!!.offers!!,this@HomeFragment)

                                    rvOffers.adapter=adapterOffers
                                    rvServices.adapter=adapterServices


                               //     rvOffers.adapter=adapterOffers

                                }


                            }


                        }
                        override fun onFailed(status: Int) {


                            swip.isRefreshing=false
                            parent!!.showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {

            swip.isRefreshing=false
            parent!!.showToastMessage(R.string.no_connection)}
    }



    fun getAllProvider(){
        if (BasicTools.isConnected(parent!!)) {

            tvEmptyProvider.visibility=View.GONE
            swip.isRefreshing=true
            BasicTools.showShimmer(rvProvider,shimmerProvider,true)

            var map=HashMap<String,String>()
            map.put("lat",TemplateActivity.wjezUser!!.lat.toString())
            map.put("long",TemplateActivity.wjezUser!!.long.toString())
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(parent!!),  BasicTools.getProtocol(parent!!).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.getPoviderNear(TemplateActivity.selectedSubCategory!!.id.toString(),map)
            disposableAllProvider.clear()
            disposableAllProvider.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ProviderNearByResponse>(parent!!) {
                        override fun onSuccess(result: ProviderNearByResponse) {


                            BasicTools.showShimmer(rvProvider,shimmerProvider,false)
                            swip.isRefreshing=false
                            if(result.status!!){


                                if(!result.data.isNullOrEmpty()){
                                    TemplateActivity.providerSubCategoryList=result.data

                                    tvEmptyProvider.visibility=View.GONE
                                   adapterProvider.change_data(result.data!!)
                                   rvProvider.adapter=adapterProvider

                                }
                                else {

                                    tvEmptyProvider.visibility=View.VISIBLE
                                    TemplateActivity.providerSubCategoryList=null
                                    adapterProvider.change_data(ArrayList())
                                }


                            }


                        }
                        override fun onFailed(status: Int) {


                            BasicTools.showShimmer(rvProvider,shimmerProvider,false)
                            swip.isRefreshing=false
                            parent!!.showToastMessage(R.string.faild)
                            //BasicTools.logOut(parent!!)
                        }
                    }))

        }
        else {

            swip.isRefreshing=false
            parent!!.showToastMessage(R.string.no_connection)}
    }



    override fun openSubCategory(item: HomeResponse.Data.Category) {
        TemplateActivity.selectedCategory=item
        var f= SubCategoryFragment()
        parent!!.show_fragment2(f,false,false)
    }

    override fun openPage(item: HomeResponse.Data.Offer) {

        TemplateActivity.selectedOffers=item
        BasicTools.openActivity(parent!!, OffersDetailsActivity::class.java,false)
    }

    override fun funPressOnSubCategory(item: HomeResponse.Data.Category) {
        TemplateActivity.selectedSubCategoryHomeCustomer=item.id!!
        TemplateActivity.selectedSubCategory=item
        adapterSubCategory.notifyDataSetChanged()
        getAllProvider()
    }

    override fun funPressOnCategory(item: HomeResponse.Data.Category) {
        Log.i("TEST_TEST","${item.id}")
        TemplateActivity.selectedCategoryHomeCustomer=item.id!!
        if(!item.subCategory.isNullOrEmpty()){
            adapterSubCategory.change_data(item.subCategory!!)
        }

    }


    override fun openProfileFile(item: HomeResponse.Data.Provider) {

        TemplateActivity.selectedProvider=item
        BasicTools.openActivity(parent!!, ProfileProviderDetailsActivity::class.java,false)


    }




}