package com.wjez.app.dialog


import android.app.Dialog
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.Spinner
import androidx.cardview.widget.CardView
import com.wjez.app.R
import com.wjez.app.adapter.ReportSpinnerAdapter
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.model.project16.ProviderWithReviewResposne
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools

import com.wjez.app.tools.TemplateActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.dialog_block.*
import kotlinx.android.synthetic.main.dialog_report.*
import kotlinx.android.synthetic.main.dialog_report.card_done
import kotlinx.android.synthetic.main.dialog_report.shimmer_wait


import okhttp3.ResponseBody


class ReportDialog(val context: TemplateActivity)
    : Dialog(context) {

    var disposable= CompositeDisposable()

    lateinit var cardDone:CardView
    lateinit var spinner:Spinner
    lateinit var adapter:ReportSpinnerAdapter

    lateinit var ivClear:ImageView




    init {


        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_report)
        initView()
        initEvent()


    }




    private fun initView() {
    cardDone=card_done
    ivClear=iv_clear_report
    spinner=spinner_report


    }


    private fun initEvent() {

        cardDone.setOnClickListener{
            ReportUser()

        }
        ivClear.setOnClickListener{
            dismiss()

        }


        adapter= ReportSpinnerAdapter(context,BasicTools.returnArrayListFormId(context,R.array.report_array))
        spinner.adapter=adapter



    }









    fun ReportUser(){
        if (BasicTools.isConnected(context)) {
            BasicTools.showShimmer(cardDone,shimmer_wait,true)
            var map=HashMap<String,String>()
            map.put("provider_id",TemplateActivity.selectedProvider?.id.toString())
            map.put("reason",spinner.selectedItem.toString())
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(context),
                BasicTools.getProtocol(context).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.reportProvider(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ResponseBody>(context) {
                        override fun onSuccess(result: ResponseBody) {

                              BasicTools.showShimmer(cardDone,shimmer_wait,false)

                            context.showToastMessage(R.string.success)
                            dismiss()


                        }
                        override fun onFailed(status: Int) {
                              BasicTools.showShimmer(cardDone,shimmer_wait,false)
                             context.showToastMessage(R.string.faild)
                            //BasicTools.logOut(context)
                        }
                    }))

        }
        else {

              BasicTools.showShimmer(cardDone,shimmer_wait,false)
            context.showToastMessage(R.string.no_connection)}
    }










}