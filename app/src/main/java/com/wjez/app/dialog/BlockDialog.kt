package com.wjez.app.dialog


import android.app.Dialog
import android.view.View
import android.view.Window
import android.widget.ImageView
import androidx.cardview.widget.CardView
import com.wjez.app.R
import com.wjez.app.api.ApiClient
import com.wjez.app.api.AppApi
import com.wjez.app.model.project16.ProviderWithReviewResposne
import com.wjez.app.tools.AppObservable
import com.wjez.app.tools.BasicTools

import com.wjez.app.tools.TemplateActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_profile_provider_details.*
import kotlinx.android.synthetic.main.dialog_block.*
import okhttp3.ResponseBody


class BlockDialog(val context: TemplateActivity)
    : Dialog(context) {

    var disposable= CompositeDisposable()

    lateinit var cardDone:CardView
    lateinit var ivClear:ImageView




    init {


        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_block)
        initView()
        initEvent()


    }




    private fun initView() {
    cardDone=card_done
        ivClear=iv_clear_block


    }


    private fun initEvent() {

        cardDone!!.setOnClickListener{
            blockUser()

        }
        ivClear!!.setOnClickListener{
            dismiss()

        }
    }







    fun blockUser(){
        if (BasicTools.isConnected(context)) {
            BasicTools.showShimmer(cardDone,shimmer_wait,true)
            var map=HashMap<String,String>()
            map.put("provider_id",TemplateActivity.selectedProvider?.id.toString())
            val shopApi = ApiClient.getClientJwt(
                BasicTools.getToken(context),
                BasicTools.getProtocol(context).toString())
                ?.create(AppApi::class.java)
            val observable= shopApi!!.blockProvider(map)
            disposable.clear()
            disposable.add(
                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : AppObservable<ResponseBody>(context) {
                        override fun onSuccess(result: ResponseBody) {

                              BasicTools.showShimmer(cardDone,shimmer_wait,false)
                            context.showToastMessage(R.string.success)
                            dismiss()


                        }
                        override fun onFailed(status: Int) {
                              BasicTools.showShimmer(cardDone,shimmer_wait,false)
                             context.showToastMessage(R.string.faild)
                            //BasicTools.logOut(context)
                        }
                    }))

        }
        else {

              BasicTools.showShimmer(cardDone,shimmer_wait,false)
            context.showToastMessage(R.string.no_connection)}
    }










}