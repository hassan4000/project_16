package com.wjez.app.model.project16


import com.google.gson.annotations.SerializedName

data class HomeResponse(
    @SerializedName("code")
    var code: Int? = null,
    @SerializedName("data")
    var `data`: Data? = null,
    @SerializedName("status")
    var status: Boolean? = null
) {
    data class Data(
        @SerializedName("best_offer")
        var bestOffer: BestOffer? = null,
        @SerializedName("categories")
        var categories: ArrayList<Category>? = null,
        @SerializedName("main_category")
        var mainCategorySearch: ArrayList<Category>? = null,
        @SerializedName("offers")
        var offers: ArrayList<Offer>? = null,
        @SerializedName("providers")
        var providers: ArrayList<Provider>? = null,
        @SerializedName("provider")
        var providersSearch: ArrayList<Provider>? = null
    ) {
        data class BestOffer(
            @SerializedName("cost")
            var cost: String? = null,
            @SerializedName("created_at")
            var createdAt: String? = null,
            @SerializedName("details")
            var details: Any? = null,
            @SerializedName("id")
            var id: Int? = null,
            @SerializedName("is_deleted")
            var isDeleted: String? = null,
            @SerializedName("provider")
            var provider: Provider? = null,
            @SerializedName("provider_id")
            var providerId: String? = null,
            @SerializedName("service_request_id")
            var serviceRequestId: String? = null,
            @SerializedName("status")
            var status: Any? = null,
            @SerializedName("updated_at")
            var updatedAt: String? = null,
            @SerializedName("user_id")
            var userId: String? = null
        ) {
            data class Provider(
                @SerializedName("id")
                var id: Int? = null,
                @SerializedName("image")
                var image: String? = null,
                @SerializedName("name")
                var name: String? = null,
                @SerializedName("rate")
                var rate: Int? = null
            )
        }

        data class Category(
            @SerializedName("id")
            var id: Int? = null,
            @SerializedName("image")
            var image: String? = null,
            @SerializedName("is_main")
            var isMain: String? = null,
            @SerializedName("name")
            var name: String? = null,
            @SerializedName("name_ar")
            var nameAr: String? = null,
            @SerializedName("parent_id")
            var parentId: String? = null,
            @SerializedName("sub_category")
            var subCategory:ArrayList<Category>?=null,
            @SerializedName("selected")
             var selected:Boolean?= false
        )

        data class Offer(
            @SerializedName("cost")
            var cost: String? = null,
            @SerializedName("created_at")
            var createdAt: String? = null,
            @SerializedName("details")
            var details: String? = null,
            @SerializedName("id")
            var id: Int? = null,
            @SerializedName("is_deleted")
            var isDeleted: String? = null,
            @SerializedName("provider")
            var provider: Provider? = null,
            @SerializedName("provider_id")
            var providerId: String? = null,
            @SerializedName("service_request_id")
            var serviceRequestId: String? = null,
            @SerializedName("status")
            var status: Any? = null,
            @SerializedName("updated_at")
            var updatedAt: String? = null,
            @SerializedName("user_id")
            var userId: String? = null
        ) {
            data class Provider(
                @SerializedName("id")
                var id: Int? = null,
                @SerializedName("image")
                var image: String? = null,
                @SerializedName("name")
                var name: String? = null,
                @SerializedName("rate")
                var rate: Any? = null
            )
        }

        data class Provider(
            @SerializedName("about_service")
            var aboutService: Any? = null,
            @SerializedName("account_status")
            var accountStatus: String? = null,
            @SerializedName("bio")
            var bio: String? = null,
            @SerializedName("birthday")
            var birthday: String? = null,
            @SerializedName("created_at")
            var createdAt: String? = null,
            @SerializedName("deleted_at")
            var deletedAt: Any? = null,
            @SerializedName("document")
            var document: String? = null,
            @SerializedName("document_id")
            var documentId: String? = null,
            @SerializedName("email")
            var email: String? = null,
            @SerializedName("email_verified_at")
            var emailVerifiedAt: Any? = null,
            @SerializedName("id")
            var id: Int? = null,
            @SerializedName("image")
            var image: String? = null,
            @SerializedName("is_deleted")
            var isDeleted: String? = null,
            @SerializedName("lat")
            var lat: String? = null,
            @SerializedName("long")
            var long: String? = null,
            @SerializedName("name")
            var name: String? = null,
            @SerializedName("password")
            var password: String? = null,
            @SerializedName("phone")
            var phone: String? = null,
            @SerializedName("rate")
            var rate: String? = null,
            @SerializedName("remember_token")
            var rememberToken: Any? = null,
            @SerializedName("talking_lang")
            var talkingLang: String? = null,
            @SerializedName("updated_at")
            var updatedAt: String? = null,
            @SerializedName("user_type")
            var userType: String? = null
        )
    }
}