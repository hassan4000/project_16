package com.wjez.app.model.project16


import com.google.gson.annotations.SerializedName

data class TokenResponse(
    @SerializedName("code")
    var code: Int? = null,
    @SerializedName("status")
    var status: Boolean? = null,
    @SerializedName("token")
    var token: String? = null
)