package com.wjez.app.model.project16


import com.google.gson.annotations.SerializedName

data class OffersNumberResponse(
    @SerializedName("code")
    var code: Int? = null,
    @SerializedName("data")
    var `data`: OffersItem?  = null,
    @SerializedName("offer_count")
    var offerCount: Int? = null,
    @SerializedName("status")
    var status: Boolean? = null
)


data class OffersItem(
    @SerializedName("offers")
    var offers: ArrayList<HomeResponse.Data.Offer>? = null
)
