package com.wjez.app.model.project16


import com.google.gson.annotations.SerializedName

data class ProviderWithReviewResposne(
    @SerializedName("provider")
    var provider: HomeResponse.Data.Provider? = null,
    @SerializedName("reviews")
    var reviews: ArrayList<Review>? = null
) {
 /*   data class Provider(
        @SerializedName("about_service")
        var aboutService: String? = null,
        @SerializedName("account_status")
        var accountStatus: String? = null,
        @SerializedName("bio")
        var bio: String? = null,
        @SerializedName("birthday")
        var birthday: String? = null,
        @SerializedName("created_at")
        var createdAt: String? = null,
        @SerializedName("deleted_at")
        var deletedAt: String? = null,
        @SerializedName("document")
        var document: String? = null,
        @SerializedName("document_id")
        var documentId: String? = null,
        @SerializedName("email")
        var email: String? = null,
        @SerializedName("email_verified_at")
        var emailVerifiedAt: String? = null,
        @SerializedName("id")
        var id: Int? = null,
        @SerializedName("image")
        var image: String? = null,
        @SerializedName("is_deleted")
        var isDeleted: String? = null,
        @SerializedName("lat")
        var lat: String? = null,
        @SerializedName("long")
        var long: String? = null,
        @SerializedName("name")
        var name: String? = null,
        @SerializedName("password")
        var password: String? = null,
        @SerializedName("phone")
        var phone: String? = null,
        @SerializedName("rate")
        var rate: String? = null,
        @SerializedName("remember_token")
        var rememberToken: String? = null,
        @SerializedName("talking_lang")
        var talkingLang: String? = null,
        @SerializedName("updated_at")
        var updatedAt: String? = null,
        @SerializedName("user_type")
        var userType: String? = null
    )*/

    data class Review(
        @SerializedName("created_at")
        var createdAt: String? = null,
        @SerializedName("feedback")
        var feedback: String? = null,
        @SerializedName("id")
        var id: Int? = null,
        @SerializedName("is_deleted")
        var isDeleted: String? = null,
        @SerializedName("provider_id")
        var providerId: String? = null,
        @SerializedName("rate")
        var rate: String? = null,
        @SerializedName("updated_at")
        var updatedAt: String? = null,
        @SerializedName("user_id")
        var userId: String? = null,
        @SerializedName("user")
        var user: UserInfoResponse? = null
    )
}