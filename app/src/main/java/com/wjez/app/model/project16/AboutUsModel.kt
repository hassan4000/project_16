package com.wjez.app.model.project16


import com.google.gson.annotations.SerializedName

data class AboutUsModel(
    @SerializedName("ar")
    var ar: Ar? = null,
    @SerializedName("code")
    var code: Int? = null,
    @SerializedName("en")
    var en: En? = null,
    @SerializedName("status")
    var status: Boolean? = null
) {
    data class Ar(
        @SerializedName("app_version")
        var appVersion: String? = null,
        @SerializedName("created_at")
        var createdAt: String? = null,
        @SerializedName("email")
        var email: String? = null,
        @SerializedName("id")
        var id: Int? = null,
        @SerializedName("information")
        var information: String? = null,
        @SerializedName("is_deleted")
        var isDeleted: String? = null,
        @SerializedName("lang")
        var lang: String? = null,
        @SerializedName("logo")
        var logo: String? = null,
        @SerializedName("updated_at")
        var updatedAt: String? = null
    )

    data class En(
        @SerializedName("app_version")
        var appVersion: String? = null,
        @SerializedName("created_at")
        var createdAt: String? = null,
        @SerializedName("email")
        var email: String? = null,
        @SerializedName("id")
        var id: Int? = null,
        @SerializedName("information")
        var information: String? = null,
        @SerializedName("is_deleted")
        var isDeleted: String? = null,
        @SerializedName("lang")
        var lang: String? = null,
        @SerializedName("logo")
        var logo: String? = null,
        @SerializedName("updated_at")
        var updatedAt: String? = null
    )
}