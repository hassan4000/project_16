package com.wjez.app.model.project16

import com.google.gson.annotations.SerializedName

data class CategeoryListResponse(
    @SerializedName("code")
    var code: Int? = null,
    @SerializedName("data")
    var `data`: ArrayList<HomeResponse.Data.Category>? = null,
    @SerializedName("status")
    var status: Boolean? = null
)