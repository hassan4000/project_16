package com.wjez.app.model.project16

import com.google.gson.annotations.SerializedName

data class SignupBody (
@SerializedName("name")
    var name: String? = null,
    @SerializedName("phone")
    var phone: String? = null,
    @SerializedName("password")
    var password: String? = null,

    @SerializedName("c_password")
    var c_password: String? = null,

    @SerializedName("user_type")
    var user_type: String? = null,

    @SerializedName("talking_lang")
    var talking_lang: String? = null,

    @SerializedName("document_num")
    var document_num: String? = null,

    @SerializedName("document_id")
    var document_id: String? = null,

    @SerializedName("bio")
    var bio: String? = null,

    @SerializedName("birthday")
    var birthday: String? = null,

    @SerializedName("city_id")
    var city_id: String? = null,

    @SerializedName("image_id")
    var image_id: String? = null,

    @SerializedName("sub_category_id")
    var sub_category_id: ArrayList<String>? = ArrayList(),


  @SerializedName("main_category_id")
  var main_category_id: String? = null

)
