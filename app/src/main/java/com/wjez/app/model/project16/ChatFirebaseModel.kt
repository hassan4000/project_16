package com.wjez.app.model.project16

import com.google.gson.annotations.SerializedName

data class ChatFirebaseModel (

 @SerializedName("isSeen")
 var isSeen: Boolean? = false,
 @SerializedName("message")
 var message: String? = "",
 @SerializedName("messageId")
 var messageId: String? = null,
 @SerializedName("receiver")
 var `receiver`: String? = null,
 @SerializedName("sender")
 var sender: String? = null,
 @SerializedName("url")
 var url: String? = ""
)
/* @SerializedName("isSeen")
 var isSeen:Boolean?=false,
     var sender:String="",
      var message:String="",
      var receiver:String="",
      var url:String="",
      var messageId:String=""*/



