package com.wjez.app.model.project16

import com.google.gson.annotations.SerializedName

data class CountryResponseModel(
    @SerializedName("code")
    var code: Int? = null,
    @SerializedName("data")
    var `data`: ArrayList<CountryModel>? = null,
    @SerializedName("status")
    var status: Boolean? = null
) {

}