package com.wjez.app.model.project16


import com.google.gson.annotations.SerializedName

data class DocumnetIdResponse(
    @SerializedName("code")
    var code: Int? = null,
    @SerializedName("data")
    var `data`: Data? = null,
    @SerializedName("status")
    var status: Boolean? = null
) {
    data class Data(
        @SerializedName("created_at")
        var createdAt: String? = null,
        @SerializedName("document")
        var document: String? = null,
        @SerializedName("id")
        var id: Int? = null,
        @SerializedName("is_deleted")
        var isDeleted: String? = null,
        @SerializedName("updated_at")
        var updatedAt: String? = null
    )
}