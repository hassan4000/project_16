package com.wjez.app.model.project16


import com.google.gson.annotations.SerializedName

data class OffersListResponse(
    @SerializedName("code")
    var code: Int? = null,
    @SerializedName("data")
    var `data`: ArrayList<HomeResponse.Data.Offer>? = null,
    @SerializedName("offer_count")
    var offerCount: Int? = null,
    @SerializedName("status")
    var status: Boolean? = null
)



