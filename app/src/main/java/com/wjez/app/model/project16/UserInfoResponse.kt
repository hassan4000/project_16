package com.wjez.app.model.project16


import com.google.gson.annotations.SerializedName

data class UserInfoResponse(
    @SerializedName("about_service")
    var aboutService: Any? = null,
    @SerializedName("account_status")
    var accountStatus: String? = null,
    @SerializedName("bio")
    var bio: Any? = null,
    @SerializedName("birthday")
    var birthday: Any? = null,
    @SerializedName("created_at")
    var createdAt: String? = null,
    @SerializedName("deleted_at")
    var deletedAt: Any? = null,
    @SerializedName("document")
    var document: Any? = null,
    @SerializedName("document_id")
    var documentId: Any? = null,
    @SerializedName("email")
    var email: String? = null,
    @SerializedName("email_verified_at")
    var emailVerifiedAt: Any? = null,
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("image")
    var image: String? = null,
    @SerializedName("is_deleted")
    var isDeleted: String? = null,
    @SerializedName("lat")
    var lat: Any? = null,
    @SerializedName("long")
    var long: Any? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("password")
    var password: String? = null,
    @SerializedName("phone")
    var phone: String? = null,
    @SerializedName("rate")
    var rate: Any? = null,
    @SerializedName("remember_token")
    var rememberToken: Any? = null,
    @SerializedName("talking_lang")
    var talkingLang: Any? = null,
    @SerializedName("updated_at")
    var updatedAt: String? = null,
    @SerializedName("user_type")
    var userType: String? = null,
    @SerializedName("city_id")
    var cityId: String? = null,
    @SerializedName("user_ratings")
    var userRate:UserRateModel.UserRatings?=null,
    @SerializedName("firebase_uuid")
    var firebaseUUID:String?=null
)