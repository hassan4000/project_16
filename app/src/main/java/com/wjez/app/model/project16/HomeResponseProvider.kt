package com.wjez.app.model.project16


import com.google.gson.annotations.SerializedName

data class HomeResponseProvider(
    @SerializedName("code")
    var code: Int? = null,
    @SerializedName("data")
    var `data`: Data? = null,
    @SerializedName("status")
    var status: Boolean? = null
) {
    data class Data(
        @SerializedName("earnings_history")
        var earningsHistory: ArrayList<EarningsHistory>? = null,
        @SerializedName("last_request")
        var lastRequest: LastRequest? = null,
        @SerializedName("provider")
        var provider: Provider? = null,
        @SerializedName("request_count")
        var requestCount: Int? = null,
        @SerializedName("requests")
        var requests: ArrayList<MyRequsetWjezModel.Upcoming>? = null,
        @SerializedName("total_earnings")
        var totalEarnings: Int? = null
    ) {
        data class EarningsHistory(
            @SerializedName("cost")
            var cost: String? = null,
            @SerializedName("details")
            var details: String? = null,
            @SerializedName("id")
            var id: Int? = null,
            @SerializedName("name")
            var name: String? = null
        )

        data class LastRequest(
            @SerializedName("address")
            var address: Any? = null,
            @SerializedName("category")
            var category: Category? = null,
            @SerializedName("category_id")
            var categoryId: String? = null,
            @SerializedName("city_id")
            var cityId: String? = null,
            @SerializedName("cost")
            var cost: Any? = null,
            @SerializedName("created_at")
            var createdAt: String? = null,
            @SerializedName("cut_expire_date")
            var cutExpireDate: Any? = null,
            @SerializedName("cut_status")
            var cutStatus: Any? = null,
            @SerializedName("cut_value")
            var cutValue: Any? = null,
            @SerializedName("details")
            var details: String? = null,
            @SerializedName("display")
            var display: String? = null,
            @SerializedName("id")
            var id: Int? = null,
            @SerializedName("is_deleted")
            var isDeleted: String? = null,
            @SerializedName("lat")
            var lat: String? = null,
            @SerializedName("long")
            var long: String? = null,
            @SerializedName("name")
            var name: String? = null,
            @SerializedName("phone")
            var phone: String? = null,
            @SerializedName("provider_id")
            var providerId: Any? = null,
            @SerializedName("service_media")
            var serviceMedia: List<ServiceMedia?>? = null,
            @SerializedName("service_status")
            var serviceStatus: String? = null,
            @SerializedName("status")
            var status: String? = null,
            @SerializedName("updated_at")
            var updatedAt: String? = null,
            @SerializedName("user")
            var user: User? = null,
            @SerializedName("user_id")
            var userId: String? = null,
            @SerializedName("voice_message")
            var voiceMessage: Any? = null
        ) {
            data class Category(
                @SerializedName("id")
                var id: Int? = null,
                @SerializedName("image")
                var image: String? = null,
                @SerializedName("name")
                var name: String? = null,
                @SerializedName("name_ar")
                var nameAr: String? = null
            )

            data class ServiceMedia(
                @SerializedName("id")
                var id: Int? = null,
                @SerializedName("media_file")
                var mediaFile: String? = null,
                @SerializedName("service_request_id")
                var serviceRequestId: String? = null
            )

            data class User(
                @SerializedName("id")
                var id: Int? = null,
                @SerializedName("image")
                var image: String? = null,
                @SerializedName("name")
                var name: String? = null
            )
        }

        data class Provider(
            @SerializedName("about_service")
            var aboutService: Any? = null,
            @SerializedName("account_status")
            var accountStatus: String? = null,
            @SerializedName("bio")
            var bio: String? = null,
            @SerializedName("birthday")
            var birthday: String? = null,
            @SerializedName("block_date")
            var blockDate: Any? = null,
            @SerializedName("city")
            var city: Any? = null,
            @SerializedName("city_id")
            var cityId: String? = null,
            @SerializedName("created_at")
            var createdAt: String? = null,
            @SerializedName("deleted_at")
            var deletedAt: Any? = null,
            @SerializedName("document_id")
            var documentId: String? = null,
            @SerializedName("document_num")
            var documentNum: String? = null,
            @SerializedName("email")
            var email: String? = null,
            @SerializedName("email_verified_at")
            var emailVerifiedAt: Any? = null,
            @SerializedName("id")
            var id: Int? = null,
            @SerializedName("image")
            var image: String? = null,
            @SerializedName("is_deleted")
            var isDeleted: String? = null,
            @SerializedName("lang")
            var lang: String? = null,
            @SerializedName("lat")
            var lat: String? = null,
            @SerializedName("long")
            var long: String? = null,
            @SerializedName("name")
            var name: String? = null,
            @SerializedName("password")
            var password: String? = null,
            @SerializedName("phone")
            var phone: String? = null,
            @SerializedName("provider_categories")
            var providerCategories: List<ProviderCategory?>? = null,
            @SerializedName("rate")
            var rate: String? = null,
            @SerializedName("remember_token")
            var rememberToken: Any? = null,
            @SerializedName("talking_lang")
            var talkingLang: String? = null,
            @SerializedName("updated_at")
            var updatedAt: String? = null,
            @SerializedName("user_type")
            var userType: String? = null
        ) {
            data class ProviderCategory(
                @SerializedName("category_id")
                var categoryId: String? = null,
                @SerializedName("created_at")
                var createdAt: String? = null,
                @SerializedName("id")
                var id: Int? = null,
                @SerializedName("is_deleted")
                var isDeleted: String? = null,
                @SerializedName("is_main")
                var isMain: String? = null,
                @SerializedName("provider_id")
                var providerId: String? = null,
                @SerializedName("updated_at")
                var updatedAt: String? = null
            )
        }

        data class Request(
            @SerializedName("address")
            var address: String? = null,
            @SerializedName("category")
            var category: Category? = null,
            @SerializedName("category_id")
            var categoryId: String? = null,
            @SerializedName("city_id")
            var cityId: String? = null,
            @SerializedName("cost")
            var cost: Any? = null,
            @SerializedName("created_at")
            var createdAt: String? = null,
            @SerializedName("cut_expire_date")
            var cutExpireDate: String? = null,
            @SerializedName("cut_status")
            var cutStatus: String? = null,
            @SerializedName("cut_value")
            var cutValue: String? = null,
            @SerializedName("details")
            var details: String? = null,
            @SerializedName("display")
            var display: String? = null,
            @SerializedName("id")
            var id: Int? = null,
            @SerializedName("is_deleted")
            var isDeleted: String? = null,
            @SerializedName("lat")
            var lat: String? = null,
            @SerializedName("long")
            var long: String? = null,
            @SerializedName("name")
            var name: String? = null,
            @SerializedName("phone")
            var phone: String? = null,
            @SerializedName("provider_id")
            var providerId: String? = null,
            @SerializedName("service_media")
            var serviceMedia: ArrayList<ServiceMedia>? = null,
            @SerializedName("service_status")
            var serviceStatus: String? = null,
            @SerializedName("status")
            var status: String? = null,
            @SerializedName("updated_at")
            var updatedAt: String? = null,
            @SerializedName("user")
            var user: User? = null,
            @SerializedName("user_id")
            var userId: String? = null,
            @SerializedName("voice_message")
            var voiceMessage: Any? = null,
            @SerializedName("distance")
            var distance:Double?=null
        ) {
            data class Category(
                @SerializedName("id")
                var id: Int? = null,
                @SerializedName("image")
                var image: String? = null,
                @SerializedName("name")
                var name: String? = null,
                @SerializedName("name_ar")
                var nameAr: String? = null
            )

            data class ServiceMedia(
                @SerializedName("id")
                var id: Int? = null,
                @SerializedName("media_file")
                var mediaFile: String? = null,
                @SerializedName("service_request_id")
                var serviceRequestId: String? = null
            )

            data class User(
                @SerializedName("id")
                var id: Int? = null,
                @SerializedName("image")
                var image: String? = null,
                @SerializedName("name")
                var name: String? = null
            )
        }
    }
}