package com.wjez.app.model.project16


import com.google.gson.annotations.SerializedName

data class PrivacyPolicyModel(
    @SerializedName("ar")
    var ar: String? = null,
    @SerializedName("code")
    var code: Int? = null,
    @SerializedName("en")
    var en: String? = null,
    @SerializedName("status")
    var status: Boolean? = null
)