package com.wjez.app.model.project16


import com.google.gson.annotations.SerializedName

data class MyRequsetWjezModel(
    @SerializedName("code")
    var code: Int? = null,
    @SerializedName("previous")
    var previous: ArrayList<Upcoming>? = null,
    @SerializedName("status")
    var status: Boolean? = null,
    @SerializedName("upcoming")
    var upcoming: ArrayList<Upcoming>? = null
) {
    data class Upcoming(
        @SerializedName("address")
        var address: String? = null,
        @SerializedName("category")
        var category: Category? = null,
        @SerializedName("category_id")
        var categoryId: String? = null,
        @SerializedName("cost")
        var cost: String? = null,
        @SerializedName("created_at")
        var createdAt: String? = null,
        @SerializedName("details")
        var details: String? = null,
        @SerializedName("display")
        var display: String? = null,
        @SerializedName("id")
        var id: Int? = null,
        @SerializedName("is_deleted")
        var isDeleted: String? = null,
        @SerializedName("lat")
        var lat: String? = null,
        @SerializedName("long")
        var long: String? = null,
        @SerializedName("name")
        var name: String? = null,
        @SerializedName("phone")
        var phone: String? = null,
        @SerializedName("provider_id")
        var providerId: String? = null,
        @SerializedName("service_media")
        var serviceMedia: ArrayList<ServiceMedia>? = null,
        @SerializedName("service_status")
        var serviceStatus: String? = null,
        @SerializedName("status")
        var status: String? = null,
        @SerializedName("updated_at")
        var updatedAt: String? = null,
        @SerializedName("user_id")
        var userId: String? = null,
        @SerializedName("provider")
        var provider:UserInfoResponse,
        @SerializedName("user")
        var user:UserInfoResponse?=null,
        @SerializedName("city_id")
        var cityID:String?=null,
        @SerializedName("cut_value")
        var cutValue:String?=null,
        @SerializedName("cut_status")
        var cutStatus:String?=null,
        @SerializedName("cut_expire_date")
        var cutExpireDate:String?=null,
        @SerializedName("voice_message")
        var voicemMessage:Any?=null,
        @SerializedName("distance")
        var distance:Double?=null
    ) {
        data class Category(
            @SerializedName("id")
            var id: Int? = null,
            @SerializedName("image")
            var image: String? = null,
            @SerializedName("name")
            var name: String? = null,
            @SerializedName("name_ar")
            var nameAr:String?=null
        )

        data class ServiceMedia(
            @SerializedName("id")
            var id: Int? = null,
            @SerializedName("media_file")
            var mediaFile: String? = null,
            @SerializedName("service_request_id")
            var serviceRequestId: String? = null
        )
    }
}