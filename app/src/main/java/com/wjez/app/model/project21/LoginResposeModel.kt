package com.wjez.app.model.project21


import com.google.gson.annotations.SerializedName

data class LoginResposeModel(
    @SerializedName("data")
    var `data`: Data? = null,
    @SerializedName("message")
    var message: String? = null,
    @SerializedName("status")
    var status: Boolean? = null
) {
    data class Data(
        @SerializedName("bio")
        var bio: String? = null,
        @SerializedName("birth_date")
        var birthDate: Long? = null,
        @SerializedName("created_at")
        var createdAt: Int? = null,
        @SerializedName("email")
        var email: String? = null,
        @SerializedName("iban")
        var iban: String? = null,
        @SerializedName("image")
        var image: String? = null,
        @SerializedName("messages")
        var messages: ArrayList<Message>? = null,
        @SerializedName("name")
        var name: String? = null,
        @SerializedName("phone")
        var phone: String? = null,
        @SerializedName("specialty")
        var specialty: String? = null,
        @SerializedName("title")
        var title: String? = null,
        @SerializedName("token")
        var token: String? = null,
        @SerializedName("type")
        var type: String? = null,
        @SerializedName("updated_at")
        var updatedAt: Int? = null
    ) {
        data class Message(
            @SerializedName("active")
            var active: String? = null,
            @SerializedName("id")
            var id: Int? = null,
            @SerializedName("price")
            var price: String? = null,
            @SerializedName("title")
            var title: String? = null,
            @SerializedName("type")
            var type: String? = null,
            var checked:Boolean=false
        )
    }
}