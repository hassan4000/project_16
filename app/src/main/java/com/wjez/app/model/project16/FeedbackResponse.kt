package com.wjez.app.model.project16


import com.google.gson.annotations.SerializedName

data class FeedbackResponse(
    @SerializedName("code")
    var code: Int? = null,
    @SerializedName("status")
    var status: Boolean? = null,
    @SerializedName("data")
    var rateData: UserRateModel.UserRatings? = null
)