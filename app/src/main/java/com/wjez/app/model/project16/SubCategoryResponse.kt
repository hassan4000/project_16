package com.wjez.app.model.project16


import com.google.gson.annotations.SerializedName

data class SubCategoryResponse(
    @SerializedName("code")
    var code: Int? = null,
    @SerializedName("data")
    var `data`: ArrayList<HomeResponse.Data.Category>? = null,
    @SerializedName("status")
    var status: Boolean? = null
) {
  /*  data class Data(
        @SerializedName("id")
        var id: Int? = null,
        @SerializedName("image")
        var image: String? = null,
        @SerializedName("is_main")
        var isMain: String? = null,
        @SerializedName("name")
        var name: String? = null,
        @SerializedName("name_ar")
        var nameAr: String? = null,
        @SerializedName("parent_id")
        var parentId: String? = null
    )*/
}