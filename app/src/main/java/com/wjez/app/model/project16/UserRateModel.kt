package com.wjez.app.model.project16


import com.google.gson.annotations.SerializedName

data class UserRateModel(
    @SerializedName("user_ratings")
    var userRatings: UserRatings? = null
) {
    data class UserRatings(
        @SerializedName("feedback")
        var feedback: String? = null,
        @SerializedName("id")
        var id: Int? = null,
        @SerializedName("rate")
        var rate: String? = null
    )
}