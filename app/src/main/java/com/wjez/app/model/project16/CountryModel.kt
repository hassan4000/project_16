package com.wjez.app.model.project16


import com.google.gson.annotations.SerializedName

data class CountryModel(
    @SerializedName("country_id")
    var countryId: String? = null,
    @SerializedName("created_at")
    var createdAt: String? = null,
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("is_deleted")
    var isDeleted: String? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("name_ar")
    var nameAr: String? = null,
    @SerializedName("updated_at")
    var updatedAt: String? = null
)